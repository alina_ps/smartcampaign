<?php
/**
 * User: Frantzolakis Manolis
 * Date: 29/2/2016
 * Time: 1:52 ��
 */

class Autoloader {

    public static $_instance;

    private $_src = array();
    private $_ext = array('.php','.class.php');

    private function __construct($src = null, $ext = null){

        if (is_array($src))
            $this->_src = $src;

        if (is_array($ext))
            $this->_ext = $ext;

        spl_autoload_register(array($this, 'dirty'));

    }

    public static function init($src = null, $ext = null){
        if (self::$_instance == null){
            self::$_instance = new self($src, $ext);
        }

        return self::$_instance;
    }


    /**
     * @param $className A string defining the class name to be loaded
     */
    private function dirty($className){

        $className = ltrim($className, '\\');
        $filename = "";


        if ($lastNsPos = strripos($className,'\\')){
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos+1);
            $filename = str_replace('\\', DIRECTORY_SEPARATOR, $namespace). DIRECTORY_SEPARATOR;
        }

        $filename .= str_replace('_', DIRECTORY_SEPARATOR, $className);
        foreach($this->_ext as $ext){
            if (file_exists($filename.$ext)){
                include_once($filename.$ext);
                return;
            }
        }



        foreach($this->_src as $resource){
            foreach($this->_ext as $ext){
                if (file_exists($resource.DIRECTORY_SEPARATOR.$filename.$ext)){
                    include_once($resource.DIRECTORY_SEPARATOR.$filename.$ext);
                    return;
                }
            }
        }

    }


}