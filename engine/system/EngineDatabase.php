<?php
/**
 * User: Frantzolakis Manolis
 * Date: 29/2/2016
 * Time: 4:17 ��
 */


class EngineDatabase{

    private static $_instance = null;
    /**
     * @var mysqli $sqli
     */
    private $sqli = null;
    private $stmt = null;
    private $query = null;

    public static function getInstance(){
        if (null === self::$_instance){
            self::$_instance = new EngineDatabase();
        }
        return self::$_instance;
    }

    private function __construct(){
        if ($this->sqli == null) {
            $settings = Settings::getSettings();
            $this->DB_HOST = $settings['db_host'];
            $this->DB_USER = $settings['db_user'];
            $this->DB_PASSWORD = $settings['db_password'];
            $this->DB_NAME = $settings['db_name'];

            $this->sqli = new mysqli($this->DB_HOST,$this->DB_USER,$this->DB_PASSWORD,$this->DB_NAME);
            $this->sqli->set_charset('utf8');
            if ($this->sqli->connect_errno){
              //  echo 'Could Not Connect To Database ';
              //  $this->sqli = null;
                throw new Exception ("Could Not Connect To Database ");
            }
        }
    }

    public function getLastInsertId(){
        return $this->sqli->insert_id;
    }

    public function buildUpdateQuery($table, $fields, $where){

        if ($fields == null || empty($where)){
            return false;
        }

        $columns = "";
        $escaping = '';
        $scheme = '';
        $values = array();

        foreach($fields as $index=>$value){

            if ($value === null){
                $columns .= $index .' = null ,';
                continue;
            }
            $columns .= $index .'= ? ,';

            if (gettype($value) == 'string'){
                $scheme .="s";
            }else if (gettype($value) == 'integer'){
                $scheme .="i";
            }else if (gettype($value) == 'double'){
                $scheme .="d";
            }else if (gettype($value) == 'boolean'){
                $scheme .='i';
            }
            $values[] = $value;
        }
        $columns = rtrim($columns,',');

        $whereClause ='';
        foreach($where as $index=>$value){
            $whereClause .= $index.' = ?';
            if (gettype($value) == 'string'){
                $scheme .="s";
            }else if (gettype($value) == 'integer'){
                $scheme .="i";
            }else if (gettype($value) == 'double'){
                $scheme .="d";
            }else if ($value == null){
                $scheme .= "s";
            }
            $values[] = $value;
        }

        $this->query = 'UPDATE '.$table.' SET '.$columns.' where '.$whereClause;
        $this->stmt = $this->sqli->prepare($this->query);
        $this->bind_params($this->stmt, $scheme, $values);
        $this->stmt->execute();
        if ($this->sqli->affected_rows == 1 || $this->sqli->errno == 0){
            $this->stmt->close();
            return true;
        }
        $this->stmt->close();
        return false;
    }

    private function bind_params(){
        $stmt = func_get_arg(0);
        $scheme = func_get_arg(1);
        $a_params = array();
        $a_params[] = &$scheme;
        $a_bind_params = "";
        for($i=2;$i<func_num_args();$i++){
            $a_bind_params = func_get_arg($i);
        }
        for ($i=0;$i<count($a_bind_params);$i++){
            $a_params[] = &$a_bind_params[$i];
        }
        call_user_func_array(array($stmt,'bind_param'),$a_params);
    }

    public function buildSelectQuery($table, $columns, $where, $order = null, $limit = null, $offset = null, $groupBy = null, $having = null){

        if ($columns != null){
            //$tColumns = $columns;
            $tColumns = implode(",",$columns);
        }else {
            $tColumns = "*";
        }

        if ($where != null){
            $relation = "AND";
            if (isset($where['relation']) && strlen(trim($where['relation'])) > 0){
                $relation = $where['relation'];
            }

            $fields = "";

            foreach($where['fields'] as $field){
                if (strpos($field, "LIKE") !== false){
                    $fields .= ' '.$field.' ? '.$relation;
                }else if (strpos($field, '=') !== false){
                    $fields .= ' '.$field.' '.$relation;
                }else {
                    $fields .= ' '.$field.'= ? '.$relation;
                }
            }

            $fields = rtrim($fields, $relation);

            $scheme = "";
            foreach($where['values'] as $value){
                if (gettype($value) == "string"){
                    $scheme .="s";
                }else if (gettype($value) == "integer"){
                    $scheme .="i";
                }else if (gettype($value) == "double"){
                    $scheme .="d";
                }else if (gettype($value) == "boolean"){
                    $scheme .="i";
                }else if ($value == null){
                    $scheme .="s";
                }
            }
        }else {
            $fields = " 1 ";
            $scheme = null;
        }

        $this->query = "SELECT ".$tColumns." from ".$table." where ".$fields;
        if ($groupBy != null){
            $this->query .= ' GROUP BY '.$groupBy;
        }
        if ($having != null){
            $this->query .= ' HAVING '.$having;
        }
        if ($order != null){
            $this->query .= ' ORDER BY '.$order;
        }
        if ($limit != null){
            $this->query .= ' LIMIT '.$limit;
        }
        if ($offset != null){
            $this->query .= ' OFFSET '.$offset;
        }

        //var_dump($this->query);

        $this->stmt = $this->sqli->prepare($this->query);
        if ($scheme != null) {
            $this->bind_params($this->stmt, $scheme, $where['values']);
        }

        $this->stmt->execute();
        $meta = $this->stmt->result_metadata();
        $resFields = $results = array();
        $fieldNames = array();
        while ($resField = $meta->fetch_field()){
            $var = $resField->name;
            $fieldNames[] = $resField->name;
            $$var = null;
            $resFields[$var] = &$$var;
        }

        $fieldCount = count($resFields);

        call_user_func_array(array($this->stmt, 'bind_result'), $resFields);

        $i = 0;

        while ($row = $this->stmt->fetch()){
            for ($l=0;$l<$fieldCount;$l++){
                $results[$i][$fieldNames[$l]] = $resFields[$fieldNames[$l]];
            }
            $i++;
        }

        $this->stmt->close();
        if (count($results) == 0){
            return null;
        }
        return $results;
    }

    public function buildInsertQuery($table, $fields){
        if ($fields == null || empty($fields)){
            return false;
        }

        $columns = '';
        $escaping = '';
        $scheme = '';
        $values = array();

        foreach($fields as $index=>$value){
            $columns .= '`'.$index.'`,';
            $escaping .= '?,';

            if (gettype($value) == 'string'){
                $scheme .="s";
            }else if (gettype($value) == 'integer'){
                $scheme .="i";
            }else if (gettype($value) == 'double' || gettype($value) == "float"){
                $scheme .="d";
            }else if ($value === null){
                $scheme .= "s";
            }
            $values[] = $value;
        }

        $columns = rtrim($columns,',');
        $escaping = rtrim($escaping,',');
        $this->query = "INSERT INTO ".$table." (".$columns.") values (".$escaping.")";

        $this->stmt = $this->sqli->prepare($this->query);

        $this->bind_params($this->stmt, $scheme, $values);

        $this->stmt->execute();

        if ($this->sqli->affected_rows == 1){
            $this->stmt->close();
            return true;
        }
        $this->stmt->close();
        return false;
    }

    public function buildDeleteQuery($table,$where){
        $whereClause ='';
        $scheme = '';
        $values = array();
        foreach($where as $index=>$value){
            $whereClause .= $index.' = ?';
            if (gettype($value) == 'string'){
                $scheme .="s";
            }else if (gettype($value) == 'integer'){
                $scheme .="i";
            }else if (gettype($value) == 'double'){
                $scheme .="d";
            }else if (gettype($value) == 'boolean'){
                $scheme .='i';
            }
            $values[] = $value;
        }
        $this->query = 'DELETE FROM  '.$table.' where '.$whereClause;
        $this->stmt = $this->sqli->prepare($this->query);
        $this->bind_params($this->stmt, $scheme, $values);
        $this->stmt->execute();
        if ($this->sqli->affected_rows == 1){
            $this->stmt->close();
            return true;
        }
        $this->stmt->close();
        return false;
    }

    public function executeRawQuery($sql){
        $this->query = $sql;
        $mResult = array();
        if ($result = $this->sqli->query($this->query)){
            while ($obj = $result->fetch_array()){
                $mResult[] = $obj;
            }
            $result->close();

        }


        return $mResult;

    }

    public function executeRawQuery1($sql){
        $this->query = $sql;
        $this->stmt = $this->sqli->prepare($this->query);
        $this->stmt->execute();
        $result = $this->fetchForRaw(true);
        $this->stmt->close();
        return $result;
    }

    private function fetchForRaw($buffer = true){
        if ($buffer) {
            $this->stmt->store_result();
        }

        $fields = $this->stmt->result_metadata()->fetch_fields();
        $args = array();
        foreach($fields AS $field) {
            $key = str_replace(' ', '_', $field->name); // space may be valid SQL, but not PHP
            $args[$key] = &$field->name; // this way the array key is also preserved
        }
        call_user_func_array(array($this->stmt, "bind_result"), $args);

        $results = array();
        while($this->stmt->fetch()) {
            $results[] = array_map(array($this,'copy_value'), $args);
        }
        if ($buffer) {
            $this->stmt->free_result();
        }
        return $results;
    }

    public function copy_value($v) {
        return $v;
    }
}