<?php
/**
 * User: Frantzolakis Manolis
 * Date: 29/2/2016
 * Time: 4:19 ��
 */

class Settings {


    public static function getSettings() {
        if (isset($_SERVER) && isset($_SERVER['HTTP_HOST'])){
            if (strpos($_SERVER['HTTP_HOST'], 'alina.smartcampaign') !== false){
                return self::getSettings_alina();
            }
            else {
                return self::getSettings_prod();
            }
        }else {
            return self::getSettings_prod();
        }
    }

    protected static function getSettings_alina() {
        return array(
            'db_host' => 'localhost',
            'db_user' => 'root',
            'db_password' => 'root',
            'db_name' => 'smartcampaign',
            'folder' => ''
        );
    }

    protected static function getSettings_prod() {
        return array(
            'db_host' => 'localhost',
            'db_user' => 'openitftp',
            'db_password' => 'IgRbp1e3YcSCjv7O',
            'db_name' => 'openit-smartcampaign',
            'folder' => 'smartcampaign'
        );
    }
}