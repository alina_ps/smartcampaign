<?php
/**
 * User: Frantzolakis Manolis
 * Date: 21/7/2016
 * Time: 12:50 μμ
 */

class Translator
{

    private static $translate = array(
        '' => array(
            'en' => '',
            'el' => ''
        ),
        'Home' => array(
            'en' => 'Home',
            'el' => 'Αρχική'
        ),
        'Patient' => array(
            'en' => 'Patient',
            'el' => 'Ασθενής'
        ),
        'Patients' => array(
            'en' => 'Patients',
            'el' => 'Ασθενείς'
        ),
        'New' => array(
            'en' => 'New',
            'el' => 'Νέος'
        ),
        'Username' => array(
            'en' => 'Username',
            'el' => 'Όνομα Χρήστη'
        ),
        'Password' => array(
            'en' => 'Password',
            'el' => 'Κωδικός'
        ),
        'Login' => array(
            'en' => 'Login',
            'el' => 'Είσοδος'
        ),
        'User' => array(
            'en' => 'User',
            'el' => 'Χρήστης'
        ),
        'Users' => array(
            'en' => 'Users',
            'el' => 'Χρήστες'
        ),
        'Create' => array(
            'en' => 'Create',
            'el' => 'Δημιουργία'
        ),
        'View' => array(
            'en' => 'View',
            'el' => 'Προβολή'
        ),
        'Add' => array(
            'en' => 'Add',
            'el' => 'Προσθήκη'
        ),
        'Logout' => array(
            'en' => 'Logout',
            'el' => 'Έξοδος'
        ),
        'Role' => array(
            'en' => 'Role',
            'el' => 'Ρόλος'
        ),
        'DB ID' => array(
            'en' => 'DB ID',
            'el' => 'ΑΝ. Βάσης'
        ),
        'Yes' => array(
            'en' => 'Yes',
            'el' => 'Ναι'
        ),
        'No' => array(
            'en' => 'No',
            'el' => 'Όχι'
        ),
        'You are not authorized for this resource.' => array(
            'en' => 'You are not authorized for this resource.',
            'el' => 'Δεν έχετε πρόσβαση σε αυτόν το πόρο.'
        ),
        'admin' => array(
            'en' => 'Administrator',
            'el' => 'Διαχειριστής'
        ),
        'user' => array(
            'en' => 'User',
            'el' => 'Χρήστης'
        ),
        'editTooltip' => array(
            'en' => 'Edit %s',
            'el' => 'Επεξεργασία %s'
        ),
        'viewTooltip' => array(
            'en' => 'View %s',
            'el' => 'Προβολή %s'
        ),
        'deleteTooltip' => array(
            'en' => 'Delete %s',
            'el' => 'Διαγραφή %s'
        ),
        'user_gen' => array(
            'en' => 'User',
            'el' => 'Χρήστη'
        ),
        'patients_gen' => array(
            'en' => 'Patients',
            'el' => 'Ασθενών'
        ),
        'patient_gen' => array(
            'en' => 'Patient',
            'el' => 'Ασθενή'
        ),
        'required' => array(
            'en' => 'Required',
            'el' => 'Υποχρεωτικό'
        ),
        'maximum_length' => array(
            'en' => 'Maximum field length is %s characters',
            'el' => 'Μέγιστο εύρος πεδίου %s χαρακτήρες'
        ),
        'select_none' => array(
            'en' => 'Nothing Selected',
            'el' => 'Τίποτα Επιλεγμένο'
        ),
        'required_fields' => array(
            'en' => 'Fields with <span class="required-star">*</span> are required.',
            'el' => 'Τα πεδία με <span class="required-star">*</span> είναι υποχρεωτικά.'
        ),
        'error_202' => array(
            'en' => 'Field %s is not valid.',
            'el' => 'Το πεδίο %s δεν είναι έγκυρο.'
        ),
        'error_201' => array(
            'en' => '%s with %s:%s already exists.',
            'el' => '%s με %s:%s υπάρχει ήδη.'
        ),
        'success_msg_user_create' => array(
            'en' => 'User with username: %s created successfully.<div><a href="%s" >Return to Users view</a></div>',
            'el' => 'Ο χρήστης με όνομα χρήστη: %s δημιουργήθηκε επιτυχώς.<div><a href="%s">Επιστροφή στη Προβολή</a></div>'
        ),
        'error_msg_user_create' => array(
            'en' => 'User with username: %s could not be created. Please try again later.',
            'el' => 'Ο χρήστης με όνομα χρήστη: %s δεν μπόρεσε να δημιουργηθεί. Παρακαλώ δοκιμάστε αργότερα.'
        ),
        'success_msg_patient_create' => array(
            'en' => 'Patient with name: %s created successfully.<div><a href="%s" >Return to Patients view</a></div>',
            'el' => 'Ο ασθενής με όνομα: %s δημιουργήθηκε επιτυχώς.<div><a href="%s">Επιστροφή στη Προβολή</a></div>'
        ),
        'success_msg_patient_update' => array(
            'en' => 'Patient with name: %s updated successfully.<div><a href="%s" >Return to Patients view</a></div>',
            'el' => 'Ο ασθενής με όνομα: %s ενημερώθηκε επιτυχώς.<div><a href="%s">Επιστροφή στη Προβολή</a></div>'
        ),
        'success_msg_examination_create' => array(
            'en' => 'Examination: %s created successfully.<div><a href="%s" >Return to %s view</a></div>',
            'el' => 'Η εξέταση: %s δημιουργήθηκε επιτυχώς.<div><a href="%s">Επιστροφή στη προβολή %s</a></div>'
        ),
        'success_msg_examination_update' => array(
            'en' => 'Examination: %s updated successfully.<div><a href="%s" >Return to %s view</a></div>',
            'el' => 'Η εξέταση: %s ενημερώθηκε επιτυχώς.<div><a href="%s">Επιστροφή στη προβολή %s</a></div>'
        ),
        'error_msg_patient_create' => array(
            'en' => 'Patient with name: %s could not be created. Please try again later.',
            'el' => 'Ο ασθενής με όνομα: %s δεν μπόρεσε να δημιουργηθεί. Παρακαλώ δοκιμάστε αργότερα.'
        ),
        'error_msg_patient_update' => array(
            'en' => 'Patient with name: %s could not be updated. Please try again later.',
            'el' => 'Ο ασθενής με όνομα: %s δεν μπόρεσε να ενημερωθεί. Παρακαλώ δοκιμάστε αργότερα.'
        ),
        'error_msg_examination_update' => array(
            'en' => 'Examination: %s could not be updated. Please try again later.',
            'el' => 'Η εξέταση: %s δεν μπόρεσε να ενημερωθει. Παρακαλώ δοκιμάστε αργότερα.'
        ),
        'error_msg_examination_create' => array(
            'en' => 'Examination: %s could not be created. Please try again later.',
            'el' => 'Η εξέταση: %s δεν μπόρεσε να δημιουργηθεί. Παρακαλώ δοκιμάστε αργότερα.'
        ),
        'user_not_found' => array(
            'en' => 'The specified user does not exist.',
            'el' => 'Ο χρήστης που ζητήσατε δεν υπάρχει.'
        ),
        'examination_not_found' => array(
            'en' => 'The specified examination does not exist.',
            'el' => 'Η εξέταση που ζητήσατε δεν υπάρχει.'
        ),
        'Edit' => array(
            'en' => 'Edit',
            'el' => 'Επεξεργασία'
        ),
        'type_only_password' => array(
            'en' => 'Fill in the password field only if you want to change the password.',
            'el' => 'Συμπληρώστε κωδικό μόνο αν θέλετε να τον αλλάξετε.'
        ),
        'success_msg_user_edit' => array(
            'en' => 'User with username: %s updated successfully.<div><a href="%s" >Return to Users view</a></div>',
            'el' => 'Ο χρήστης με όνομα χρήστη: %s ενημερώθηκε επιτυχώς.<div><a href="%s">Επιστροφή στη Προβολή</a></div>'
        ),
        'error_msg_user_edit' => array(
            'en' => 'User with username %s could not be updated. Please try again later.',
            'el' => 'Ο χρήστης με όνομα χρήστη: %s δεν ενημερώθηκε. Παρακαλώ δοκιμάστε αργότερα.'
        ),
        'success_msg_patient_edit' => array(
            'en' => 'Patient with name: %s updated successfully.<div><a href="%s" >Return to Patients view</a></div>',
            'el' => 'Ο ασθενής με όνομα: %s ενημερώθηκε επιτυχώς.<div><a href="%s">Επιστροφή στη Προβολή</a></div>'
        ),
        'error_msg_patient_edit' => array(
            'en' => 'Patient with name %s could not be updated. Please try again later.',
            'el' => 'Ο ασθενής με όνομα: %s δεν ενημερώθηκε. Παρακαλώ δοκιμάστε αργότερα.'
        ),
        'failed_request' => array(
            'en' => 'Your request failed.',
            'el' => 'Το αίτημα σας απέτυχε.'
        ),
        'empty_field' => array(
            'en' => 'Field can not be empty.',
            'el' => 'Παρακαλώ συμπληρώστε το πεδίο.'
        ),
        'can_not_exceed' => array(
            'en' => 'Field can not exceed %s characters',
            'el' => 'Το πεδίο δεν πρέπει να έχει παραπάνω απο %s χαρακτήρες.'
        ),
        'unique_id' => array(
            'en' => 'Identity Number',
            'el' => 'Α.Δ.Τ'
        ),
        'name' => array(
            'en' => 'Name',
            'el' => 'Όνομα'
        ),
        'father_name' => array(
            'en' => "Father Name",
            'el' => 'Πατρώνυμο'
        ),
        'birthdate' => array(
            'en' => 'Birthdate',
            'el' => 'Ημ. Γέννησης'
        ),
        'insurance_number' => array(
            'en' => 'SSN',
            'el' => 'Α.Μ.Κ.Α'
        ),
        'mobile_phone' => array(
            'en' => 'Mobile Phone',
            'el' => 'Κινητό Τηλ.'
        ),
        'fixed_phone' => array(
            'en' => 'Home Phone',
            'el' => 'Σταθερό Τηλ.'
        ),
        'Patient_gen' => array(
            'en' => 'Patient',
            'el' => 'Ασθενή'
        ),
        'surname' => array(
            'en' => 'Surname',
            'el' => 'Επίθετο'
        ),
        'first_name' => array(
            'en' => 'Name',
            'el' => 'Όνομα'
        ),
        'icn' => array(
            'en' => 'Identity Card',
            'el' => 'Α.Δ.Τ'
        ),
        'Insurance' => array(
            'en' => 'Insurance',
            'el' => 'Ασφάλεια'
        ),
        'Insurance_Number' => array(
            'en' => 'SSN',
            'el' => 'Α.Μ.Κ.Α'
        ),
        'male' => array(
            'en' => 'Male',
            'el' => 'Άρρεν'
        ),
        'female' => array(
            'en' => 'Female',
            'el' => 'Θήλυ'
        ),
        'gender' => array(
            'en' => 'Gender',
            'el' => 'Φύλο'
        ),
        'email' => array(
            'en' => 'Email',
            'el' => 'Email'
        ),
        'blood_type' => array(
            'en' => 'Blood Type',
            'el' => 'Ομάδα Αίματος'
        ),
        'marital_status' => array(
            'en' => 'Marital Status',
            'el' => 'Οικογ. Κατάσταση'
        ),
        'Single' => array(
            'en' => 'Single',
            'el' => 'Άγαμος'
        ),
        'Married' => array(
            'en' => 'Married',
            'el' => 'Έγγαμος'
        ),
        'Divorced' => array(
            'en' => 'Divorced',
            'el' => 'Διαζευγμένος'
        ),
        'Widowed' => array(
            'en' => 'Widowed',
            'el' => 'Σε Χηρεία'
        ),
        'Other' => array(
            'en' => 'Other',
            'el' => 'Άλλο'
        ),
        'address' => array(
            'en' => 'Address',
            'el' => 'Διεύθυνση'
        ),
        'recommended_doctor' => array(
            'en' => 'Recommended Doctor',
            'el' => 'Συστήσας Γιατρός'
        ),
        'citizenship' => array(
            'en' => 'Citizenship',
            'el' => 'Υπηκοότητα'
        ),
        'spouse_name' => array(
            'en' => 'Spouse Name',
            'el' => 'Όνομα Συζύγου'
        ),
        'demographics' => array(
            'en' => 'Demographics',
            'el' => 'Δημογραφικά'
        ),
        'examinations' => array(
            'en' => 'Examinations',
            'el' => 'Εξετάσεις'
        ),
        'Examination' => array(
            'en' => 'Examination',
            'el' => 'Εξέτασης'
        ),
        'spouse' => array(
            'en' => 'Spouse',
            'el' => 'Σύζυγος'
        ),
        'Spouse_Information' => array(
            'en' => 'Spouse Information',
            'el' => 'Στοιχεία Συζύγου'
        ),
        'spouse_not_found' => array(
            'en' => 'Spouse information not found.',
            'el' => 'Δεν βρέθηκαν στοιχεία συζύγου.'
        ),
        'patient_not_found' => array(
            'en' => 'Patient not found.',
            'el' => 'Ο Ασθενής δεν βρέθηκε.'
        ),
        'Sperm_Refrigeration' => array(
            'en' => 'Sperm Refrigeration',
            'el' => 'Κατάψυξη Σπέρματος'
        ),
        'Oocyte_Vitrification' => array(
            'en' => 'Oocyte Vitrification',
            'el' => 'Υαλοποίηση Ωαρίων'
        ),
        'Embryo_Vitrification' => array(
            'en' => 'Embryo Vitrification',
            'el' => 'Υαλοποίηση Εμβρύων'
        ),
        'date_done' => array(
            'en' => 'Sample Day/Hour',
            'el' => 'Ημ. Δείγματος/Ώρα'
        ),
        'date_frozen' => array(
            'en' => 'Date Refrigerated',
            'el' => 'Ημ. Κατάψυξης'
        ),
        'volume' => array(
            'en' => 'Volume',
            'el' => 'Όγκος'
        ),
        'total_number' => array(
            'en' => 'Total Number',
            'el' => 'Ολικός Αριθμός'
        ),
        'analysis_done' => array(
            'en' => 'Sample Analysis',
            'el' => 'Ανάλυση Δείγματος'
        ),
        'motility' => array(
            'en' => '% Motility',
            'el' => '% Κινητικότητα'
        ),
        'storage_position' => array(
            'en' => 'Storage Position',
            'el' => 'Θέση Φύλαξης'
        ),
        'eggcollection_date' => array(
            'en' => 'Egg Collection Date',
            'el' => 'Ημ. Ωοληψίας'
        ),
        'vitrification_date' => array(
            'en' => 'Vitrification Date',
            'el' => 'Ημ. Υαλοποίησης'
        ),
        'thaw_time' => array(
            'en' => 'Thawing Time',
            'el' => 'Ώρα Απόψυξης'
        ),
        'number_thawed_ova' => array(
            'en' => 'Eggs Thawed',
            'el' => 'Αποψυχθέντα Ωάρια'
        ),
        'number_survived_ova' => array(
            'en' => 'Survived Eggs',
            'el' => 'Επιβιώσαντα Ωάρια'
        ),
        'embryo_transfer_date' => array(
            'en' => 'Embryo Transfer Date',
            'el' => 'Ημ. Εμβρυομεταφοράς'
        ),
        'refrigerated_embryos' => array(
            'en' => 'Refrigerated Embryos',
            'el' => 'Καταψυχθέντα Έμβρυα'
        ),
        'number_thawed_embryos' => array(
            'en' => 'Thawed Embryos',
            'el' => 'Αποψυχθέντα Έμβρυα'
        ),
        'survived_embryos' => array(
            'en' => '# Survived Embryos',
            'el' => '# Επιβιώσαντα Έμβρυα'
        ),
        'Select_Examination' => array(
            'en' => 'Select Examination',
            'el' => 'Επιλογή Εξέτασης'
        ),
        'abstinence' => array(
            'en' => 'Abstinence',
            'el' => 'Αποχή'
        ),
        'fluidity' => array(
            'en' => 'Fluidity',
            'el' => 'Ρευστοποίηση'
        ),
        'number_sperm' => array(
            'en' => 'Sperm Count/ml',
            'el' => 'Αρ. Σπερ/ρίων /ml'
        ),
        'A' => array(
            'en' => 'A',
            'el' => 'Α'
        ),
        'B' => array(
            'en' => 'B',
            'el' => 'Β'
        ),
        'C' => array(
            'en' => 'C',
            'el' => 'Γ'
        ),
        'motility_type' => array(
            'en' => 'Motility Type',
            'el' => 'Τύπος Κινητικότητας'
        ),
		'refrig_type' => array(
            'en' => 'Refrigeration Type',
            'el' => 'Τύπος Κατάψυξης'
        ),
        'vitality' => array(
            'en' => '% Vitality',
            'el' => '% Ζωτικότητα'
        ),
        'vial' => array(
            'en' => 'Vial',
            'el' => 'Φιάλη'
        ),
        'position' => array(
            'en' => 'Position',
            'el' => 'Θέση'
        ),
        'drawer' => array(
            'en' => 'Drawer',
            'el' => 'Συρτάρι'
        ),
        'plastic_color' => array(
            'en' => 'Plastic Color',
            'el' => 'Χρώμα Πλαστικού'
        ),
        'total_samples' => array(
            'en' => 'Total Samples',
            'el' => 'Συνολ. Αρ. Δειγμάτων'
        ),
        'Ova_Vitrification' => array(
            'en' => 'Ova Vitrification',
            'el' => 'Υαλοποίηση Ωαρίων'
        ),
        'date_egg_collection' => array(
            'en' => 'Egg Collection Date',
            'el' => 'Ημ. Ωοληψίας'
        ),
        'date_vitrification' => array(
            'en' => 'Vitrification Date',
            'el' => 'Ημ. Υαλοποίησης'
        ),
        'vitrification_liquid_type' => array(
            'en' => 'Vitrification Liquid',
            'el' => 'Τύπος Καλ/κού Υγρού'
        ),
        'vitrification_straws_type' => array(
            'en' => 'Straw Type',
            'el' => 'Τύπος straws'
        ),
        'number_of_eggs_frozen' => array(
            'en' => '# Frozen Eggs',
            'el' => '# Κατεψυγμένων Ωαρίων'
        ),
        'number_of_straws' => array(
            'en' => '# Straws',
            'el' => '# Straws'
        ),
        'eggs_per_straw' => array(
            'en' => '# Eggs/Straw',
            'el' => '# Ωαρίων/Straw'
        ),
        'thawing_done' => array(
            'en' => 'Thawing Ovas',
            'el' => 'Απόψυξη Ωαριών'
        ),
        'thawing_liquid_type' => array(
            'en' => 'Thawing Liquid Type',
            'el' => 'Τύπος Καλ/κού Υγρού Απόψυξης'
        ),
        'thawing_hour' => array(
            'en' => 'Thawing Hour',
            'el' => 'Ώρα Απόψυξης'
        ),
        'thawed_ovas' => array(
            'en' => '# Thawed Ovas',
            'el' => '# Ωαρίων που Αποψύχθηκαν'
        ),
        'remaining_ovas' => array(
            'en' => '# Remaining Ovas',
            'el' => '# Υπόλοιπο Ωαρίων'
        ),
        'survived_ovas' => array(
            'en' => '# Survived Ovas',
            'el' => '# Ωαρίων που Επιβίωσαν'
        ),
        'survival_percentage' => array(
            'en' => 'Survival Perc.',
            'el' => 'Ποσοστό Επιβίωσης'
        ),
        'fertilized_embryos' => array(
            'en' => '# Fertilized Embryos',
            'el' => '# Γονιμοποιήμένων Ωαρίων'
        ),
        'divided_embryos' => array(
            'en' => '# Divided Embryos',
            'el' => '# Διαιρεμένων Εμβρύων'
        ),
        'catheter_type_embryo_transfer' => array(
            'en' => 'Embryo Transfer Catheter',
            'el' => 'Καθετήρας Εμβρυομεταφοράς'
        ),
        'transfered_embryos' => array(
            'en' => '# Transferred Embryos',
            'el' => '# Μεταφερούμενων Εμβρύων'
        ),
        'bchg_result' => array(
            'en' => 'b HCG Result',
            'el' => 'Αποτέλεσμα b HCG'
        ),
        'Positive' => array(
            'en' => 'Positive',
            'el' => 'Θετικό'
        ),
        'Negative' => array(
            'en' => 'Negative',
            'el' => 'Αρνητικό'
        ),
        'Embryos_Vitrification' => array(
            'en' => 'Embryos Vitrification',
            'el' => 'Υαλοποίηση Εμβρύων'
        ),
        'frozen_embryos' => array(
            'en' => '# Frozen Embryos',
            'el' => '# Κατεψυγμένων Εμβρύων'
        ),
        'embryos_per_straws' => array(
            'en' => '# Embryos/Straw',
            'el' => '# Εμβρύων/Straw'
        ),
        'embryo_development_stage' => array(
            'en' => 'Embryo Development Stage',
            'el' => 'Στάδιο Ανάπτυξης Εμβρύων'
        ),
        'thawing_embryo_done' => array(
            'en' => 'Embryo Thawing',
            'el' => 'Απόψυξη Εμβρύων'
        ),
        'thawed_embryos' => array(
            'en' => '# Thawed Embryos',
            'el' => '# Εμβρύων που Αποψύχθηκαν'
        ),
		'remaining_embryos' => array(
            'en' => 'Remaining Embryos',
            'el' => 'Υπόλοιπο Έμβρύων'
        ),
        'before_transfer_time' => array(
            'en' => 'Cultivation Time',
            'el' => 'Χρόνος Καλλιέργειας'
        ),
        'Find_Spouse' => array(
            'en' => 'Find Spouse',
            'el' => 'Εύρεση Συζύγου'
        ),
        'Remove_Spouse' => array(
            'en' => 'Remove Spouse',
            'el' => 'Αφαίρεση'
        ),
        'Update' => array(
            'en' => 'Update',
            'el' => 'Ανανέωση'
        ),
        'Not_Available' => array(
            'en' => 'NA',
            'el' => 'ΜΔ'
        ),
        'Sample_Analysis' => array(
            'en' => 'Sample Analysis',
            'el' => 'Ανάλυση Δείγματος'
        ),
        'Sample_Analysis_After' => array(
            'en' => 'Sample Analysis After Thawing',
            'el' => 'Ανάλυση Δείγματος Μετά Από Απόψυξη'
        ),
        'days' => array(
            'en' => 'Days',
            'el' => 'Ημέρες'
        ),
		'Blood_Examination' => array(
            'en' => 'Blood Examination',
            'el' => 'Εξέταση Αίματος'
        ),
		'exam_date' => array(
            'en' => 'Date',
            'el' => 'Ημερομηνία'
        ),
		'hcv' => array(
            'en' => 'HCV',
            'el' => 'HCV'
        ),
		'hiv1' => array(
            'en' => 'HIV 1',
            'el' => 'HIV 1'
        ),
		'hiv2' => array(
            'en' => 'HIV 2',
            'el' => 'HIV 2'
        ),
		'hbsag' => array(
            'en' => 'Hbs Ag',
            'el' => 'Hbs Ag'
        ),
		'syphilis' => array(
            'en' => 'Syphilis',
            'el' => 'Σύφιλη'
        ),
		'Med_History' => array(
            'en' => 'Medical History',
            'el' => 'Ιατρικό Ιστορικό'
        ),
		'woman_name' => array(
            'en' => 'Patient',
            'el' => 'Ασθενής'
        ),
		'woman_details' => array(
            'en' => 'Woman Details',
            'el' => 'Γυναίκα'
        ),
		'man_name' => array(
            'en' => 'Spouse',
            'el' => 'Σύζηγος Ασθενή'
        ),
		'man_details' => array(
            'en' => 'Man Details',
            'el' => 'Άνδρας'
        ),
		'date_done' => array(
            'en' => 'Date',
            'el' => 'Ημερομηνία'
        ),
		'wter' => array(
            'en' => 'TER',
            'el' => 'ΤΕΡ'
        ),
		'wcircle' => array(
            'en' => 'Cycle',
            'el' => 'Κύκλος'
        ),
		'wcause' => array(
            'en' => 'Visit reason',
            'el' => 'Αιτία Προσέλευσης'
        ),
		'whistory' => array(
            'en' => 'Gynecological History',
            'el' => 'Γυναικολογικό Ιστορικό'
        ),
		'wpap' => array(
            'en' => 'PAP TEST',
            'el' => 'PAP ΤΕΣΤ'
        ),
		'wmenses_start' => array(
            'en' => 'Menses start',
            'el' => 'Εμμηναρχή'
        ),
		'wdysmenses_1929' => array(
            'en' => 'Dysmenorrhea 19th-29th',
            'el' => 'Δυσμηνόρροια 19ης-29ης'
        ),
		'whalf_circle' => array(
            'en' => 'Half Cycle',
            'el' => 'Αιμόρροια μετά επαφή - Μέσο Κύκλου'
        ),
		'wdyspareunia' => array(
            'en' => 'Dyspareunia',
            'el' => 'Δυσπαρεύνια'
        ),
		'werythras' => array(
            'en' => 'Rubella Ant.',
            'el' => 'Αντ. Ερυθράς'
        ),
		'wcontraception' => array(
            'en' => 'Contraception',
            'el' => 'Αντισύλληψη'
        ),
		'wsterility_history' => array(
            'en' => 'Sterility History',
            'el' => 'Ιστορικό Στειρότητας'
        ),
		'wyears' => array(
            'en' => 'Attempts Years',
            'el' => 'Έτη που προσπαθεί'
        ),
		'wexams_done' => array(
            'en' => 'Exams Done',
            'el' => 'Εξετάσεις που έχουν γίνει'
        ),
		'wobstetric_history' => array(
            'en' => 'Obstetric History',
            'el' => 'Μαιευτικό Ιστορικό'
        ),
		'wmed_history' => array(
            'en' => 'Medical History',
            'el' => 'Ιατρικό Ιστορικό'
        ),
		'wmedicine' => array(
            'en' => 'Medicine',
            'el' => 'Φάρμακα'
        ),
		'walcohol' => array(
            'en' => 'Alcohol',
            'el' => 'Αλκοόλ'
        ),
		'wallergies' => array(
            'en' => 'Allergies',
            'el' => 'Αλλεργίες'
        ),
		'wtobacco' => array(
            'en' => 'Tobacco',
            'el' => 'Καπνός'
        ),
		'wweight' => array(
            'en' => 'Weight',
            'el' => 'Βάρος'
        ),
		'wexams' => array(
            'en' => 'Εξετάσεις',
            'el' => 'Exams'
        ),
		'wconclusions' => array(
            'en' => 'Conclusions',
            'el' => 'Συμπεράσματα'
        ),
		'wdecision' => array(
            'en' => 'Doc. Decision',
            'el' => 'Απόφαση Ιατρού'
        ),
		'mfertility_history' => array(
            'en' => 'Fertility History',
            'el' => 'Ιστορικό Γονιμότητας/Αναπαραγωγής'
        ),
		'mcontact_history' => array(
            'en' => 'Contact History',
            'el' => 'Ιστορικό Επαφών'
        ),
		'mspermodiagram' => array(
            'en' => 'Spermodiagram',
            'el' => 'Σπερμοδιάγραμμα'
        ),
		'mmed_history' => array(
            'en' => 'Medical History',
            'el' => 'Ιατρικό Ιστορικό'
        ),
		'mmedicine' => array(
            'en' => 'Medicine',
            'el' => 'Φάρμακα'
        ),
		'malcohol' => array(
            'en' => 'Alcohol',
            'el' => 'Αλκοόλ'
        ),
		'mallergies' => array(
            'en' => 'Allergies',
            'el' => 'Αλλεργίες'
        ),
		'mtobacco' => array(
            'en' => 'Tobacco',
            'el' => 'Καπνός'
        ),
		'mweight' => array(
            'en' => 'Weight',
            'el' => 'Βάρος'
        ),
		'mexams' => array(
            'en' => 'Εξετάσεις',
            'el' => 'Exams'
        ),
		'mconclusions' => array(
            'en' => 'Conclusions',
            'el' => 'Συμπεράσματα'
        ),
		'mdecision' => array(
            'en' => 'Doc. Decision',
            'el' => 'Απόφαση Ιατρού'
        )
		
		
		
		
    );

    public static $colors = array(
        'black' => array(
            'en' => 'Black',
            'el' => 'Μαύρο'
        ),
        'white' => array(
            'en' => 'White',
            'el' => 'Λευκό'
        ),
        'transparent' => array(
            'en' => 'Transparent',
            'el' => 'Διάφανο'
        ),
        'red' => array(
            'en' => 'Red',
            'el' => 'Κόκκινο'
        ),
        'blue' => array(
            'en' => 'Blue',
            'el' => 'Μπλε'
        ),
		'purple' => array(
            'en' => 'Purple',
            'el' => 'Μωβ'
        ),
		'orange' => array(
            'en' => 'Orange',
            'el' => 'Πορτοκαλί'
        ),
		'green' => array(
            'en' => 'Green',
            'el' => 'Πράσινο'
        ),
		'grey' => array(
            'en' => 'Grey',
            'el' => 'Γκρι'
        ),
		'brown' => array(
            'en' => 'Brown',
            'el' => 'Καφέ'
        )
		
    );

    public static function getTranslation($key){
        return Translator::$translate[$key][Engine::$LANGUAGE];
    }

    public static function getFormattedTranslation($key, $test){
        $return = Translator::$translate[$key][Engine::$LANGUAGE];

        $return = sprintf($return, $test);

        return $return;
    }

    public static function getColors(){
        $colors = array();
        foreach (self::$colors as $index=>$value){
            $colors[$index] = $value[Engine::$LANGUAGE];
        }
        return $colors;
    }

}