<?php
/**
 * User: Frantzolakis Manolis
 * Date: 29/2/2016
 * Time: 1:47 ��
 */


class Engine {

    private static $PROTOCOL;
    private static $HOST_ATTRIBUTE;
    public static $LANGUAGE = "el";

    /**
     * Main function that starts our engine, initializes and loads configurations and classes.
     *
     * @param boolean $shouldKick Flag that shows if we should start the UI.
     * @param array $autoloaderPaths Array that holds the paths for the autoloader. Pass null for default paths.
     */
    public function run($shouldKick, $autoloaderPaths = null){

        $this->_initializeConfiguration($shouldKick);
        $this->_initializeAutoloader($autoloaderPaths);
        $this->_initializeSession();
        $this->_initializeError();

        if ($shouldKick){
            $this->_initializeRouter();
            //$this->_initializeGateways();
            $this->_kickOff();
        }

    }

    /**
     * Initializes the base configuration of engine such as protocol, paths etc.
     *
     * @param boolean $kickOff Flag to tell the engine if we have a ui/routing request or cron request.
     */
    private function _initializeConfiguration($kickOff){

        mb_internal_encoding("utf-8");
        //if ($kickOff){
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? self::$PROTOCOL = "https": self::$PROTOCOL = "http";
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? self::$HOST_ATTRIBUTE = "HTTPS_HOST": self::$HOST_ATTRIBUTE = "HTTP_HOST";
       /* }else{
            self::$PROTOCOL = "https";
            self::$HOST_ATTRIBUTE = "HTTPS_HOST";
        }*/
        define ("ENGINE_ROOT_PATH", str_replace("\\", "/", dirname(dirname(dirname(__FILE__))))."/");
        define ("ENGINE_CONFIGURATION_PATH", ENGINE_ROOT_PATH."configuration/");
        define ("ENGINE_PATH", str_replace("\\", "/", dirname(dirname(dirname(__FILE__))))."/engine/");
        define ("ENGINE_SYSTEM_PATH", ENGINE_PATH."system/");

        require_once (ENGINE_CONFIGURATION_PATH."engine.config.php");

        define ("APPLICATION_ROOT_PATH", ENGINE_ROOT_PATH.APP_FOLDER.'/');
        define ("APPLICATION_VIEWS_PATH", APPLICATION_ROOT_PATH."views/");
        define ("APPLICATION_LAYOUT_PATH", APPLICATION_VIEWS_PATH."layout/");

        require_once (ENGINE_CONFIGURATION_PATH."application.config.php");
        require_once (ENGINE_SYSTEM_PATH."Settings.php");
        $settings = Settings::getSettings();
        if ($kickOff) {
            if ($settings['folder'] === ''){
                define('ENGINE_BASEURL', self::$PROTOCOL."://{$_SERVER['HTTP_HOST']}/".$settings['folder']);
            }else {
                define('ENGINE_BASEURL', self::$PROTOCOL."://{$_SERVER['HTTP_HOST']}/".$settings['folder']."/");
            }
        }else {
            if ($settings['folder'] === '') {
                define('ENGINE_BASEURL', "https://www.openit.gr/".$settings['folder']);
            }else {
                define('ENGINE_BASEURL', "https://www.openit.gr/".$settings['folder']."/");
            }
        }

        define ("ENGINE_ASSETS_URL", ENGINE_BASEURL."assets/");

        define ("ENGINE_STYLES_URLS", ENGINE_ASSETS_URL."css/");
        define ("ENGINE_STYLES_LIB_URLS", ENGINE_STYLES_URLS."libs/");
        define ("ENGINE_STYLES_PLUGIN_URLS", ENGINE_STYLES_URLS."plugins/" );
        define ("ENGINE_STYLE_BOOTSTRAP_URL", ENGINE_STYLES_LIB_URLS."bootstrap/");

        define ("ENGINE_SCRIPTS_URLS", ENGINE_ASSETS_URL."js/");
        define ("ENGINE_SCRIPTS_LIB_URLS", ENGINE_SCRIPTS_URLS."libs/");
        define ("ENGINE_SCRIPTS_PLUGIN_URLS", ENGINE_SCRIPTS_URLS."plugins/");
        define ("ENGINE_SCRIPTS_BOOTSTRAP_URL", ENGINE_SCRIPTS_LIB_URLS."bootstrap/");

        define ("APPLICATION_STYLES_URL", ENGINE_BASEURL.APP_FOLDER."/assets/css/");
        define ("APPLICATION_IMAGES_URL", ENGINE_BASEURL.APP_FOLDER."/assets/imgs/");
        define ("APPLICATION_SCRIPTS_URL", ENGINE_BASEURL.APP_FOLDER."/assets/js/");
    }


    /**
     * Initialises the autoloader of the engine.
     *
     * @param $paths Array of strings from existing folders with classes.
     */
    private function _initializeAutoloader($paths){
        require_once ('Autoloader.class.php');
        if ($paths == null) {
            $paths = $this->buildAutoloaderPaths();
            autoloader::init($paths);
        }else {
            autoloader::init($paths);
        }
    }

    /**
     * Builds the array of main paths for the autoloader. If you need another folder to be able
     * to autoload then enter the path in the array.
     *
     * @return array from strings of main classes paths.
     */
    private function buildAutoloaderPaths(){

        return array(
            ENGINE_PATH."system",
            ENGINE_PATH."base",
            ENGINE_PATH."external",
            APPLICATION_ROOT_PATH."controllers",
            APPLICATION_ROOT_PATH."models",
            APPLICATION_ROOT_PATH."external"
        );

    }

    /**
     * Initializes our Session.
     */
    private function _initializeSession(){

        EngineSession::getInstance();

    }


    /**
     * Initializes our error strings.
     */
    private function _initializeError(){
        EngineError::initialize();
    }

    public static function getProtocol(){
        return self::$PROTOCOL;
    }

    /**
     * Initializes our router object. Responsible for routing requests.
     */
    private function _initializeRouter(){

        EngineRouter::getInstance();

    }

    private function _kickOff(){
        $controller = ucfirst(EngineRouter::getController());
        $action = EngineRouter::getAction();

        $controllerName = APP_FOLDER."\\controllers\\".$controller;
        //var_dump($controllerName);
        if (!class_exists($controllerName)){
            //var_dump($controllerName);
            //Engine::redirect(Engine::url(array('controller'=>'index','action'=>'fault')));
            exit;
        }

        $cControl = new $controllerName;

        if (!method_exists($cControl, $action)){
           // Engine::redirect(Engine::url(array('controller'=>'index','action'=>'fault')));
            exit;
        }
        if (Engine::$LANGUAGE === "en"){
            define('ENGINE_LANGUAGE', "en-US");
        }else if (Engine::$LANGUAGE === "el"){
            define('ENGINE_LANGUAGE', "el-GR");
        }

        define('NONE_SELECTED_TEXT', Translator::getTranslation("select_none"));

        define('ENGINE_AJAXURL', Engine::url(array('controller'=>$controller)));
        $cControl->$action(array('_get_params'=>EngineRouter::getArguments(), '_request_params' => EngineRouter::getRequestArguments()));

    }

    public static function redirect($url){
        isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? $protocol = 'https' : $protocol = 'http';
        strpos($url, 'http') !== false OR $url = "{$protocol}://{$url}";
        header("location:$url");
        exit;
    }

    public static function url($args = array()) {
        $url = array();
        $arguments = array();
        foreach($args as $key => $value){
            if($key == 'controller'){ if($value) $url[1] = $value; }
            else if($key == 'action'){ if ($value) $url[2] = $value; }
            else if ($key == 'lang'){ if ($value) $url[0] = $value;}
            else { $arguments[] = "{$key}:{$value}"; }
        }
        if (!isset($args['lang'])){
            $url[0] = Engine::$LANGUAGE;
        }
        ksort($url);
        $url[] = implode(",", $arguments); $url = rtrim(implode("/", $url), "/");

        if ($url=='index/index'){
            $url='';
        }else if ($url == "en/index/index"){
            $url = '';
        }else if ($url == "el/index/index"){
            $url = "el";
        }
        return ENGINE_BASEURL.$url;
    }

    public static function getCurrentController1(){
        return EngineRouter::getController();
    }

    public static function getCurrentAction(){
        return EngineRouter::getAction();
    }

    public static function getArguments(){
        return EngineRouter::getArguments();
    }

    public static function getCurrentController(){
        return isset($_GET['controller'])?$_GET['controller']:'index';
    }

    public static function setResponseContent(){
        header('Accept: application/json');
        header('Content-Type: application/json; charset=utf-8');
    }
}