<?php
/**
 * User: Frantzolakis Manolis
 * Date: 29/2/2016
 * Time: 1:57 ��
 */


class EngineRouter {

    protected static $_instance = null;

    protected static $_items = array(
        '_request_uri' => null,
        '_request_method' => 'GET',
        '_request_arguments' => array(),
        '_controller' => 'index',
        '_action' => 'index',
        '_args' => array()
    );

    public function __construct(){}
    private function __clone(){}

    public static function getInstance($options = null){
        if (!self::$_instance){
            self::_initialize();
            self::$_instance = new static();
        }
        return self::$_instance;
    }

    protected static function _initialize(){

        $server_protocol = Engine::getProtocol();
        if (isset($_SERVER['REQUEST_URI']) && trim(substr($_SERVER['REQUEST_URI'],1))){
            $request_URI = trim(substr($_SERVER['REQUEST_URI'],1));

            $parts = explode('/', $request_URI);
            $parts = array_slice($parts, 0, 6, true);

            if (count($parts) == 1 && !$parts[0]){
                $parts = array();
            }

            if (!empty($_GET)){
                self::$_items['_request_arguments'] = $_GET;
            }

            if(count($parts)){
                $parts[count($parts)-1] = str_replace('?'.$_SERVER['QUERY_STRING'], '', $parts[count($parts) - 1]);
            }

            $settings = Settings::getSettings();
            //var_dump(count($parts));
            //var_dump($parts);
            switch(count($parts)){
                case 1:
                    self::$_items['_controller'] = 'index';
                    self::$_items['_action'] = 'index';

                    self::determineEngineLanguage($parts[0]);

                    $pairs = explode(',', urldecode($parts[0]));
                    if (count($pairs) == 1){
                        self::$_items['_args'] = array();
                    }else{
                        foreach ($pairs as $p) {
                            $p1 = urldecode($p);
                            $key_value = explode(':', $p1);
                            if (sizeof($key_value) > 1) {
                                if (strpos($key_value[0],'[]') !== false){
                                    $key_value[0] = trim($key_value[0],'[]');
                                    self::$_items['_args'][$key_value[0]][] = urldecode($key_value[1]);
                                }else {
                                    self::$_items['_args'][$key_value[0]] = urldecode($key_value[1]);
                                }
                            }
                            else {
                                self::$_items['_args'][$key_value[0]] = '';
                            }
                        }
                    }
                    break;
                case 2:
                    if ($settings['folder'] !== ''){
                        self::$_items['_controller'] = "index";
                        //self::$_items['_action'] = $parts[1];
                        //self::$_items['_action'] = "index";
                        self::determineEngineLanguage($parts[1]);
                    }else {
                        if ($parts[1] === ""){
                            self::$_items['_controller'] = "index";
                        } else {
                            self::$_items['_controller'] = $parts[1];
                        }
                        //self::$_items['_action'] = $parts[1];
                        self::$_items['_action'] = "index";
                        self::determineEngineLanguage($parts[0]);
                    }
                    break;
                case 3:
                    if ($settings['folder'] !== ''){
                        self::$_items['_controller'] = $parts[2];
                        self::$_items['_action'] = 'index';
                        self::determineEngineLanguage($parts[1]);
                        $pairs = explode(',', urldecode($parts[2]));
                        foreach ($pairs as $p) {
                            $p1 = urldecode($p);
                            $key_value = explode(':', $p1);
                            if (sizeof($key_value) > 1) {
                                if (strpos($key_value[0],'[]') !== false){
                                    $key_value[0] = trim($key_value[0],'[]');
                                    self::$_items['_args'][$key_value[0]][] = urldecode($key_value[1]);
                                }else {
                                    self::$_items['_args'][$key_value[0]] = urldecode($key_value[1]);
                                }
                            }
                            else {
                                self::$_items['_args'][$key_value[0]] = '';
                            }
                        }
                    }else{
                        self::$_items['_controller'] = $parts[1];
                        self::$_items['_action'] = $parts[2];
                        self::determineEngineLanguage($parts[0]);
                        $pairs = explode(',', urldecode($parts[2]));
                        if (count($pairs) == 1){
                            self::$_items['_args'] = array();
                        }else {
                            foreach ($pairs as $p) {
                                $p1 = urldecode($p);
                                $key_value = explode(':', $p1);
                                if (sizeof($key_value) > 1) {
                                    if (strpos($key_value[0],'[]') !== false){
                                        $key_value[0] = trim($key_value[0],'[]');
                                        self::$_items['_args'][$key_value[0]][] = urldecode($key_value[1]);
                                    }else {
                                        self::$_items['_args'][$key_value[0]] = urldecode($key_value[1]);
                                    }
                                }
                                else {
                                    self::$_items['_args'][$key_value[0]] = '';
                                }
                            }
                        }
                    }
                    break;
                case 4:
                    if ($settings['folder'] !== ''){
                        self::$_items['_controller'] = $parts[2];
                        self::$_items['_action'] = $parts[3];
                        self::determineEngineLanguage($parts[1]);
                        $pairs = explode(',', urldecode($parts[3]));
                        foreach ($pairs as $p) {
                            $p1 = urldecode($p);
                            $key_value = explode(':', $p1);
                            if (sizeof($key_value) > 1) {
                                if (strpos($key_value[0],'[]') !== false){
                                    $key_value[0] = trim($key_value[0],'[]');
                                    self::$_items['_args'][$key_value[0]][] = urldecode($key_value[1]);
                                }else {
                                    self::$_items['_args'][$key_value[0]] = urldecode($key_value[1]);
                                }
                            }
                            else {
                                self::$_items['_args'][$key_value[0]] = '';
                            }
                        }
                    }else{
                        self::$_items['_controller'] = $parts[1];
                        self::$_items['_action'] = $parts[2];
                        self::determineEngineLanguage($parts[0]);
                        $pairs = explode(',', urldecode($parts[3]));
                        foreach ($pairs as $p) {
                            $p1 = urldecode($p);
                            $key_value = explode(':', $p1);
                            if (sizeof($key_value) > 1) {
                                if (strpos($key_value[0],'[]') !== false){
                                    $key_value[0] = trim($key_value[0],'[]');
                                    self::$_items['_args'][$key_value[0]][] = urldecode($key_value[1]);
                                }else {
                                    self::$_items['_args'][$key_value[0]] = urldecode($key_value[1]);
                                }
                            }
                            else {
                                self::$_items['_args'][$key_value[0]] = '';
                            }
                        }
                    }
                    break;
                case 5:
                    self::$_items['_controller'] = $parts[2];
                    self::$_items['_action'] = $parts[3];
                    self::determineEngineLanguage($parts[1]);
                    $pairs = explode(',', urldecode($parts[4]));
                    foreach ($pairs as $p) {
                        $p1 = urldecode($p);
                        $key_value = explode(':', $p1);
                        if (sizeof($key_value) > 1) {
                            if (strpos($key_value[0],'[]') !== false){
                                $key_value[0] = trim($key_value[0],'[]');
                                self::$_items['_args'][$key_value[0]][] = urldecode($key_value[1]);
                            }else {
                                self::$_items['_args'][$key_value[0]] = urldecode($key_value[1]);
                            }
                        }
                        else {
                            self::$_items['_args'][$key_value[0]] = '';
                        }
                    }
                    break;
                case 6:
                    self::$_items['_controller'] = $parts[2];
                    self::$_items['_action'] = $parts[3];
                    self::determineEngineLanguage($parts[1]);
                    $pairs = explode(',', urldecode($parts[4]));
                    foreach ($pairs as $p) {
                        $p1 = urldecode($p);
                        $key_value = explode(':', $p1);
                        if (sizeof($key_value) > 1) {
                            if (strpos($key_value[0],'[]') !== false){
                                $key_value[0] = trim($key_value[0],'[]');
                                self::$_items['_args'][$key_value[0]][] = urldecode($key_value[1]);
                            }else {
                                self::$_items['_args'][$key_value[0]] = urldecode($key_value[1]);
                            }
                        }
                        else {
                            self::$_items['_args'][$key_value[0]] = '';
                        }
                    }
                    break;
                default:
                    self::$_items['_controller'] = 'index';
                    self::$_items['_action'] = 'fault';
                    break;
            }
        }else {
            Engine::$LANGUAGE = "el";
        }
        //var_dump(self::$_items);
        //die();
      /*  var_dump(self::$_items);
        die();*/
    }

    public static function getController(){
        if(!self::$_instance){
            throw new Exception("Router is not properly instantiated.");
        }
        return self::$_items['_controller'];
    }

    public static function getAction(){
        if(!self::$_instance){
            throw new Exception("Router is not properly instantiated.");
        }
        return self::$_items['_action'];
    }

    public static function getArguments(){
        if(!self::$_instance){
            throw new Exception("Router is not properly instantiated.");
        }

        return self::$_items['_args'];
    }

    public static function getRequestArguments(){
        if(!self::$_instance){
            throw new Exception("Router is not properly instantiated.");
        }

        return self::$_items['_request_arguments'];
    }

    private static function determineEngineLanguage($part){
        if ($part === "en" || $part === "el"){
            Engine::$LANGUAGE = $part;
        }else {
            Engine::$LANGUAGE = "el";
        }
    }
}