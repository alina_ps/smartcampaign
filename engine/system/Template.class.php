<?php
/**
 * User: Frantzolakis Manolis
 * Date: 29/2/2016
 * Time: 2:54 ��
 */

class Template {

    private $_variables = array();
    private $_layout = "layout.php";
    private $_header = "";
    private $_footer = "";

    private static $_instance = null;

    public static function getInstance(){
        if (null === self::$_instance){
            self::$_instance = new Template();
        }
        return self::$_instance;
    }

    private function __construct() {}

    public function setHeader($header){
        $this->_header = $header;
    }

    public function setFooter($footer){
        $this->_footer = $footer;
    }

    public function assign($name, $value){
        $this->_variables[$name] = $value;
    }

    public function display($file){
        extract($this->_variables);

        if ($this->_header){
            ob_start();
            include APPLICATION_LAYOUT_PATH.$this->_header;
            $header_code = ob_get_clean();
        }

        ob_start();
        include APPLICATION_VIEWS_PATH.$file;
        $content_code = ob_get_clean();

        if ($this->_footer){
            ob_start();
            include APPLICATION_LAYOUT_PATH.$this->_footer;
            $footer_code = ob_get_clean();
        }

        include APPLICATION_LAYOUT_PATH.$this->_layout;
    }

}