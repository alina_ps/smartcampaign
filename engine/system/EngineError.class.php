<?php
/**
 * User: Frantzolakis Manolis
 * Date: 29/2/2016
 * Time: 1:55 ��
 */

class EngineError {

    private static $ERRORS = null;
    private static $WARNINGS = null;

    public static function initialize(){
        if (self::$ERRORS == null) {
            self::$ERRORS = array(
                "001" => array("code" => 001, "msg" => "Your request failed.", "description" => "Please try again later."),
                "100" => array("code" => 100, "msg" => "There was an error in your request.", "description" => "Your information can not be validated"),
                "101" => array("code" => 101, "msg" => "Login failed.", "description" => "Please insert the right Username/Password combination."),
                "102" => array("code" => 102, "msg" => "Your request failed.", "description" => "You do not have the right permissions to perform this action."),
                "105" => array("code" => 105, "msg" => "The specified %s does not exist.", "description" => "The specified %s does not exist."),
                "200" => array("code" => 200, "msg" => "Your request failed.","description"=> "Required fields should not be empty."),
                "201" => array("code" => 201, "msg" => "The %s can not be created.", "description" => "%s with %s:%s already exists."),
                "202" => array("code" => 202, "msg" => "The %s can not be created.", "description" => "Field %s is not valid.")
            );
        }
        if (self::$WARNINGS == null){
            self::$WARNINGS = array(
                "EMPTY_DB_TABLE"=>"No %s found"
            );
        }
    }

    public static function getErrors(){
        return self::$ERRORS;
    }

    public static function getErrorMessage($key){
        return self::$ERRORS[$key];
    }

    public static function getWarnings(){
        return self::$WARNINGS;
    }

    public static function getWarningMessage($key){
        return self::$WARNINGS[$key];
    }

}