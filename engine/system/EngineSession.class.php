<?php
/**
 * User: Frantzolakis Manolis
 * Date: 29/2/2016
 * Time: 1:53 ��
 */


class EngineSession {

    const MAGIC_KEY = SESSION_MAGIC_KEY;

    /**
     * @var \application\models\User $user
     */
    private $user;
    private $last_activity;
    private $cart;

    /**
     * Main Constructor. Destroys session if activity has passed.
     */
    public static function getInstance(){
        /**
         * @var EngineSession $instance - Holds a Session object.
         */
        static $instance = null;
        if (null === $instance){
            $instance = new static();

            session_start();
            $instance->restoreSession();
            if (isset($instance->last_activity) && (time() - $instance->last_activity) > SESSION_TIMEOUT){
                $instance->destroy();
                $instance = null;
            }else {
                $instance->last_activity = time();
                $fields['user'] = $instance->user;
                $fields['last_activity'] = $instance->last_activity;
                if (isset($instance->cart)){
                    $fields['cart'] = $instance->cart;
                }
                $instance->registerMagicCode($fields);
            }
        }
        return $instance;

    }
    /**
     * @param array $fields
     * @param int $mode 0 for user, 1 for device
     * @throws Exception
     */
    public function initialize($fields = array(), $mode = 0){
        if ($mode == 0){
            if (!isset($fields['user']) || !isset($fields['last_activity']) )
                throw new Exception('Field Missing');

            $this->_user = $fields['user'];
            $this->_last_activity = $fields['last_activity'];
            $this->registerMagicCode($fields);
        }
    }

    /**
     * Populates our class properties/variables.
     */
    private function restoreSession(){
        if (isset($_SESSION['magiccode'])){

            $fields = $this->revertMagicCode();

            if (isset($fields['cart'])){
                $fields['cart'] = unserialize($fields['cart']);
            }

            if (isset($fields['user'])){
                $fields['user'] = unserialize($fields['user']);
            }

            foreach($fields as $key=>$value){
                $this->$key = $value;
            }
        }
    }

    /**
     * Gets the encrypted $_SESSION and decrypts it returning an array of values/fields.
     *
     * @return mixed Session fields decrypted.
     */
    private function revertMagicCode(){
        $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(self::MAGIC_KEY), base64_decode($_SESSION['magiccode']), MCRYPT_MODE_CBC, md5(md5(self::MAGIC_KEY)));
        $decrypted = rtrim($decrypted, "\0");
        return unserialize($decrypted);
    }

    /**
     * Encodes the $_SESSION variable. Session fields are readable through our Session class.
     *
     * @param array $fields array that holds the session fields/variables.
     */
    private function registerMagicCode($fields = array()){
        if (isset($fields['cart'])){
            $fields['cart'] = serialize($fields['cart']);
        }
        if (isset($fields['user'])){
            $fields['user'] = serialize($fields['user']);
        }

        $_SESSION['magiccode'] = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(self::MAGIC_KEY), serialize($fields), MCRYPT_MODE_CBC, md5(md5(self::MAGIC_KEY))));
    }

    /**
     * Destroys the session. Actively logging out the user.
     */
    public function destroy(){

        if (session_id() == ''){
            session_start();
        }

        $_SESSION = array();

        if (ini_get('session.use_cookies')){
            $params = session_get_cookie_params();
            setcookie(
                session_name(), "", time() - SESSION_TIMEOUT,
                $params['path'], $params['domain'], $params['secure'],
                $params['httponly']
            );
        }

        session_destroy();
    }

    public function isUserLoggedIn(){
        return ($this->user && $this->user->getUsername());
    }

    public function flushErrorMessage() {
        if (isset($_SESSION['main_error_msg'])) {
            $tmp = $_SESSION['main_error_msg'];
            unset($_SESSION['main_error_msg']);
            return $tmp;
        }
        else {
            return null;
        }
    }

    public function getUser(){
        return $this->user;
    }

    public function getUserId(){
        return $this->user->getId();
    }

    public function setErrorMessage($message){
        $_SESSION['main_error_msg'] = $message;
    }

    public function setSuccessMessage($message){
        $_SESSION['main_success_msg'] = $message;
    }

    public function flushSuccessMessage() {
        if (isset($_SESSION['main_success_msg'])) {
            $tmp = $_SESSION['main_success_msg'];
            unset($_SESSION['main_success_msg']);
            return $tmp;
        }
        else {
            return null;
        }
    }
}