<?php
/**
 * User: Frantzolakis Manolis
 * Date: 29/2/2016
 * Time: 2:51 ��
 */

class BaseController {

    protected $view;
    protected $model;
    protected $viewName;
    protected $mainScripts = array();
    protected $mainStylesheets = array();
    protected $session;

    public function __construct(){
        $this->view = Template::getInstance();
        $this->viewName = "";
        $this->mainScripts[] = ENGINE_SCRIPTS_LIB_URLS."jquery-2.2.0.min.js";
        $this->mainScripts[] = ENGINE_SCRIPTS_BOOTSTRAP_URL."bootstrap.min.js";

        $this->mainStylesheets[]['rel'] = ENGINE_STYLE_BOOTSTRAP_URL."bootstrap.min.css";
        $this->mainStylesheets[]['rel'] = ENGINE_STYLES_LIB_URLS."font-awesome.min.css";
        $this->mainStylesheets[]['rel'] = ENGINE_STYLES_URLS."bootstrap-reset.css?v=".time();

        $this->view->assign("mainScripts", $this->mainScripts);
        $this->session = EngineSession::getInstance();

        if ($this->session->isUserLoggedIn()){
            $s = $this->session->getUser();
        }
        $ar = array('lang'=>'el','controller'=>Engine::getCurrentController1(), 'action'=>Engine::getCurrentAction());
        $ar = array_merge($ar, Engine::getArguments());
        $this->view->assign('greekurl', Engine::url($ar));
        $ar = array('lang'=>'en','controller'=>Engine::getCurrentController1(), 'action'=>Engine::getCurrentAction());
        $ar = array_merge($ar, Engine::getArguments());
        $this->view->assign('englishurl', Engine::url($ar));
    }

    public function __destruct(){
        $this->view->assign("mainStylesheets", $this->mainStylesheets);
        if (!empty($this->viewName)){
            $this->view->display($this->viewName);
        }
    }

    protected function pushStyle($stylesheet,$noscript = false){
        $this->mainStylesheets[] = array('rel'=>$stylesheet, 'noscript'=>$noscript);
    }

    protected function removeStyle($stylesheet){
        for($i=0;$i<count($this->mainStylesheets);$i++){
            if (strcmp($stylesheet, $this->mainStylesheets[$i]['rel']) == 0) {
                unset($this->mainStylesheets[$i]);
            }
        }
    }

    protected function logout(){
        $this->session->destroy();
        Engine::redirect(Engine::url(array("controller"=>"user","action"=>"login",'lang'=>Engine::$LANGUAGE)));
    }

}