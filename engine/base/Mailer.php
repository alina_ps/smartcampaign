<?php
/**
 * User: Frantzolakis Manolis
 * Date: 19/4/2016
 * Time: 6:31 ��
 */

require_once (APPLICATION_ROOT_PATH."external/amazon/aws-autoloader.php");
use Aws\Ses\SesClient;

class Mailer {

    private $key = null;
    private $secret = null;
    private $region = null;
    /**
     * @var SesClient $client
     */
    private $client = null;

    public function __construct($config){

        foreach ($config as $index=>$value){
            $this->{$index} = $value;
        }

    }

    public function init (){
        $this->client = new SesClient(array('credentials'=>array('key'=>$this->key, 'secret'=>$this->secret),'region'=>$this->region,'version'=>'2010-12-01'));
    }

    public function sendMail($subject = null, $from = null, $to = null, $body = null, $type = null, $replyTo = null, $ccTo = null){

        $msg = array();

        if ($subject === null && $from === null && $to === null && $body === null && $type === null){
            $msg['Source'] = "Openichnos.com <noreply@fastphotos.gr>";
            $msg['Message']['Subject']['Data'] = "Test Mailer";
            $msg['Message']['Subject']['Charset'] = "UTF-8";
            $msg['Message']['Body']['Text']['Data'] = "This is a test message";
            $msg['Message']['Body']['Text']['Charset'] = "UTF-8";
            $msg['Destination']['ToAddresses'][] = "info@openit.gr";
            return $this->send($msg);
        }

        if (is_array($from)){
            foreach($from as $index=>$value){
                $msg['Source'] = mb_encode_mimeheader("OpenIchnos", 'UTF-8')." <".$index.">";
                break;
            }
        }else {
            $msg['Source'] = mb_encode_mimeheader("OpenIchnos", 'UTF-8')." <".$from.">";
        }

        $msg['Message']['Subject']['Data'] = $subject;
        $msg['Message']['Subject']['Charset'] = "UTF-8";
        if ($type!= null){
            $msg['Message']['Body']['Html']['Data'] = $body;
            $msg['Message']['Body']['Html']['Charset'] = "UTF-8";
        }else {
            $msg['Message']['Body']['Text']['Data'] = $body;
            $msg['Message']['Body']['Text']['Charset'] = "UTF-8";
        }
        if (is_array($to)){
            foreach ($to as $dest){
                $msg['Destination']['ToAddresses'][] = $dest;
            }
        }else {
            $msg['Destination']['ToAddresses'][] = $to;
        }
        if ($ccTo != null){
            $msg['Destination']['CcAddresses'][] = $ccTo;
        }
        if ($replyTo != null){
            if (is_array($replyTo)){
                $msg['ReplyToAddresses'] = $replyTo;
            }else {
                $msg['ReplyToAddresses'] = array($replyTo);
            }
        }

        $x = $this->send($msg);
        return $x;

    }

    private function send($msg){
        try{
            $result = $this->client->sendEmail($msg);
            $msg_id = $result->get('MessageId');

        } catch (Exception $e) {
            $result['error'] = array('errorcode'=>'100', 'errormsg'=>'Error in SES sending internal machine.'. $e->getMessage());
            $result['success'] = false;
        }
       // var_dump($result);
        return $result;
    }

    public function getLayout(){
        return file_get_contents(APPLICATION_VIEWS_PATH."Mails/layout.php");
    }

    public function getNormalContent($filename){
        return file_get_contents(APPLICATION_VIEWS_PATH."Mails/normal/".$filename);
    }

    public function getOutlookContent($filename){
        return file_get_contents(APPLICATION_VIEWS_PATH."Mails/outlook/".$filename);
    }

}