<?php
/**
 * User: Frantzolakis Manolis
 * Date: 29/2/2016
 * Time: 4:16 ��
 */

class BaseModel {

    protected $db = null;
    protected $DB_TABLE;

    public function __construct($table){
        $this->db = EngineDatabase::getInstance();
        $this->DB_TABLE = $table;
    }

    protected function extractSelectArguments($args){

        $result = array();

        if (empty($args)){
            $result['table'] = null;
            $result['columns'] = null;
            $result['where'] = null;
            $result['order'] = null;
            $result['limit'] = null;
            $result['offset'] = null;
            return $result;
        }

        if (isset($args['table']) && !empty($args['table'])){
            $result['table'] = $args['table'];
        }else {
            $result['table'] = null;
        }

        if (isset($args['columns']) && !empty($args['columns'])){
            $columns = array();
            foreach($args['columns'] as $col){
                $columns[] = $col;
            }
            $result['columns'] = $columns;
        }else {
            $result['columns'] = null;
        }

        if (isset($args['order']) && !empty($args['order'])){
            $result['order'] = $args['order'];
        }else {
            $result['order'] = null;
        }

        if (isset($args['limit']) && !empty($args['limit'])){
            $result['limit'] = $args['limit'];
        }else {
            $result['limit'] = null;
        }

        if (isset($args['offset']) && !empty($args['offset'])){
            $result['offset'] = $args['offset'];
        }else {
            $result['offset'] = null;
        }
        if (isset($args['where']) && !empty($args['where'])){
            $fields = array();
            $values = array();
            foreach($args['where'] as $index=>$value){
                if (is_numeric($index)){
                    if (is_array($value)){
                        foreach($value as $mInd=>$mVal){
                            $fields[] = rtrim($mInd, "_");
                            $values[] = $mVal;
                        }
                        continue;
                    }else {
                        $fields[] = $value;
                        continue;
                    }
                }
                $fields[] = rtrim($index, "_");
                $values[] = $value;
            }
            $relation="AND";
            if (isset($args['relation'])){
                $relation = $args['relation'];
            }
            $result['where'] = array('relation'=>$relation, 'fields'=>$fields, 'values'=>$values);
        }else{
            $result['where'] = null;
        }
        return $result;
    }

}