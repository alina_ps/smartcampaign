<?php
/**
 * User: Frantzolakis Manolis
 * Date: 29/2/2016
 * Time: 1:50 ��
 */

define ("APP_FOLDER", "application");

/**
 * Defines For Modules
 */
define ("QRCODE_ENABLED", false);
define ("PDF_ENABLED", false);
define ("GOOGLE_FONTS", false);
define ("PAYMENTS", false);
define ("AMAZON_ENABLED", false);

/**
 * Define For State {LIVE,DEBUG,PRODUCTION}
 */
define ("APP_STATE", "DEBUG");