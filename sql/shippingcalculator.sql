-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Φιλοξενητής: 127.0.0.1
-- Χρόνος δημιουργίας: 04 Απρ 2017 στις 07:50:38
-- Έκδοση διακομιστή: 5.7.14
-- Έκδοση PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Βάση δεδομένων: `shippingcalculator`
--

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `deliveries`
--

CREATE TABLE `deliveries` (
  `id` int(11) NOT NULL,
  `tmp_id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `delivery_date` varchar(15) NOT NULL,
  `creation_date` varchar(15) NOT NULL,
  `choice` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Άδειασμα δεδομένων του πίνακα `deliveries`
--

INSERT INTO `deliveries` (`id`, `tmp_id`, `code`, `description`, `status`, `delivery_date`, `creation_date`, `choice`) VALUES
(263, -121, '1-263aa', 'Αποστολή 2', 0, '1490623853', '1490623853', '[{"destination":"Αδριανουπόλεως 16","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff":"180 ευρώ","total_cost":"214.52 ευρώ"}]'),
(264, -171, '1-264', 'Αποστολή 2', 0, '1490623853', '1490628699', '[{"destination":"Αδριανουπόλεως 16","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff":"180 ευρώ","total_cost":"214.52 ευρώ"}]'),
(265, -28, '1-265', 'Αποστολή 2', 0, '1490623853', '1490628873', '[{"destination":"Αδριανουπόλεως 16","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff":"180 ευρώ","total_cost":"214.52 ευρώ"}]'),
(266, -46, '1-266', 'Αποστολή 3', 0, '1490623853', '1490630184', '[{"destination":"Αδριανουπόλεως 16","truck":"Neo","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"180 ευρώ","total_cost":"214.52 ευρώ"}]'),
(267, -73, '1-267', 'Αποστολή 2', 0, '1490623853', '1490689108', '[{"destination":"Αδριανουπόλεως 16","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff":"180 ευρώ","total_cost":"214.52 ευρώ"}]'),
(268, -76, '1-268', 'Αποστολή 2', 0, '1490623853', '1490689241', '[{"destination":"Αδριανουπόλεως 16","truck":"Neo","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"180 ευρώ","total_cost":"214.52 ευρώ"}]'),
(269, -188, '1-269', 'Αποστολή 2', 0, '1490623853', '1490689827', '[{"destination":"Αλωνίων 3","truck":"Neo","duration":"17 λεπτά","distance":"8.81 χλμ","cost":"22.45 ευρώ","staff_cost":"180 ευρώ","total_cost":"202.45 ευρώ"}]'),
(270, 269, '1-270', 'Αποστολή 2', 0, '1490623853', '1490689827', '[{"destination":"Αλωνίων 3","truck":"Neo","duration":"17 λεπτά","distance":"8.81 χλμ","cost":"22.45 ευρώ","staff_cost":"180 ευρώ","total_cost":"202.45 ευρώ"}]'),
(275, -135, '1-275', 'Αποστολή 3', 0, '1490623853', '1490694014', '[{"destination":"Αλωνίων 3","truck":"Neo","duration":"17 λεπτά","distance":"8.81 χλμ","cost":"22.45 ευρώ","staff_cost":"180 ευρώ","total_cost":"202.45 ευρώ"},{"destination":"Αδριανουπόλεως 16","truck":"Connect","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"180 ευρώ","total_cost":"214.52 ευρώ"}]'),
(276, -106, '1-276', 'Αποστολή 2', 0, '1490623853', '1490695827', '[{"destination":"Αδριανουπόλεως 16","truck":"1824 Με γερανο","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"240 ευρώ","total_cost":"274.52 ευρώ"},{"destination":"Αλωνίων 3","truck":"847","duration":"17 λεπτά","distance":"8.81 χλμ","cost":"22.45 ευρώ","staff_cost":"180 ευρώ","total_cost":"202.45 ευρώ"}]'),
(277, -80, '1-277', 'Αποστολή 3', 0, '1490623853', '1490696095', '[{"destination":"Αλωνίων 3","truck":"Neo","duration":"17 λεπτά","distance":"8.81 χλμ","cost":"22.45 ευρώ","staff_cost":"180 ευρώ","total_cost":"202.45 ευρώ"},{"destination":"Κυπαρισσιών 1","truck":"1824 Με γερανο","duration":"0 λεπτά","distance":"0.04 χλμ","cost":"0.11 ευρώ","staff_cost":"240 ευρώ","total_cost":"240.11 ευρώ"},{"destination":"Αδριανουπόλεως 16","truck":"847","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"180 ευρώ","total_cost":"214.52 ευρώ"}]'),
(278, -59, '1-278', 'Αποστολή 2', 0, '1490623853', '1490696411', '[{"destination":"Αλωνίων 3","truck":"847","duration":"17 λεπτά","distance":"8.81 χλμ","cost":"22.45 ευρώ","staff_cost":"180 ευρώ","total_cost":"202.45 ευρώ"},{"destination":"Αδριανουπόλεως 16","truck":"Transit","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"180 ευρώ","total_cost":"214.52 ευρώ"},{"destination":"Κυπαρισσιών 1","truck":"Connect","duration":"0 λεπτά","distance":"0.04 χλμ","cost":"0.11 ευρώ","staff_cost":"180 ευρώ","total_cost":"180.11 ευρώ"}]'),
(279, -98, '1-279', 'Αποστολή 3', 0, '1490623853', '1490702865', '[{"destination":"Σκεπαστού 27","truck":"1824 Με γερανο","duration":"6 ώρες και 60 λεπτά","distance":"742.65 χλμ","cost":"1893.76 ευρώ","staff_cost":"240 ευρώ","stay":"240 ευρώ","total_cost":"2373.76 ευρώ"}]'),
(280, -48, '1-280', 'Αποστολή 1', 0, '1490623853', '1490718158', '[{"destination":"Κυπαρισσιών 1","truck":"1824 Με γερανο","duration":"0 λεπτά","distance":"0.04 χλμ","cost":"0.11 ευρώ","staff_cost":"240 ευρώ","total_cost":"240.11 ευρώ"}]'),
(281, -2, '1-281', 'νεα αποστολή', 0, '1490623853', '1490718333', '[{"destination":"Κυπαρισσιών 1","truck":"Neo","duration":"0 λεπτά","distance":"0.04 χλμ","cost":"0.11 ευρώ","staff_cost":"180 ευρώ","total_cost":"180.11 ευρώ"}]'),
(282, -178, '1-282', 'νεα αποστολή', 0, '1490623853', '1490718729', '[{"destination":"Κυπαρισσιών 1","truck":"Neo","duration":"0 λεπτά","distance":"0.04 χλμ","cost":"0.11 ευρώ","staff_cost":"180 ευρώ","total_cost":"180.11 ευρώ"}]'),
(285, -100, '1-285', 'Αποστολή 2', 0, '1490623853', '1490774131', '[{"destination":"Κυπαρισσιών 1","truck":"Neo","duration":"0 λεπτά","distance":"0.04 χλμ","cost":"0.11 ευρώ","staff_cost":"180 ευρώ","total_cost":"180.11 ευρώ"},{"destination":"Αδριανουπόλεως 16","truck":"1824 Με γερανο","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"240 ευρώ","total_cost":"274.52 ευρώ"}]'),
(286, -103, '1-286', 'Αποστολή 3', 0, '1490623853', '1490777020', '[{"destination":"Αδριανουπόλεως 16, Κυπαρισσιών 1","truck":"Neo","duration":"36 λεπτά","distance":"27.18 χλμ","cost":"69.30 ευρώ","staff_cost":"180 ευρώ","total_cost":"249.30 ευρώ"},{"destination":"Αλωνίων 3","truck":"1824 Με γερανο","duration":"17 λεπτά","distance":"8.81 χλμ","cost":"22.45 ευρώ","staff_cost":"240 ευρώ","total_cost":"262.45 ευρώ"}]'),
(287, -160, '1-287', 'Αποστολή 2', 0, '1490623853', '1490777378', '[{"destination":"Κυπαρισσιών 1","truck":"Neo","duration":"0 λεπτά","distance":"0.04 χλμ","cost":"0.11 ευρώ","staff_cost":"180 ευρώ","total_cost":"180.11 ευρώ"},{"destination":"Αλωνίων 3","truck":"1824 Με γερανο","duration":"17 λεπτά","distance":"8.81 χλμ","cost":"22.45 ευρώ","staff_cost":"240 ευρώ","total_cost":"262.45 ευρώ"},{"destination":"Αδριανουπόλεως 16","truck":"847","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"180 ευρώ","total_cost":"214.52 ευρώ"}]'),
(288, -107, '1-288', 'Αποστολή 2', 0, '1490623853', '1490793163', '[{"destination":"Αλωνίων 3, Κυπαρισσιών 1","truck":"Neo","duration":"33 λεπτά","distance":"17.46 χλμ","cost":"44.52 ευρώ","staff_cost":"180 ευρώ","total_cost":"224.52 ευρώ"},{"destination":"Αδριανουπόλεως 16","truck":"Connect","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"180 ευρώ","total_cost":"214.52 ευρώ"}]'),
(289, -39, '1-289', 'Αποστολή 2', 0, '1490623853', '1490798092', '[{"destination":"Αλωνίων 3, Κυπαρισσιών 1, Αδριανουπόλεως 16","truck":"1824 Με γερανο","duration":"51 λεπτά","distance":"30.95 χλμ","cost":"78.93 ευρώ","staff_cost":"240 ευρώ","total_cost":"318.93 ευρώ"}]'),
(293, -20, '1-293', 'Αποστολή 2', 0, '1490623853', '1490866458', '[{"destination":"Αδριανουπόλεως 16, Αλωνίων 3, Κυπαρισσιών 1","truck":"Connect","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"180 ευρώ","total_cost":"214.52 ευρώ"}]'),
(294, -99, '1-294', 'Αποστολή 3', 0, '1490832000', '1490870610', '[{"destination":"Αδριανουπόλεως 16","truck":"1824 Με γερανο","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"240 ευρώ","total_cost":"274.52 ευρώ"}]'),
(295, -99, '1-295', 'Αποστολή 3', 0, '1490832000', '1490870625', '[{"destination":"Αδριανουπόλεως 16","truck":"1824 Με γερανο","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"240 ευρώ","total_cost":"274.52 ευρώ"}]'),
(296, -137, '1-296', 'Αποστολή 2', 0, '1490918400', '1490870732', '[{"destination":"Αδριανουπόλεως 16","truck":"1824 Με γερανο","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"240 ευρώ","total_cost":"274.52 ευρώ"}]'),
(297, -187, '1-297', 'Αποστολή 2', 0, '1490918400', '1490875758', '[{"destination":"Αδριανουπόλεως 16","truck":"1824 Με γερανο","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"240 ευρώ","total_cost":"274.52 ευρώ"}]'),
(299, -37, '1-299', 'Αποστολή 2', 0, '1490918400', '1490875998', '[{"destination":"Αδριανουπόλεως 16","truck":"Transit","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"180 ευρώ","total_cost":"214.52 ευρώ"},{"destination":"Αλωνίων 3","truck":"Connect","duration":"17 λεπτά","distance":"8.81 χλμ","cost":"22.45 ευρώ","staff_cost":"180 ευρώ","total_cost":"202.45 ευρώ"}]'),
(300, -123, '1-300', 'Αποστολή 3', 0, '1490832000', '1490880205', '[{"destination":"Αδριανουπόλεως 16","truck":"1824 Με γερανο","duration":"18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"240 ευρώ","total_cost":"274.52 ευρώ"},{"destination":"Αλωνίων 3","truck":"847","duration":"17 λεπτά","distance":"8.81 χλμ","cost":"22.45 ευρώ","staff_cost":"180 ευρώ","total_cost":"202.45 ευρώ"}]'),
(301, -189, '1-301', 'Αποστολή 3', 0, '1490832000', '1490885604', '[{"destination":"Κυπαρισσιών 1","truck":"Transit","duration":"4 ώρες","distance":"0.04 χλμ","cost":"0.11 ευρώ","staff_cost":"180 ευρώ","total_cost":"180.11 ευρώ"},{"destination":"Αδριανουπόλεως 16, Αλωνίων 3","truck":"Connect","duration":"8 ώρες και 47 λεπτά","distance":"34.80 χλμ","cost":"88.73 ευρώ","staff_cost":"180 ευρώ","total_cost":"268.73 ευρώ"}]'),
(302, -136, '1-302', 'Αποστολή 2', 0, '1493164800', '1491230219', '[{"destination":"Αλωνίων 3","truck":"Neo","duration":"4 ώρες και 17 λεπτά","distance":"8.81 χλμ","cost":"22.45 ευρώ","staff_cost":"180 ευρώ","total_cost":"202.45 ευρώ"},{"destination":"Αδριανουπόλεως 16","truck":"1824 Με γερανο","duration":"4 ώρες και 18 λεπτά","distance":"13.54 χλμ","cost":"34.52 ευρώ","staff_cost":"240 ευρώ","total_cost":"274.52 ευρώ"}]');

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `destinations`
--

CREATE TABLE `destinations` (
  `id` int(11) NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `tmp_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `road` varchar(60) NOT NULL,
  `city` varchar(60) NOT NULL,
  `postal_code` varchar(10) NOT NULL,
  `items` varchar(400) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `distance` int(11) NOT NULL,
  `duration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Άδειασμα δεδομένων του πίνακα `destinations`
--

INSERT INTO `destinations` (`id`, `delivery_id`, `tmp_id`, `name`, `road`, `city`, `postal_code`, `items`, `latitude`, `longitude`, `distance`, `duration`) VALUES
(215, 263, -155, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', '', '', '', '', 37.9653, 23.7571, 0, 0),
(216, 265, -144, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(217, 266, -192, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '[{"type":"49","noItems":"2"},{"type":"50","noItems":"2"}]', 37.9653, 23.7571, 13536, 1106),
(218, 267, -7, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '[{"type":"49","noItems":"2"}]', 37.9653, 23.7571, 13536, 1106),
(219, 268, -179, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '[{"type":"49","noItems":"2"}]', 37.9653, 23.7571, 13536, 1106),
(220, 269, -154, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '[{"type":"50","noItems":"2"}]', 38.0706, 23.8135, 8805, 1005),
(221, 270, -171, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '[{"type":"50","noItems":"3"},{"type":"49","noItems":"2"}]', 37.9653, 23.7571, 13536, 1106),
(222, 271, -6, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '[{"type":"50","noItems":"3"}]', 38.0706, 23.8135, 8805, 1005),
(223, 272, -15, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '[{"type":"49","noItems":"1"}]', 37.9653, 23.7571, 13536, 1106),
(224, 273, -19, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '', 38.0706, 23.8135, 8805, 1005),
(225, 274, -157, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(226, -81, -117, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(227, -81, -79, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(228, -17, -120, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '', 38.0706, 23.8135, 8805, 1005),
(229, -17, -119, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(230, -31, -133, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '', 38.0706, 23.8135, 8805, 1005),
(231, -31, -53, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(232, -10, -63, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '', 38.0706, 23.8135, 8805, 1005),
(233, -10, -47, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(234, -51, -9, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '', 38.0706, 23.8135, 8805, 1005),
(235, -51, -129, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(236, 275, -177, 'Κυπαρισσιών 1, Αγ. Παρασκευή 153 43, Ελλάδα', 'kyparissiwn 1', 'agia paraskeyh', '15343', '[{"type":"49","noItems":"2"}]', 38.0214, 23.8413, 42, 8),
(237, 275, -39, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(238, 276, -195, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '[{"type":"50","noItems":"2"},{"type":"49","noItems":"2"}]', 38.0706, 23.8135, 8805, 1005),
(239, 276, -142, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'Αδριανουπόλεως 16', 'καισαριανή', '16121', '', 37.9653, 23.7571, 13536, 1106),
(240, -106, -188, 'Κυπαρισσιών 1, Αγ. Παρασκευή 153 43, Ελλάδα', 'kyparissiwn 1', 'agia paraskeyh', '15343', '[{"type":"49","noItems":"2"},{"type":"50","noItems":"2"}]', 38.0214, 23.8413, 42, 8),
(241, 277, -180, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '[{"type":"49","noItems":"2"}]', 38.0706, 23.8135, 8805, 1005),
(242, 277, -107, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '[{"type":"49","noItems":"3"},{"type":"50","noItems":"3"}]', 37.9653, 23.7571, 13536, 1106),
(243, 278, -111, 'Κυπαρισσιών 1, Αγ. Παρασκευή 153 43, Ελλάδα', 'kyparissiwn 1', 'agia paraskeyh', '15343', '[{"type":"50","noItems":"2"}]', 38.0214, 23.8413, 42, 8),
(244, 278, -22, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '[{"type":"50","noItems":"1"},{"type":"49","noItems":"2"}]', 38.0706, 23.8135, 8805, 1005),
(245, 278, -168, 'Κυπαρισσιών 1, Αγ. Παρασκευή 153 43, Ελλάδα', 'kyparissiwn 1', 'agia paraskeyh', '15343', '', 38.0214, 23.8413, 42, 8),
(246, 279, -64, 'Σκεπαστού 27, Κομοτηνή 691 00, Ελλάδα', 'skepastou 27', 'komotini', '69100', '[{"type":"49","noItems":"2"}]', 41.1146, 25.3958, 742650, 25172),
(247, 280, -122, 'Κυπαρισσιών 1, Αγ. Παρασκευή 153 43, Ελλάδα', 'kyparissiwn 1', 'agia paraskeyh', '15343', '', 38.0214, 23.8413, 42, 8),
(248, 281, -71, 'Κυπαρισσιών 1, Αγ. Παρασκευή 153 43, Ελλάδα', 'kyparissiwn 1', 'agia paraskeyh', '15343', '', 38.0214, 23.8413, 42, 8),
(249, 281, -27, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(250, 282, -110, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '', 38.0706, 23.8135, 8805, 1005),
(251, 282, -55, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(252, 283, -43, 'Κυπαρισσιών 1, Αγ. Παρασκευή 153 43, Ελλάδα', 'kyparissiwn 1', 'agia paraskeyh', '15343', '', 38.0214, 23.8413, 42, 8),
(253, 284, -17, 'Κυπαρισσιών 1, Αγ. Παρασκευή 153 43, Ελλάδα', 'kyparissiwn 1', 'agia paraskeyh', '15343', '', 38.0214, 23.8413, 42, 8),
(254, 285, -151, 'Κυπαρισσιών 1, Αγ. Παρασκευή 153 43, Ελλάδα', 'kyparissiwn 1', 'agia paraskeyh', '15343', '', 38.0214, 23.8413, 42, 8),
(255, 285, -82, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(256, 286, -46, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(257, 286, -95, 'Κυπαρισσιών 1, Αγ. Παρασκευή 153 43, Ελλάδα', 'kyparissiwn 1', 'agia paraskeyh', '15343', '', 38.0214, 23.8413, 42, 8),
(258, 287, -116, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '', 38.0706, 23.8135, 8805, 1005),
(259, 287, -197, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(260, 288, -52, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '', 38.0706, 23.8135, 8805, 1005),
(261, 288, -189, 'Κυπαρισσιών 1, Αγ. Παρασκευή 153 43, Ελλάδα', 'kyparissiwn 1', 'agia paraskeyh', '15343', '', 38.0214, 23.8413, 42, 8),
(262, 288, -120, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(263, 289, -115, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '[{"type":"49","noItems":"2"}]', 38.0706, 23.8135, 8805, 1005),
(264, 289, -154, 'Κυπαρισσιών 1, Αγ. Παρασκευή 153 43, Ελλάδα', 'kyparissiwn 1', 'agia paraskeyh', '15343', '[{"type":"49","noItems":"2"},{"type":"50","noItems":"2"}]', 38.0214, 23.8413, 42, 8),
(265, 289, -9, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '[{"type":"49","noItems":"2"}]', 37.9653, 23.7571, 13536, 1106),
(270, 292, -10, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '[{"type":"49","noItems":"2"}]', 37.9653, 23.7571, 13536, 1106),
(271, 293, -51, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(272, 293, -100, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '', 38.0706, 23.8135, 8805, 1005),
(273, 293, -193, 'Κυπαρισσιών 1, Αγ. Παρασκευή 153 43, Ελλάδα', 'kyparissiwn 1', 'agia paraskeyh', '15343', '', 38.0214, 23.8413, 42, 8),
(274, 294, -144, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(275, 295, -144, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(276, 296, -45, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(277, 297, -138, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(280, 299, -16, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(281, 299, -107, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '', 38.0706, 23.8135, 8805, 1005),
(282, 300, -16, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '[{"type":"49","noItems":"2"}]', 37.9653, 23.7571, 13536, 1106),
(283, 300, -43, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '[{"type":"49","noItems":"3"}]', 38.0706, 23.8135, 8805, 1005),
(284, 301, -45, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '', 37.9653, 23.7571, 13536, 1106),
(285, 301, -155, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '', 38.0706, 23.8135, 8805, 1005),
(286, 301, -189, 'Κυπαρισσιών 1, Αγ. Παρασκευή 153 43, Ελλάδα', 'kyparissiwn 1', 'agia paraskeyh', '15343', '', 38.0214, 23.8413, 42, 8),
(287, 302, -21, 'Αδριανουπόλεως 16, Καισαριανή 161 21, Ελλάδα', 'adrianoupoleos 16', 'kaisariani', '16121', '[{"type":"50","noItems":"2"}]', 37.9653, 23.7571, 13536, 1106),
(288, 302, -174, 'Αλωνίων 3, Κηφισιά 145 62, Ελλάδα', 'alwniwn 3', 'khfisia', '14562', '[{"type":"49","noItems":"2"}]', 38.0706, 23.8135, 8805, 1005);

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Άδειασμα δεδομένων του πίνακα `items`
--

INSERT INTO `items` (`id`, `name`, `length`, `width`, `height`, `user_id`) VALUES
(49, 'item2', 122, 121, 121, 3),
(50, 'newItem', 100, 90, 20, 3);

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `trucks`
--

CREATE TABLE `trucks` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `user_id` int(11) NOT NULL,
  `length` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `klm_cost` float NOT NULL,
  `staff_cost` float NOT NULL,
  `stay_cost` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Άδειασμα δεδομένων του πίνακα `trucks`
--

INSERT INTO `trucks` (`id`, `name`, `user_id`, `length`, `width`, `height`, `klm_cost`, `staff_cost`, `stay_cost`) VALUES
(1, 'Neo', 3, 740, 245, 260, 2.55, 180, 180),
(2, '1824 Με γερανο', 3, 640, 245, 260, 2.55, 240, 240),
(3, '847', 3, 520, 245, 260, 2.55, 180, 0),
(4, 'Transit', 3, 220, 130, 170, 2.55, 180, 0),
(5, 'Connect', 3, 160, 120, 150, 2.55, 180, 0);

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` text NOT NULL,
  `role` enum('admin','user') NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Άδειασμα δεδομένων του πίνακα `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES
(3, 'openit', '0fb94c3dd20d8ff5d4ce8ed5f5885713', 'admin'),
(4, 'demomed', '552797e6031ff7fccc2b0f72bbafcccd', 'user'),
(7, 'alina2', '914a23f72f590809d3fe431573ecb71f', 'user');

--
-- Ευρετήρια για άχρηστους πίνακες
--

--
-- Ευρετήρια για πίνακα `deliveries`
--
ALTER TABLE `deliveries`
  ADD PRIMARY KEY (`id`);

--
-- Ευρετήρια για πίνακα `destinations`
--
ALTER TABLE `destinations`
  ADD PRIMARY KEY (`id`);

--
-- Ευρετήρια για πίνακα `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Ευρετήρια για πίνακα `trucks`
--
ALTER TABLE `trucks`
  ADD PRIMARY KEY (`id`);

--
-- Ευρετήρια για πίνακα `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT για άχρηστους πίνακες
--

--
-- AUTO_INCREMENT για πίνακα `deliveries`
--
ALTER TABLE `deliveries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=303;
--
-- AUTO_INCREMENT για πίνακα `destinations`
--
ALTER TABLE `destinations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=289;
--
-- AUTO_INCREMENT για πίνακα `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT για πίνακα `trucks`
--
ALTER TABLE `trucks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT για πίνακα `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
