<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 1/3/2017
 * Time: 6:14 μμ
 */
?>
<?php
if (isset($successmsg) || isset($errormsg)){
    ?>
    <div class="row top-buffer">
        <div class="col-md-12">
            <?php
            if (isset($successmsg)){
                $class = " alert-info";
                $msg = $successmsg;
            }else if (isset($errormsg)){
                $class = " alert-danger";
                $msg = $errormsg;
            }else {
                $class = " hidden";
                $msg = "";
            }
            ?>
            <div id="request-msg" class="alert <?php echo $class;?>"><?php echo $msg; ?></div>
        </div>
    </div>
<?php } ?>
<?php
?>
<div class="row top-buffer">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><?php echo 'Add mean'?></h3></div>
            <div class="panel-body">
                <form id="create-item-form" class="form-horizontal create-item-form" role="form" method="POST" action="<?php echo Engine::url(array('controller'=>'item','action'=>'create'));?>">
                    <div class="form-group">
                        <label for="type" class="col-sm-2 col-xs-4 control-label">Type <span class="required-star">*</span></label>
                        <div class="col-sm-6 col-xs-12">
                            <select class="form-control" name="type" id="type">
                                <option value="TV">TV</option>
                                <option value="SEO">SEO</option>
                                <option value="Adwords">Adwords</option>
                                <option value="Facebook">Facebook</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-2 col-xs-4 control-label">Description</label>
                        <div class="col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="description" name="description" placeholder="text"/>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ROI" class="col-sm-2 col-xs-4 control-label">Returns-of-Investments (ROI) (%) <span class="required-star">*</span></label>
                        <div class="col-sm-2 col-xs-2 visible-xs">
                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo "Required. Percentage (0-100)";?>"></i>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="ROI" name="ROI" placeholder="10"/>
                            <div class="help-block"></div>
                        </div>
                        <div class="col-sm-2 hidden-xs">
                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo "Required. Percentage (0-100)";?>"></i>
                        </div>
                    </div>
              <!--      <div class="form-group">
                        <label for="min_spend" class="col-sm-2 col-xs-4 control-label">Minimum Spend of total budget (%) </label>

                        <div class="col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="min_spend" name="min_spend" placeholder="30"/>
                            <div class="help-block"></div>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="max_spend" class="col-sm-2 col-xs-4 control-label">Maximum Spend of total budget (%) </label>

                        <div class="col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="max_spend" name="max_spend" placeholder="30"/>
                            <div class="help-block"></div>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="min_cost" class="col-sm-2 col-xs-4 control-label">Minimum Cost </label>

                        <div class="col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="min_cost" name="min_cost" placeholder="1000"/>
                            <div class="help-block"></div>
                        </div>

                    </div> -->
                    <div class="form-group">
                        <label for="cpereuro" class="col-sm-2 col-xs-4 control-label">Customer per euro</label>

                        <div class="col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="cpereuro" name="cpereuro" placeholder="2.3"/>
                            <div class="help-block"></div>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="submit" class="col-sm-2 control-label"></label>
                        <div class="col-sm-6">
                            <input class="btn btn-primary" type="submit" name="submit" value="Save">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-6">Fields with <span class="required-star">*</span> are required.
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
