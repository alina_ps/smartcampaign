<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 1/3/2017
 * Time: 6:14 μμ
 */
?>
<?php
if (isset($successmsg) || isset($errormsg)){
    ?>
    <div class="row top-buffer">
        <div class="col-md-12">
            <?php
            if (isset($successmsg)){
                $class = " alert-info";
                $msg = $successmsg;
            }else if (isset($errormsg)){
                $class = " alert-danger";
                $msg = $errormsg;
            }else {
                $class = " hidden";
                $msg = "";
            }
            ?>
            <div id="request-msg" class="alert <?php echo $class;?>"><?php echo $msg; ?></div>
        </div>
    </div>
<?php } ?>
<div class="row top-buffer">
    <div class="col-md-12">
        <a href="<?php echo Engine::url(array('controller'=>'item','action'=>'create'));?>" class="btn btn-primary"><?php echo 'Add a new mean'; ?></a>
    </div>
</div>
<div class="row top-buffer">
    <div class="col-md-12 col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><?php echo 'Means';?></h3></div>
                    <table id="tbl-items" data-classes="table table-responsive table-hover table-stripped" class="table table-responsive">
                        <thead>
                        <tr>
                         <!--   <th data-field="unique_id" data-sortable="true" class="text-center" data-searchable="true"><?php echo "ID";?></th> -->
                            <th data-field="type" class="text-center"><?php echo 'Type';?></th>
                            <th data-field="ROI" class="text-center"><?php echo 'Returns-of-Investments (ROI) ';?></th>
                           <!-- <th data-field="min_spend" class="text-center"><?php echo 'Minimum Spend of total budget (%) ';?></th>
                            <th data-field="max_spend" class="text-center"><?php echo 'Maximum Spend of total budget (%) ';?></th>
                            <th data-field="min_cost" class="text-center"><?php echo 'Minimum cost ';?></th>-->
                            <th data-field="cpereuro" class="text-center"><?php echo 'Customer per euro ';?></th>
                            <th data-align="center" data-formatter="itemActionFormatter" class="text-center"></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>