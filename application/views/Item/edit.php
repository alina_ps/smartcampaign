<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 1/3/2017
 * Time: 6:14 μμ
 */
?>
<?php
if (isset($successmsg) || isset($errormsg)){
    ?>
    <div class="row top-buffer">
        <div class="col-md-12">
            <?php
            if (isset($successmsg)){
                $class = " alert-info";
                $msg = $successmsg;
            }else if (isset($errormsg)){
                $class = " alert-danger";
                $msg = $errormsg;
            }else {
                $class = " hidden";
                $msg = "";
            }
            ?>
            <div id="request-msg" class="alert <?php echo $class;?>"><?php echo $msg; ?></div>
        </div>
    </div>
<?php } ?>
<?php
?>
<div class="row top-buffer">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><?php echo 'Επεξεργασία: ' . $item['type'];?></h3></div>
            <div class="panel-body">
                <form id="edit-item-form" class="form-horizontal edit-item-form" role="form" method="POST" action="<?php echo Engine::url(array('controller'=>'item','action'=>'edit', 'id'=>$item['id']));?>">
                    <input type="hidden" name="item_id" value="<?php echo $item['id'];?>"/>
                    <div class="form-group">
                        <label for="type" class="col-sm-2 col-xs-4 control-label">Type <span class="required-star">*</span></label>
                        <div class="col-sm-6 col-xs-12">
                            <select class="form-control" name="type" id="type">
                                <option value="TV" <?php if (isset($item)){if($item['type'] === 'TV') { echo 'selected';}}?>>TV</option>
                                <option value="SEO" <?php if (isset($item)){if($item['type'] === 'SEO') { echo 'selected';}}?>>SEO</option>
                                <option value="Adwords" <?php if (isset($item)){if($item['type'] === 'Adwords') { echo 'selected';}}?>>Adwords</option>
                                <option value="Facebook" <?php if (isset($item)){if($item['type'] === 'Facebook') { echo 'selected';}}?>>Facebook</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-2 col-xs-4 control-label">Description</label>
                        <div class="col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="description" name="description" value="<?php echo $item['description'];?>"/>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ROI" class="col-sm-2 col-xs-4 control-label">Returns-of-Investments (ROI) (%)</label>
                        <div class="col-sm-2 col-xs-2 visible-xs">
                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo "Required. Percentage (0-100)";?>"></i>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="ROI" name="ROI" value="<?php echo $item['ROI'];?>"/>
                            <div class="help-block"></div>
                        </div>
                        <div class="col-sm-2 hidden-xs">
                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo "Required. Percentage (0-100)";?>"></i>
                        </div>
                    </div>
          <!--          <div class="form-group">
                        <label for="min_spend" class="col-sm-2 col-xs-4 control-label">Minimum Spend of total budget (%) </label>
                        <div class="col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="min_spend" name="min_spend" value="<?php echo $item['min_spend'];?>"/>
                            <div class="help-block"></div>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="max_spend" class="col-sm-2 col-xs-4 control-label">Maximum Spend of total budget (%) </label>

                        <div class="col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="max_spend" name="max_spend" value="<?php echo $item['max_spend'];?>"/>
                            <div class="help-block"></div>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="min_cost" class="col-sm-2 col-xs-4 control-label">Minimum Cost </label>

                        <div class="col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="min_cost" name="min_cost" value="<?php echo $item['min_cost'];?>"/>
                            <div class="help-block"></div>
                        </div>

                    </div> -->
                    <div class="form-group">
                        <label for="cpereuro" class="col-sm-2 col-xs-4 control-label">Customer per euro</label>

                        <div class="col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="cpereuro" name="cpereuro" value="<?php echo $item['cpereuro'];?>"/>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="submit" class="col-sm-2 control-label"></label>
                        <div class="col-sm-6">
                            <input class="btn btn-primary" type="submit" name="submit" value="Update">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-6">Fields with <span class="required-star">*</span> are required.
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
