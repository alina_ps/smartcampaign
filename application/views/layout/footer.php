<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 col-xs-12">
                <p class="text-center text-muted">Powered by <a href="http://www.openit.gr" target="_blank" >OpenIT</a></p>
            </div>
            <div class="col-md-3 col-xs-12">
                <p class="pull-right text-muted">Copyright &copy; <?php echo date('Y');?></p>
            </div>
        </div>
    </div>
</footer>
  