<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">
        <title><?php echo APPLICATION_NAME; ?></title>
        <link rel="icon" type="image/x-icon" href="<?php echo APPLICATION_IMAGES_URL."favicon.png";?>"/>
        <?php
        if (isset($mainStylesheets)){
            foreach($mainStylesheets as $style){
                if (isset($style['noscript']) && $style['noscript']){
                    echo '<noscript><link rel="stylesheet" href="'.$style['rel'].'"></noscript>';
                }else {
                    echo '<link rel="stylesheet" href="'.$style['rel'].'">';
                }
            }
        }
        ?>
        <?php echo '<script type="text/javascript">var ajaxUrl="'.ENGINE_AJAXURL.'";</script>'; ?>
        <?php echo '<script type="text/javascript">var engineLanguage="'.ENGINE_LANGUAGE.'";</script>'; ?>
        <?php
        if (isset($rawScripts)){
            foreach ($rawScripts as $script){
                echo $script;
            }
        }
        ?>
    </head>
    <body>
    <?php if (isset($header_code)){ echo $header_code;} ?>

    <div class="container<?php if (isset($fluid)) { echo "-fluid"; } ?>">

    <?php echo $content_code; ?>
    </div>

    <?php if (isset($footer_code)){ echo $footer_code;} ?>
    <?php
    if (isset($mainScripts)){
        foreach($mainScripts as $script){
            echo '<script type="text/javascript" src="'.$script.'"></script>';
        }
    }
    if (isset($scripts)){
        foreach($scripts as $script){
            if (is_array($script)){
                echo '<!--[if '.$script['comment'].']><script type="text/javascript" src="' . $script['url'] . '"></script><![endif]-->';
            }else {
                echo '<script type="text/javascript" src="' . $script . '"></script>';
            }
        }
    }
    ?>
    </body>
</html>
  