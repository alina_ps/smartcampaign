<?php
/**
 * User: Frantzolakis Manolis
 * Date: 21/7/2016
 * Time: 2:59 μμ
 */
?>
<?php
if (isset($successmsg) || isset($errormsg)){
    ?>
    <div class="row top-buffer">
        <div class="col-md-12">
            <?php
            if (isset($successmsg)){
                $class = " alert-info";
                $msg = $successmsg;
            }else if (isset($errormsg)){
                $class = " alert-danger";
                $msg = $errormsg;
            }else {
                $class = " hidden";
                $msg = "";
            }
            ?>
            <div id="request-msg" class="alert <?php echo $class;?>"><?php echo $msg; ?></div>
        </div>
    </div>
<?php } ?>
<div class="row top-buffer">
    <div class="col-md-12">
        <a href="<?php echo Engine::url(array('controller'=>'user','action'=>'create'));?>" class="btn btn-primary"><?php echo Translator::getTranslation('Create').' '.Translator::getTranslation('user_gen'); ?></a>
    </div>
</div>
<div class="row top-buffer">
    <div class="col-md-12 col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><?php echo Translator::getTranslation('Users');?></h3></div>
                    <table id="tbl-users" data-classes="table table-responsive table-hover table-stripped" class="table table-responsive">
                        <thead>
                        <tr>
                            <th data-field="id" data-sortable="true" class="text-center" data-searchable="true"><?php echo Translator::getTranslation('DB ID');?></th>
                            <th data-field="username" data-sortable="true" class="text-center" data-searchable="true"><?php echo Translator::getTranslation('Username');?></th>
                            <th data-field="role" class="text-center"><?php echo Translator::getTranslation('Role');?></th>
                            <th data-align="center" data-formatter="userActionFormatter" class="text-center"></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
