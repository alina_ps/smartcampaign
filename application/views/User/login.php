<div class="row large-buffer">
    <div class="col-md-12">
        <div class="jumbotron login-jumbotron">
            <?php if (isset($errormsg)) { ?>
                <div class="alert alert-danger alert-dismissible login-error" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="msg"><?php echo $errormsg;?></span>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h2>Smart Campaign</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <form id="login-form" class="login-form form-horizontal" role="form" method="POST" action="<?php echo $loginUrl;?>">
                        <div class="form-group">
                            <label for="username" class="col-sm-3 control-label"><?php echo Translator::getTranslation("Username");?></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="<?php echo Translator::getTranslation("Username");?>" required="required" id="username" name="username" value="<?php if ( isset ($username)){ echo $username;}?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-3 control-label"><?php echo Translator::getTranslation("Password");?></label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" placeholder="<?php echo Translator::getTranslation("Password");?>" required="required" id="password" name="password" value="<?php if ( isset ($password)){ echo $password;}?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-6">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" name="submit" value="<?php echo Translator::getTranslation("Login");?>">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>