<?php
/**
 * User: Frantzolakis Manolis
 * Date: 21/7/2016
 * Time: 3:54 μμ
 */

?>
<?php
    if (isset($successmsg) || isset($errormsg)){
?>
<div class="row top-buffer">
    <div class="col-md-12">
        <?php
        if (isset($successmsg)){
            $class = " alert-info";
            $msg = $successmsg;
        }else if (isset($errormsg)){
            $class = " alert-danger";
            $msg = $errormsg;
        }else {
            $class = " hidden";
            $msg = "";
        }
        ?>
        <div id="request-msg" class="alert <?php echo $class;?>"><?php echo $msg; ?></div>
    </div>
</div>
<?php } ?>
<div class="row top-buffer">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><?php echo Translator::getTranslation('Create').' '.Translator::getTranslation('user_gen');?></h3></div>
            <div class="panel-body">
                <form id="create-user-form" class="form-horizontal create-user-form" role="form" method="POST" action="<?php echo Engine::url(array('lang'=>Engine::$LANGUAGE, 'controller'=>'user','action'=>'create'));?>">
                    <div class="form-group">
                        <label for="username" class="col-sm-2 col-xs-4 control-label"><?php echo Translator::getTranslation('Username');?> <span class="required-star">*</span></label>
                        <div class="col-sm-2 col-xs-2 visible-xs">
                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo Translator::getTranslation("required").'. '.Translator::getFormattedTranslation("maximum_length", "30");?>"></i>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="username" name="username" placeholder="<?php echo Translator::getTranslation('Username');?>"/>
                            <div class="help-block"></div>
                        </div>
                        <div class="col-sm-2 hidden-xs">
                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo Translator::getTranslation("required").'. '.Translator::getFormattedTranslation("maximum_length", "30");?>"></i>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-2 col-xs-4 control-label"><?php echo Translator::getTranslation('Password');?> <span class="required-star">*</span></label>
                        <div class="col-sm-2 col-xs-2 visible-xs">
                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo Translator::getTranslation("required");?>"></i>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <input type="password" class="form-control" id="password" name="password" placeholder="<?php echo Translator::getTranslation('Password');?>"/>
                            <div class="help-block"></div>
                        </div>
                        <div class="col-sm-2 col-xs-2 hidden-xs">
                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo Translator::getTranslation("required");?>"></i>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="role" class="col-sm-2 col-xs-4 control-label"><?php echo Translator::getTranslation("Role");?> <span class="required-star">*</span></label>
                        <div class="col-sm-2 col-xs-2 visible-xs">
                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo Translator::getTranslation("required");?>"></i>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <select name="role" id="role" class="form-control">
                                <?php
                                $roles = array('admin', 'user');
                                echo '<option value="-1" selected></option>';
                                foreach ($roles as $role){
                                    echo '<option value="'.$role.'" >'.Translator::getTranslation($role).'</option>';
                                }
                                ?>
                            </select>
                            <div class="help-block"></div>
                        </div>
                        <div class="col-sm-2 col-xs-2 hidden-xs">
                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo Translator::getTranslation("required");?>"></i>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="submit" class="col-sm-2 control-label"></label>
                        <div class="col-sm-6">
                            <input class="btn btn-primary" type="submit" name="submit" value="<?php echo Translator::getTranslation('Create');?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-6">
                            <?php echo Translator::getTranslation('required_fields');?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
