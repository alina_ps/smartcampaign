<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 1/3/2017
 * Time: 6:14 μμ
 */
?>
<?php

if (isset($successmsg) || isset($errormsg)){
    ?>
    <div class="row top-buffer">
        <div class="col-md-12">
            <?php
            if (isset($successmsg)){
                $class = " alert-info";
                $msg = $successmsg;
            }else if (isset($errormsg)){
                $class = " alert-danger";
                $msg = $errormsg;
            }else {
                $class = " hidden";
                $msg = "";
            }
            ?>
            <div id="request-msg" class="alert <?php echo $class;?>"><?php echo $msg; ?></div>
        </div>
    </div>
<?php } ?>
<div class="row top-buffer">
    <div class="col-md-12">
        <a href="<?php echo Engine::url(array('controller'=>'campaign','action'=>'create'));?>" class="btn btn-primary"><?php echo 'Προσθήκη νέας αποστολής'; ?></a>
    </div>
</div>
<div class="row top-buffer">
    <div class="col-md-12 col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><?php echo 'Καμπάνιες';?></h3></div>
                    <table id="tbl-campaigns" data-classes="table table-responsive table-hover table-stripped" class="table table-responsive">
                        <thead>
                        <tr>
                            <!--   <th data-field="unique_id" data-sortable="true" class="text-center" data-searchable="true"><?php echo "ID";?></th> -->
                            <th data-field="name" class="text-center"><?php echo 'Name';?></th>
                            <th data-field="description" class="text-center"><?php echo 'Description';?></th>
                      <!--      <th data-field="legs" class="text-center"><?php echo 'Βήματα';?></th> -->
                            <th data-field="budget" data-field="status" class="text-center"><?php echo 'Budget';?></th>
                            <th data-field="market_base" data-field="status" class="text-center"><?php echo 'Market base';?></th>
                            <th data-field="items" data-field="status" class="text-center"><?php echo 'Items';?></th>
                            <th data-align="center" data-formatter="campaignActionFormatter" class="text-center"></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>