<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/3/2017
 * Time: 10:30 πμ
 */
if (isset($successmsg) || isset($errormsg)){
    ?>
    <div class="row top-buffer">
        <div class="col-md-12">
            <?php
            if (isset($successmsg)){
                $class = " alert-info";
                $msg = $successmsg;
            }else if (isset($errormsg)){
                $class = " alert-danger";
                $msg = $errormsg;
            }else {
                $class = " hidden";
                $msg = "";
            }
            ?>
            <div id="request-msg" class="alert <?php echo $class;?>"><?php echo $msg; ?></div>
        </div>
    </div>
<?php } ?>
<?php
?>
<div class="row top-buffer">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><?php echo 'Προβολή: ' . $delivery['description'];?></h3></div>
            <div class="panel-body">
                <div class="row" role="tabpanel">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#settings" aria-controls="general" role="tab" data-toggle="tab">Ρυθμίσεις </a></li>
                        <li role="presentation"><a href="#destinations" aria-controls="general" role="tab" data-toggle="tab">Προορισμοί </a></li>
                        <li role="presentation"><a href="#combinations" aria-controls="general" role="tab" data-toggle="tab">Συνδυασμοί διαδρομών </a></li>
                    </ul>
                </div>
                <div class="row">
                    <div class="container-fluid">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="settings">
                                <form id="edit-delivery-form" class="form-horizontal create-delivery-form" role="form" method="POST" action="<?php echo Engine::url(array('controller'=>'delivery','action'=>'edit', 'id'=>$delivery['id']));?>">
                                    <input type="hidden" id="delivery_id" name="delivery_id" value="<?php echo $delivery['id'];?>"/>
                                    <div class="form-group">
                                        <label for="description" class="col-sm-2 col-xs-4 control-label">Περιγραφή <span class="required-star">*</span></label>
                                        <div class="col-sm-2 col-xs-2 visible-xs">
                                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo "Υποχρεωτικό. Μέγιστο εύρος πεδίου 100 χαρακτήρες";?>"></i>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="description" name="description" value="<?php echo $delivery['description'];?>"/>
                                            <div class="help-block"></div>
                                        </div>
                                        <div class="col-sm-2 hidden-xs">
                                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo "Υποχρεωτικό. Μέγιστο εύρος πεδίου 100 χαρακτήρες";?>"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="code" class="col-sm-2 col-xs-4 control-label">Κωδικός <span class="required-star">*</span></label>
                                        <div class="col-sm-2 col-xs-2 visible-xs">
                                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo "Υποχρεωτικό. Ακέραιος αριθμός.";?>"></i>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="code" name="code" value="<?php echo $delivery['code'];?>"/>
                                            <div class="help-block"></div>
                                        </div>
                                        <div class="col-sm-2 hidden-xs">
                                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo "Υποχρεωτικό. Ακέραιος αριθμός.";?>"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="status" class="col-sm-2 col-xs-4 control-label">Κατάσταση <span class="required-star">*</span></label>
                                        <div class="col-sm-2 col-xs-2 visible-xs">
                                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo "Υποχρεωτικό. Ακέραιος αριθμός.";?>"></i>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="status" name="status" value="<?php echo $delivery['status'];?>"/>
                                            <div class="help-block"></div>
                                        </div>
                                        <div class="col-sm-2 hidden-xs">
                                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo "Υποχρεωτικό. Ακέραιος αριθμός.";?>"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="deliveryDate" class="col-sm-2 col-xs-4 control-label">Ημερομηνία αποστολής <span class="required-star">*</span></label>
                                        <div class="col-sm-2 col-xs-2 visible-xs">
                                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo "Υποχρεωτικό. Ακέραιος αριθμός.";?>"></i>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <input type="date" class="form-control" id="deliveryDate" name="deliveryDate" value="<?php echo $delivery['delivery_date'];?>"/>
                                            <div class="help-block"></div>
                                        </div>
                                        <div class="col-sm-2 hidden-xs">
                                            <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo "Υποχρεωτικό. Ακέραιος αριθμός.";?>"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="submit" class="col-sm-2 control-label"></label>
                                        <div class="col-sm-6">
                                            <input class="btn btn-primary" type="submit" name="submit" value="Αποθήκευση">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                         <div class="col-sm-offset-2 col-sm-6">Τα πεδία με <span class="required-star">*</span> είναι υποχρεωτικά.
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="destinations">
                                <div class="row">
                                    <input type="hidden" name="delivery_id" value="<?php echo $delivery['id'];?>"/>
                                    <div id="ad-container" class="col-md-7 ad-container">
                                  <!--      <div class="add-destination-container">
                                            <div class="add-destination-container">
                                                <button class="btn btn-success add-destination"><i class="glyphicon glyphicon-plus"></i>Προσθήκη νέου προορισμού</button>
                                            </div>
                                        </div> -->
                                        <div class="destinations-container">
                                            <ol class="sortable sortable-destinations">
                                                <?php
                                                if(isset($destinations)){
                                                    foreach ($destinations as $destination) {?>
                                                        <li>
                                                            <div class="destination-container"
                                                                 data-id="<?php echo $destination['id'] ?>">
                                                                <span
                                                                    class="disclose sortable-icon glyphicon glyphicon-triangle-top"
                                                                    data-toggle="tooltip"
                                                                    title="Expand to edit destination."
                                                                    data-placement="top"></span>
                                                                <span
                                                                    class="destination-text"><?php echo $destination['name'] ?></span>
                                                          <!--      <span
                                                                    class="remove-destination glyphicon glyphicon-remove"
                                                                    data-toggle="tooltip" title="Διαγραφή προορισμού"
                                                                    data-placement="top"></span> -->
                                                                <span
                                                                    class="process-destination glyphicon glyphicon-menu-right hidden"
                                                                    data-toggle="tooltip"
                                                                    title="Click to see destination."
                                                                    data-placement="top"></span>
                                                                <div class="destination-input-container">
                                                                    <div class="input-group col-sm-6 col-md-6 ">
                                                                        <input class="form-control1 road-input"
                                                                               type="text" name="road_text[]"
                                                                               value="<?php echo $destination['road'] ?>">
                                                                    </div>
                                                                    <div class="input-group  col-sm-6 col-md-6 ">
                                                                        <input class="form-control1 city-input"
                                                                               type="text" name="city_text[]"
                                                                               value="<?php echo $destination['city'] ?>">
                                                                    </div>
                                                                    <div class="input-group  col-sm-6 col-md-6 ">
                                                                        <input class="form-control1 postalcode-input"
                                                                               type="text" name="postal_text[]"
                                                                               value="<?php echo $destination['postal_code'] ?>">
                                                                    </div>
                                                                    <input type="hidden" class="encodedanswers"
                                                                           value='<?php echo $destination['items'] ?>'>
                                                                    <input type="hidden" class="latitude"
                                                                           value="<?php echo $destination['latitude'] ?>">
                                                                    <input type="hidden" class="longitude"
                                                                           value="<?php echo $destination['longitude'] ?>">
                                                                    <input type="hidden" class="distance" value="<?php echo $destination['distance'] ?>">
                                                                    <input type="hidden" class="duration" value="<?php echo $destination['duration'] ?>">
                                                                    <div class="input-group  col-sm-6 col-md-6">
                                                                        <button class="btn btn-success checkAddress"
                                                                                type="button" disabled>Έλεγχος Διεύθυνσης</button>
                                                                        <button
                                                                            class="btn btn-success editDestination"
                                                                            type="button">Προβολή Ειδών
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </ol>
                                        </div>
                                        <div class="view-combinations-container hidden">
                                            <button class="btn btn-primary view-combinations">Συνδυασμοί διαδρομών</button>
                                        </div>
                                    </div>
                                    <div id="ed-container" class="col-md-7 ed-container hidden" >
                                        <input type="hidden" id="destination_id" name="destination_id" value=""/>
                                        <div class="add-item-container">
                                      <!--      <div class="add-item-container">
                                                <button class="btn btn-success add-item"><i class="glyphicon glyphicon-plus"></i>Προσθήκη νέου είδους</button>
                                            </div> -->
                                        </div>
                                        <div class="items-container">
                                            <ol class="sortable sortable-items">
                                            </ol>
                                        </div>
                                     <!--   <div class="save-item-container hidden">
                                            <button class="btn btn-success save-item">Αποθήκευση</button>
                                        </div> -->
                                        <div class="back-container">
                                            <button class="btn btn-success back-button">Επιστροφή</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="combinations">
                                <div class="row">
                                    <div id="combination-container" class="col-md-7 combination-container">
                                        <div class="combinations-container">
                                            <ol class="sortable sortable-combinations">
                                                <li>
                                                <?php
                                                $details = json_decode($delivery['choice'],true);
                                                foreach ($details as $detail){
                                                ?>

                                                    <div class="truck-container">
                                                        <div class="truck-input-container ">
                                                            <label for="destin" class="destin control-label"> Προορισμός: <?php echo $detail['destination']?></label>                                     '
                                                        </div>
                                                        <div class="truck-input-container">
                                                            <label for="truck" class="truck control-label">Φορτηγό: <?php echo $detail['truck']?></label>
                                                          </div>
                                                        <div class="truck-input-container">
                                                           <label for="duration" class="duration control-label">Χρόνος: <?php echo $detail['duration']?></label>
                                                        </div>
                                                        <div class="truck-input-container">
                                                            <label for="distance" class="distance control-label">Απόσταση: <?php echo $detail['distance']?></label>
                                                        </div>
                                                        <div class="truck-input-container">
                                                            <label for="cost" class="cost control-label">Κόστος: <?php echo $detail['cost']?></label>
                                                        </div>
                                                        <div class="truck-input-container">
                                                            <label for="staff" class="staff control-label">Κόστος προσωπικού: <?php echo $detail['staff_cost']?></label>
                                                        </div>
                                                        <?php
                                                            if (isset($detail['stay'])) {
                                                                    echo '<div class="truck-input-container">';
                                                                    echo '<label for="stay" class="stay control-label">Κόστος διανυκτέρευσης: ' . $detail['stay'] . '</label>' ;
                                                                    echo '</div>';
                                                                }
                                                        ?>
                                                        <div class="truck-input-container">
                                                            <label for="total_cost" class="total_cost control-label">Συνολικό κόστος: <?php echo $detail['total_cost']?></label>
                                                        </div>
                                                    </div>
                                                    <?php }?>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
