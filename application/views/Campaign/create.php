<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/3/2017
 * Time: 5:31 μμ
 */

if (isset($successmsg) || isset($errormsg)){
    ?>
    <div class="row top-buffer">
        <div class="col-md-12">
            <?php
            if (isset($successmsg)){
                $class = " alert-info";
                $msg = $successmsg;
            }else if (isset($errormsg)){
                $class = " alert-danger";
                $msg = $errormsg;
            }else {
                $class = " hidden";
                $msg = "";
            }
            ?>
            <div id="request-msg" class="alert <?php echo $class;?>"><?php echo $msg; ?></div>
        </div>
    </div>
<?php } ?>
<?php
?>
<div class="row top-buffer">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><?php echo 'Create new campaign'?></h3></div>
            <div class="panel-body">
                <div class="row" role="tabpanel">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active" data-id="1"><a href="#settings" aria-controls="general" role="tab" data-toggle="tab">Settings </a></li>
                        <li role="presentation" class="hidden" data-id="2"><a href="#destinations" aria-controls="general" role="tab" data-toggle="tab">Means of Marketing </a></li>
                        <li role="presentation" class="hidden" data-id="3"><a href="#combinations" aria-controls="general" role="tab" data-toggle="tab">Marketing policy </a></li>
                    </ul>
                </div>
                <div class="row">
                    <div class="container-fluid">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="settings">
                            <form id="create-campaign-form" class="form-horizontal create-campaign-form" role="form" method="POST" action="<?php echo Engine::url(array('controller'=>'campaign','action'=>'create'));?>">
                                <input type="hidden" id="campaign_id" name="campaign_id" value=""/>
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 col-xs-4 control-label">Name <span class="required-star">*</span></label>
                                    <div class="col-sm-2 col-xs-2 visible-xs">
                                        <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo "Required. Text up to 200 characters.";?>"></i>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="name" name="name"/>
                                        <div class="help-block"></div>
                                    </div>
                                    <div class="col-sm-2 hidden-xs">
                                        <i class="fa fa-info-circle js-link" data-toggle="tooltip" data-placement="top" title="<?php echo "Required. Text up to 200 characters.";?>"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="col-sm-2 col-xs-4 control-label">Description </label>
                                    <div class="col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="description" name="description"/>
                                        <div class="help-block"></div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="budget" class="col-sm-2 col-xs-4 control-label">Budget <span class="required-star">*</span></label>

                                    <div class="col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="budget" name="budget"/>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="market_base" class="col-sm-2 col-xs-4 control-label">Market base <span class="required-star">*</span></label>

                                    <div class="col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="market_base" name="market_base"/>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <?php
                                foreach($items as $item){ ?>
                                    <div class="form-group">
                                    <label for="item" class="col-sm-2 col-xs-4 control-label"><?php echo $item['type'];?></label>
                                        <div class="col-sm-6 col-xs-12">
                                            <select class="form-control" name="type" id="<?php echo $item['type'];?>">
                                                <option value="<?php echo $item['id'] ?>">ΝΑΙ</option>
                                                <option value="-1">ΟΧΙ</option>
                                            </select>
                                        </div>
                                </div>

                            <?php    }
                                ?>
                         <!--       <div class="form-group">
                                    <div id="ad-container" class="col-md-7 ad-container">
                                        <fieldset>
                                            <legend><small>Means of Marketing</small></legend>
                                        </fieldset>
                                        <div class="add-item-container">
                                            <div class="add-item-container">
                                                <input type="button" class="btn btn-default add-item" value="Προσθήκη νέου είδους"></input>
                                            </div>
                                        </div>
                                        <div class="items-container">
                                            <ol class="sortable sortable-items">
                                            </ol>
                                        </div>
                                        <div class="save-item-container hidden">
                                            <button class="btn btn-success save-item">Calculate Campaign</button>
                                        </div>
                                    </div>
                                </div>

-->
                                <div class="save-settings-container col-md-8 col-sm-8 col-xs-12">
                                    <input type="button" class="btn btn-primary save-settings" value="Calculate campaign">
                                </div>
                          <!--      <div class="form-group" hidden>
                                    <label for="submit" class="col-sm-2 control-label"></label>
                                    <div class="col-sm-6">
                                        <input class="btn btn-primary save-settings" type="submit" name="submit" value="Next">
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-6">Fields with <span class="required-star">*</span> are required.
                                    </div>
                                </div>
                            </form>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="combinations">
                                <div class="row">
                                    <fieldset>
                                        <legend><small>Campaigns' marketing policy</small></legend>
                                    </fieldset>
                                    <div class="form-group">
                                        <label for="objective" class="col-sm-2 col-xs-4 control-label">Objective function (Maximum)</label>

                                        <div class="col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="objective" name="objective"/>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </div>

                                    <?php
                                    $i=0;
                                    foreach($items as $item){?>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="item" class="col-sm-2 col-xs-4 control-label"><?php echo $item['type'];?></label>
                                        <div class="col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="<?php echo $item['type'].'1'; ?>" name="<?php echo $item['type']; ?>"/>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </div>

                                    <?php    $i++;}?>
                                <div class="save-campaign-container">
                                    <button class="btn btn-primary save-campaign">Αποθήκευση Καμπάνιας</button>
                                </div>

                                  <!--  <div id="combination-container" class="col-md-7 combination-container">
                                        <div class="add-truck-container">
                                            <div class="add-truck-container">
                                                <button class="btn btn-success add-destination"><i class="glyphicon glyphicon-plus"></i>Προσθήκη νέου προορισμού</button>
                                            </div>
                                        </div>
                                        <div class="combinations-container">
                                            <ol class="sortable sortable-combinations">
                                            </ol>
                                        </div>
                                        <div class="save-destinations-container">
                                            <button class="btn btn-primary save-destinations">Αποθήκευση Αποστολής</button>
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                           <!-- <div role="tabpanel" class="tab-pane" id="combinations">
                                <div class="row">
                                    <div id="showTable">
                                        <table id="tbl-combinations" data-classes="table table-responsive table-hover table-stripped" class="table table-responsive">
                                            <thead>
                                            <tr>

                                                <th data-field="combination" class="text-center"><?php echo 'Συνδυασμός';?></th>
                                                <th data-field="destinations" class="text-center"><?php echo 'Προορισμοί';?></th>
                                                <th data-field="truck" class="text-center"><?php echo 'Φορτηγό';?></th>
                                                <th data-field="total_cost" class="text-center"><?php echo 'Συνολικό κόστος';?></th>
                                                <th data-formatter="chooseFormatter" data-field="choice" class="text-center"><?php echo 'Επιλογή';?></th>
                                                <th data-align="center" data-formatter="deliveryActionFormatter" class="text-center"></th>
                                            </tr>
                                            </thead>
                                        </table>
                                        <div class="save-destinations-container">
                                            <button class="btn btn-primary save-table-destinations">Αποθήκευση Αποστολής</button>
                                        </div>
                                    </div>
                                    <div id="showItem" class="hidden">
                                        <div id="combination-container2" class="col-md-7 combination-container">
                                            <div class="combinations-container2">
                                                <ol class="sortable sortable-combinations2">
                                                </ol>
                                            </div>
                                            <div class="back-container">
                                                <button class="btn btn-success back-show">Επιστροφή</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

