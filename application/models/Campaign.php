<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/3/2017
 * Time: 4:51 μμ
 */

namespace application\models;


class Campaign extends \BaseModel {

    private $id = null;
    private $user_id = null;
    private $name = null;
    private $description = null;
    private $budget = null;
    private $market_base = null;
    private $items = null;

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param null $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param null $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return null
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * @param null $budget
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;
    }

    /**
     * @return null
     */
    public function getMarketBase()
    {
        return $this->market_base;
    }

    /**
     * @param null $market_base
     */
    public function setMarketBase($market_base)
    {
        $this->market_base = $market_base;
    }

    /**
     * @return null
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param null $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }



    public function __construct(){
        parent::__construct("campaign");
    }


    public function get($args = array(), $create = false){
        $selectArguments = $this->extractSelectArguments($args);

        $result = $this->db->buildSelectQuery($this->DB_TABLE, $selectArguments['columns'],$selectArguments['where'],$selectArguments['order'],$selectArguments['limit'], $selectArguments['offset'] );

        if ($result){
            if ($create){
                $newResult = array();
                foreach($result as &$res){
                    $item = new Item();
                    $item->create($res);
                    $newResult[] = $item;
                }
                unset($res);
                return $newResult;
            }
            return $result;
        }
        return false;
    }

    public function create($args){
        foreach($args as $index=>$value){
            $this->{$index} = $value;
        }
    }

    public function save()
    {
        $args = array();

        $args['user_id'] = $this->user_id;
        $args['name'] = $this->name;
        $args['description'] = $this->description;
        $args['budget'] = $this->budget;
        $args['market_base'] = $this->market_base;
        $args['items'] = $this->items;

        $result = $this->db->buildInsertQuery($this->DB_TABLE, $args);
        if ($result){
            $this->id = $this->db->getLastInsertId();
            return $this->id;
        }else {
            return false;
        }
    }

    public function update($args = array()){
        $fields = null;
        if (empty($args)){
            $fields = array();
            $fields['id'] = $this->id;
            $fields['user_id'] = $this->user_id;
            $fields['name'] = $this->name;
            $fields['description'] = $this->description;
            $fields['budget'] = $this->budget;
            $fields['market_base'] = $this->market_base;
            $fields['items'] = $this->items;
        }else {
            $fields = array();
            foreach($args as $arg){
                $fields[$arg] = $this->{$arg};
            }
        }

        $result = $this->db->buildUpdateQuery($this->DB_TABLE, $fields, array('id'=>$this->id));

        if ($result){
            return true;
        }
        return false;
    }
    public function delete($id){

        $result = $this->db->buildDeleteQuery($this->DB_TABLE,array('id'=>intval($id)));

        if ($result){
            return true;
        }
        return false;

    }
}