<?php

namespace application\models;

use EngineError;

class Item extends \BaseModel {

    private $id = null;
    private $type = null;
    private $description = null;
    private $ROI = null;
/*    private $min_spend = null;
    private $max_spend = null;
    private $min_cost = null;*/
    private $cpereuro = null;
    private $user_id = null;
    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param null $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return null
     */
    public function getROI()
    {
        return $this->ROI;
    }

    /**
     * @param null $ROI
     */
    public function setROI($ROI)
    {
        $this->ROI = $ROI;
    }


    /**
     * @return null
     */
    public function getCpereuro()
    {
        return $this->cpereuro;
    }

    /**
     * @param null $cpereuro
     */
    public function setCpereuro($cpereuro)
    {
        $this->cpereuro = $cpereuro;
    }


    /**
     * @return null
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param null $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }


    public function __construct(){
        parent::__construct("items");
    }


    public function get($args = array(), $create = false){
        $selectArguments = $this->extractSelectArguments($args);

        $result = $this->db->buildSelectQuery($this->DB_TABLE, $selectArguments['columns'],$selectArguments['where'],$selectArguments['order'],$selectArguments['limit'], $selectArguments['offset'] );

        if ($result){
            if ($create){
                $newResult = array();
                foreach($result as &$res){
                    $item = new Item();
                    $item->create($res);
                    $newResult[] = $item;
                }
                unset($res);
                return $newResult;
            }
            return $result;
        }
        return false;
    }

    public function create($args){
        foreach($args as $index=>$value){
            $this->{$index} = $value;
        }
    }

    public function save()
    {
        $args = array();
        $args['id'] = $this->id;
        $args['type'] = $this->type;
        $args['description'] = $this->description;
        $args['ROI'] = $this->ROI;
    /*    $args['min_spend'] = $this->min_spend;
        $args['max_spend'] = $this->max_spend;
        $args['min_cost'] = $this->min_cost;*/
        $args['cpereuro'] = $this->cpereuro;
        $args['user_id'] = $this->user_id;

        $result = $this->db->buildInsertQuery($this->DB_TABLE, $args);
        if ($result){
            $this->id = $this->db->getLastInsertId();
            return $this;
        }else {
            return false;
        }
    }

    public function update($args = array()){
        $fields = null;
        if (empty($args)){
            $fields = array();
            $fields['id'] = $this->id;
            $fields['type'] = $this->type;
            $fields['description'] = $this->description;
            $fields['ROI'] = $this->ROI;
        /*    $fields['min_spend'] = $this->min_spend;
            $fields['max_spend'] = $this->max_spend;
            $fields['min_cost'] = $this->min_cost;*/
            $fields['cpereuro'] = $this->cpereuro;
            $fields['user_id'] = $this->user_id;
        }else {
            $fields = array();
           foreach($args as $arg){
                $fields[$arg] = $this->{$arg};
            }
        }

        $result = $this->db->buildUpdateQuery($this->DB_TABLE, $fields, array('id'=>$this->id));

        if ($result){
            return true;
        }
        return false;
    }
    public function delete($id){

        $result = $this->db->buildDeleteQuery($this->DB_TABLE,array('id'=>intval($id)));

        if ($result){
            return true;
        }
        return false;

    }
}