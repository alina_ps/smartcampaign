<?php
/**
 * User: Frantzolakis Manolis
 * Date: 19/7/2016
 * Time: 1:01 μμ
 */

namespace application\models;

use EngineError;

class User extends \BaseModel implements \Serializable {

    private $id = null;
    private $username = null;
    private $password = null;
    private $role = null;


    public function __construct($table = "users")
    {
        parent::__construct($table);
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param null $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return null
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param null $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return null
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param null $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    public function validate($elements, $mode = 0){

        $result = array();
        if ($elements == null || !is_array($elements) || count($elements) == 0){
            $result['success'] = false;
            $result['error'] = EngineError::getErrorMessage("100");
            return $result;
        }

        if ($mode == 0){
            $dbresult = $this->validateLogin($elements);
            if ($dbresult === false){
                $result['success'] = false;
                $result['error'] = EngineError::getErrorMessage("101");
            }else {
                $result['success'] = true;
                $result['user'] = $dbresult;
            }
        }else if ($mode == 1){
            $result = $this->validateRegister($elements);
        }

        return $result;
    }

    private function validateLogin($elements){

        $result = array();

        if (!isset($elements['username']) || strlen(trim($elements['username'])) == 0){
            $result['success'] = false;
            $result['error'] = EngineError::getErrorMessage("101");
            return $result;
        }

        if (!isset($elements['password']) || strlen(trim($elements['password'])) == 0){
            $result['success'] = false;
            $result['error'] = EngineError::getErrorMessage("101");
            return $result;
        }

        $user = $this->get(array('where'=>array('username'=>$elements['username'], 'password'=>md5($elements['password']))), true);
        
        if ($user){
            return $user[0];
        }
        return false;

    }

    public function checkUsernameValidity($username, $id){

        if (strlen(trim($username)) == 0){
            return false;
        }

        $array = array('Index', 'History', 'Api', 'Device', 'User');

        for ($i =0 ; $i < count($array); $i++){
            if (strcasecmp($username, $array[$i]) == 0){
                return false;
            }
        }

        $res = $this->get(array('where'=>array('username'=>$username)));
        if ($res){
            if ($res[0]['id'] != $id){
                return false;
            }
        }

        return true;

    }

    private function validateRegister($elements){

    }

    public function get($args = array(), $create = false){
        $selectArguments = $this->extractSelectArguments($args);

        $result = $this->db->buildSelectQuery($this->DB_TABLE, $selectArguments['columns'],$selectArguments['where'],$selectArguments['order'],$selectArguments['limit'], $selectArguments['offset'] );

        if ($result){
            if ($create){
                $newResult = array();
                foreach($result as &$res){
                    $user = new User();
                    $password = $res['password'];
                    unset($res['password']);
                    $user->create($res);
                    $user->setPassword($password);
                    $newResult[] = $user;
                }
                unset($res);
                return $newResult;
            }
            return $result;
        }
        return false;
    }

    public function create($args){
        foreach($args as $index=>$value){
            $this->{$index} = $value;
        }
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return serialize(array(
            'username'=>$this->username,
            'role'=>$this->role,
            'id'=>$this->id
        ));
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);
        $this->username = $data['username'];
        $this->role = $data['role'];
        $this->id = $data['id'];
    }

    public function save()
    {
        if ($this->username == null || $this->password == null) {
            return false;
        }
        $args = array();
        $args['username'] = $this->username;
        $args['password'] = md5($this->password);
        $args['role'] = $this->role;
        $result = $this->db->buildInsertQuery($this->DB_TABLE, $args);
        if ($result){
            $this->id = $this->db->getLastInsertId();
            return $this;
        }else {
            return false;
        }
    }

    public function update($args = array()){
        $fields = null;
        if (empty($args)){
            $fields = array();
            $fields['username'] = $this->username;
            $fields['password'] = md5($this->password);
            $fields['role'] = $this->role;
        }else {
            $fields = array();
            foreach($args as $arg){
                if ($arg === "password"){
                    $fields[$arg] = md5($this->{$arg});
                    continue;
                }
                $fields[$arg] = $this->{$arg};
            }
        }


        $result = $this->db->buildUpdateQuery($this->DB_TABLE, $fields, array('id'=>$this->id));
        if ($result){
            return true;
        }
        return false;
    }

    public function delete(){
        $result = $this->db->buildDeleteQuery($this->DB_TABLE,array('id'=>$this->id));

        if ($result){
            return true;
        }
        return false;
    }


}