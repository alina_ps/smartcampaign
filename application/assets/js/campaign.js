/**
 * Created by user on 2/3/2017.
 */
var destinationsList;
var combinationsList;
var combinationsList2;
var itemsList;
var availableItems;
var deliveryAddress;
var deliveryDistance;
var deliveryDistanceText;
var deliveryDuration;
var deliveryDurationText;
var installationTime = 14400 /*4 hours*/
var maxTime = 32400 /*9 hours*/
var origin = 'Κυπαρισσίων 11, Αγία Παρασκευή 153 43, Ελλάδα';
var combinedDestinations;
var distances;
var durations;
var truckList;
var delivery = {};

$(document).ready(function () {

    $('[data-toggle="tooltip"]').tooltip();

    if ($('#tbl-campaigns').length > 0){
        $('#tbl-campaigns').bootstrapTable({
            locale: engineLanguage,
            sidePagination: 'server',
            pagination: true,
            pageSize: 50,
            pageList: [50, 100],
            url: ajaxUrl +"/getCampaigns",
            search: true,
            showFooter: false,
            uniqueId: 'id'
        });
        $('#tbl-deliveries').on('post-body.bs.table', function (e) {
            $('[data-toggle="tooltip"]').tooltip();
            $('.delete-campaign').on('click', function(e){
                e.preventDefault();
                vDeleteCampaign($(this));
            });
        });
    }

    if ($('#tbl-combinations').length > 0){
        $('#tbl-combinations').bootstrapTable({
            locale: engineLanguage,
            sidePagination: 'server',
            pagination: true,
            pageSize: 125,
            pageList: [200, 400],
            url: ajaxUrl +"/getCombinations",
            search: true,
            showFooter: false,
            uniqueId: 'id'
        });
        $('#tbl-combinations').on('post-body.bs.table', function (e) {
            $('[data-toggle="tooltip"]').tooltip();
            $('.view-details').on('click', function(e){
                e.preventDefault();
                viewDetails($(this));
            });
       });
   }

/*    if ($('select').length > 0)
        $('select').selectpicker({
            noneSelectedText: noneSelected
        });
*/

    if ($('.date-input').length > 0) {
        $('.date-input').datetimepicker(
            {
                locale: datepickerlocale,
                format: 'DD/MM/YYYY'
            }
        );
    }

    if ($('.nav-tabs').length > 0){
        $('.nav-tabs a').click(function(e){
            e.preventDefault();
            $(this).tab('show');
        });
        $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
            var id = $(e.target).attr("href").substr(1);
            window.location.hash = id;
        });
        var hash = window.location.hash;
        $('.nav-tabs a[href="' + hash + '"]').tab('show');
    }

    $('.save-settings').on('click', function (e){

        if(!$('#name').val() || !$('#budget').val() || !$('#market_base').val()){
            alert("Παρακαλώ συμπληρώστε τα πεδία στη φόρμα, είναι υποχρεωτικά για να συνεχίσετε!");
            return;
        }

        var rand = Math.floor(Math.random()*200)-200;
        $('#campaign_id').val(rand);
        var list = $('ul.nav-tabs');
        list.children('li').each(function() {
            if($(this).attr('data-id') == 3) {
                $(this).removeClass('hidden');
                $(this).find('a').trigger('click');
           }
        });

        var tv = 0;
        var seo = 0;
        var adwords = 0;
        var facebook = 0;

        if($('#TV').length > 0){
            if($('#TV').val()>0){
                tv=$('#TV').val();
            }else{
                tv=0;
            }
        }
        if($('#SEO').length > 0){
            if($('#SEO').val()>0){
                seo=$('#SEO').val();
            }else{
                seo=0;
            }
        }
        if($('#Adwords').length > 0){
            if($('#Adwords').val()>0){
                adwords=$('#Adwords').val();
            }else{
                adwords=0;
            }
        }
        if($('#Facebook').length > 0){
            if($('#Facebook').val()>0){
                facebook=$('#Facebook').val();
            }else{
                facebook=0;
            }
        }

        $.ajax({
            url: ajaxUrl+"/calculateCampaign",
            type: "POST",
            dataType: "JSON",
            data: {name: $('#name').val(),budget: $('#budget').val(), market_base: $('#market_base').val(),tv:tv, seo:seo,adwords:adwords,facebook:facebook},
            success:function(data){
                if (data.success == true){
                    $('#objective').val(data.objective);
                    for(i=0; i<data.variables.length; i++) {
                        if(data.variables[i][0] === 'X1')
                            $('#TV1').val(data.variables[i][1]);
                        else if(data.variables[i][0] === 'X2')
                            $('#SEO1').val(data.variables[i][1]);
                        else if(data.variables[i][0] === 'X3')
                            $('#Adwords1').val(data.variables[i][1]);
                        else if(data.variables[i][0] === 'X4')
                            $('#Facebook1').val(data.variables[i][1]);
                    }
                }
            },
            error: function(){

            }
        })
    });

    $('.add-destination').on('click', function (e) {
        addDestination($(this));
    });

    $('.add-item').on('click', function (e) {
        addItem($(this));

    });

  /*  $('.back-button').on('click',function(e){
        e.stopPropagation();
        $('.ad-container').removeClass('hidden');
        $('.ed-container').addClass('hidden');
    });*/
    $('.back-show').on('click',function(e){
        e.stopPropagation();
        $('#showTable').removeClass('hidden');
        $('#showItem').addClass('hidden');
    });

    $('.view-combinations').on('click',function(e){
        e.stopPropagation();

        var list = $('ul.nav-tabs');
        list.children('li').each(function() {
            if($(this).attr('data-id') == 4) {
                $(this).removeClass('hidden');
                $(this).find('a').trigger('click');
            }
        });
        getTrucks();
        viewCombinations($(this));

    });

    $('.save-destinations').on('click',function(e){
        e.stopPropagation();

        saveDelivery($(this));

    });

    $('.save-campaign').on('click',function(e){
        e.stopPropagation();
     //   var choice = $('input[name=tmpCombination]:checked').val();
     //   $("input[name=choiceCombination][value="+choice+"]").prop("checked",true);
        saveCampaign($(this));

    });

    $('.editDestination').on('click',function(e){
        e.stopPropagation();
        editDestinations($(this).parent('div'));
    });

    if ($('.destinations-container').length > 0){
        destinationsList = $('ol.sortable.sortable-destinations').nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div.destination-container',
            helper: 'clone',
            items: 'li',
            opacity:.6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 1,
            isTree: false,
            expandOnHover: 700,
            startCollapsed: false
        });

        $('.disclose').on('click', function(e){
            e.stopPropagation();
            var obj = $(this);
            toggleDestination(obj);
        });

    }

    if ($('.combinations-container').length > 0){
        combinationsList = $('ol.sortable.sortable-combinations').nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div.combinations-container',
            helper: 'clone',
            items: 'li',
            opacity:.6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 1,
            isTree: false,
            expandOnHover: 700,
            startCollapsed: false
        });

        $('.disclose').on('click', function(e){
            e.stopPropagation();
            var obj = $(this);
            toggleDestination(obj);
        });

    }

    if ($('.combinations-container2').length > 0){
        combinationsList2 = $('ol.sortable.sortable-combinations2').nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div.combinations-container2',
            helper: 'clone',
            items: 'li',
            opacity:.6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 1,
            isTree: false,
            expandOnHover: 700,
            startCollapsed: false
        });


    }
    if ($('.items-container').length > 0){
        itemsList = $('ol.sortable.sortable-items').nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div.item-container',
            helper: 'clone',
            items: 'li',
            opacity:.6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 1,
            isTree: false,
            expandOnHover: 700,
            startCollapsed: false
        });

        $('.disclose').on('click', function(e){
            e.stopPropagation();
            var obj = $(this);
            toggleItem(obj);
        });

        $('.item-type').on('change', function (e) {
            e.stopPropagation();
       //     changeItemType($(this));
        });

        $('.item-type').on('click', function(e){
            e.stopPropagation();
        });
        $('.item-input').on('click', function(e){
            e.stopPropagation();
        });


         $('.save-item').on('click', function(e){

             e.stopPropagation();
             var list = $('ul.nav-tabs');
             list.children('li').each(function() {
                 if($(this).attr('data-id') == 3) {
                     $(this).removeClass('hidden');
                     $(this).find('a').trigger('click');
                 }
             });
    });
    }

    getItems();
});


function campaignActionFormatter(value, row, index){
    return row.formatter;

}


function vDeleteCampaign(object){
    var uid = object.attr('data-id');
    var code = object.attr('data-code');
    var res = confirm("Είστε σίγουρος ότι θέλετε να διαγράψετε την καμπάνια: "+code+" ;");
    if (res){
        $.ajax({
            url: ajaxUrl+"/delete",
            type: "POST",
            dataType: "JSON",
            data: {delivery_id: uid},
            success:function(data){
                if (data.success == true){
                    $('#tbl-campaigns').bootstrapTable('removeByUniqueId', uid);
                }
            },
            error: function(){

            }
        })
    }
}

function addDestination(obj){

    var isCreate = true;
    if ($('#delivery_id').length != 0 && $('#delivery_id').val() != -1){
        isCreate = false;
    }

    var disabled = 'disabled="disabled"';
    var hidden = 'hidden';

    var listDom = $('ol.sortable.sortable-destinations');

    if(destinationsList.children().length==3){
        alert("Δεν μπορείτε να προσθέσετε προορισμό. Ο μέγιστος αριθμός προορισμών είναι 3.")
        return;
    }


    var rand = Math.floor(Math.random()*200)-200;
    while ($('.destination-container[data-id="'+rand+'"]').length > 0){
        rand = Math.floor(Math.random()*200)-200;
    }

    var html = '<li>';
    html +=         '<div class="destination-container" data-id="'+rand+'">';
    html +=             '<span class="disclose sortable-icon glyphicon glyphicon-triangle-top" data-toggle="tooltip" title="Expand to edit destination." data-placement="top"></span>';
    html +=             '<span class="destination-text">Προορισμός</span>';
    html +=             '<span class="remove-destination glyphicon glyphicon-remove" data-toggle="tooltip" title="Διαγραφή προορισμού" data-placement="top"></span>';
    html +=             '<span class="process-destination glyphicon glyphicon-menu-right hidden" data-toggle="tooltip" title="Click to see destination." data-placement="top"></span>';
    html +=             '<div class="destination-input-container">';
    html +=                 '<div class="input-group col-sm-6 col-md-6 ">' +
        '<input class="form-control1 road-input" type="text" name="road_text[]" value="Οδός αριθμός"></div>';
    html +=                 '<div class="input-group  col-sm-6 col-md-6 ">' +
        '<input class="form-control1 city-input" type="text" name="city_text[]" value="Πόλη"></div>';
    html +=                 '<div class="input-group  col-sm-6 col-md-6 ">' +
        '<input class="form-control1 postalcode-input" type="text" name="postal_text[]" value="Ταχυδρομικός κώδικας"></div>';
    html +=                 '<input type="hidden" class="encodedanswers" />';
    html +=                 '<input type="hidden" class="latitude" />';
    html +=                 '<input type="hidden" class="longitude" />';
    html +=                 '<input type="hidden" class="distance" />';
    html +=                 '<input type="hidden" class="duration" />';
    html +=                 '<input type="hidden" class="distanceText" />';
    html +=                 '<input type="hidden" class="durationText" />';
    html +=                 '<div class="input-group  col-sm-6 col-md-6 ">' +
        '<button class="btn btn-success checkAddress" type="button">Έλεγχος Διεύθυνσης</button>'+
        '<button class="btn btn-success editDestination disabled" type="button">Προσθήκη Ειδών</button></div>';
    html +=         '</div>';
    html += '</li>';

  //  listDom.prepend(html);
    listDom.append(html);

    destinationsList.nestedSortable('refresh');
    $('.view-combinations-container').removeClass('hidden');
    $('[data-toggle="tooltip"]').tooltip();
//    loadSelection(obj);
    $('.disclose').off('click');
    $('.disclose').on('click',function(e){
        e.stopPropagation();
        toggleDestination($(this));
    });

    $('.remove-destination').off('click');
    $('.remove-destination').on('click', function(e){
        e.stopPropagation();
        removeDestination($(this));
    });

    $('.editDestination').on('click',function(e){
        e.stopPropagation();
        editDestinations($(this).parent('div'));
    });


    $('.checkAddress').on('click', function(){
        var obj= $(this).parent('div').parent('div').parent('div');
        var geocoder = new google.maps.Geocoder();
      /*  var address_value = "Κυπαρισσίων 11"
        var city_value = "Αγία Παρασκευή";
        var postal_value = "15343";*/
        var address_value = obj.find($('.road-input')).val();
        var city_value = obj.find($('.city-input')).val();
        var postal_value = obj.find($('.postalcode-input')).val();

     /*   if ($.trim(address_value).length == 0 || $.trim(city_value).length == 0 || $.trim(postal_value) == 0){
            $('#find_coords').tooltip('show');
            setTimeout(function(){
                $('#find_coords').tooltip('hide');
            }, 2000)
            return;
        }*/

        var address = address_value +' '+ city_value +" "+postal_value +" ΕΛΛΑΔΑ";
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                deliveryAddress = results[0];
                obj.find($('.latitude')).val(results[0].geometry.location.lat());
                obj.find($('.longitude')).val(results[0].geometry.location.lng());
                saveDestinationText(results[0].formatted_address, obj.attr('data-id'));
                computeDistance($(obj));
                $('.editDestination').removeClass('disabled');
            }else {
                alert("Η διεύθυνση δεν είναι έγκυρη. Προσπαθήστε ξανά.");
                return;
            }
        });
    });

    $('.sw-container').height($('#destinations').outerHeight(true));

}

function computeDistance(obj){

    var destination;
    if(deliveryAddress) {
        destination = deliveryAddress.formatted_address;
    }else{
        /*    $('.checkAddress').trigger('click');
         if(!deliveryAddress){
         return;
         }*/
    }

    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
        {
            origins: [origin],
            destinations: [destination],
            travelMode: 'DRIVING',
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
        }, function(results, status) {
            if (status == google.maps.DistanceMatrixStatus.OK) {
                deliveryDistance =  results.rows[0].elements[0].distance.value;
                obj.find($('.distance')).val(deliveryDistance);
                deliveryDuration = results.rows[0].elements[0].duration.value;
                obj.find($('.duration')).val(deliveryDuration);
                console.log("OK");
            }else {
                console.log("Failed");
            }
        });

}

function addItem(obj){

    var isCreate = true;
    if ($('#campaign_id').length != 0 && $('#campaign_id').val() != -1){
        isCreate = false;
    }

    var listDom = $('ol.sortable.sortable-items');
    var rand = Math.floor(Math.random()*200)-200;
    while ($('.item-container[data-id="'+rand+'"]').length > 0){
        rand = Math.floor(Math.random()*200)-200;
    }
    var html = '<li id="'+rand+'">';
    html +=         '<div class="item-container" data-id="'+rand+'">';
    html +=             '<span class="disclose sortable-icon glyphicon glyphicon-triangle-top" data-toggle="tooltip" title="Expand to edit item." data-placement="top"></span>';
    html +=             '<span class="item-text">Είδος</span>';
    html +=             '<span class="remove-item glyphicon glyphicon-remove" data-toggle="tooltip" title="Διαγραφή είδους" data-placement="top"></span>';
    html +=             '<span class="process-item glyphicon glyphicon-menu-right hidden" data-toggle="tooltip" title="Click to see item." data-placement="top"></span>';
    html +=             '<div class="item-input-container">';
      html +=               '<label for="item-type" class="control-label">Είδος: </label>' +
     '<select id="item-type" name="item-type[]" class="form-control1 item-type">' +
     '<option value="-1" selected="">Επιλέξτε είδος</option>';
            for(i=0; i<availableItems.length;i++) {
                html += '<option value="'+ availableItems[i]['id'] +'">'+availableItems[i]['type']+'</option>';
            }
    html +=    '</select>' ;/*+
        '<label for="noItems" class="control-label">Ποσότητα: </label>' +
        '<input class="noItems form-control1" type="number" name="noItems" value="0"></div>';*/
    html +=         '</div>';
    html += '</li>';

    listDom.prepend(html);

    itemsList.nestedSortable('refresh');
    $('[data-toggle="tooltip"]').tooltip();

    $('.save-item-container').removeClass('hidden');
//    loadSelection(obj);
    $('.disclose').off('click');
    $('.disclose').on('click',function(e){
        e.stopPropagation();
        toggleItem($(this));
    });

    $('.item-type').off('change');
    $('.item-type').on('change', function (e) {
        e.stopPropagation();
        saveItemText( $('.item-type :selected').html(),rand);
    });

    $('.item-type').off('click');
    $('.item-type').on('click', function(e){
        e.stopPropagation();
    });
    $('.item-input').off('click');
    $('.item-input').on('click', function(e){
        e.stopPropagation();
    });
    $('.remove-item').off('click');
    $('.remove-item').on('click', function(e){
        e.stopPropagation();
        removeItem($(this));
    });

    $('.save-item').on('click',function(e){
        e.stopPropagation();

        var list = $('ul.nav-tabs');
        list.children('li').each(function() {
            if($(this).attr('data-id') == 3) {
                $(this).removeClass('hidden');
                $(this).find('a').trigger('click');
            }
        });
      //  getTrucks();
      //  viewCombinations($(this));
    });
    $('.sw-container').height($('#destinations').outerHeight(true));

}


function toggleDestination(obj){
    var sib = obj.siblings('div');
    if (sib.hasClass('hidden')){
        sib.removeClass('hidden');
        obj.removeClass('glyphicon-triangle-bottom');
        obj.addClass('glyphicon-triangle-top');
    }else{
        sib.addClass('hidden');
        obj.removeClass('glyphicon-triangle-top');
        obj.addClass('glyphicon-triangle-bottom');
    }
    $('.sw-container').height($('#destinations').outerHeight(true));

}

function toggleItem(obj){
    var sib = obj.siblings('div');
    if (sib.hasClass('hidden')){
        sib.removeClass('hidden');
        obj.removeClass('glyphicon-triangle-bottom');
        obj.addClass('glyphicon-triangle-top');
    }else{
        sib.addClass('hidden');
        obj.removeClass('glyphicon-triangle-top');
        obj.addClass('glyphicon-triangle-bottom');
    }
    $('.sw-container').height($('#destinations').outerHeight(true));

}

function saveDestinationText(newValue, id){

    var dList = $('ol.sortable-destinations');
    var target;
    var oldValue;
    dList.children('li').each(function(){
        if($(this).find($('.destination-container')).attr('data-id') == id) {
            target = $(this).find('.destination-text');
            oldValue = target.html();
            target.html(newValue);
            return;
        }
    });

}

function saveItemText(newValue, id){
    var dList = $('ol.sortable-items');
    var target;
    var oldValue;
    dList.children('li').each(function(){
        if($(this).find($('.item-container')).attr('data-id') == id) {
            target = $(this).find('.item-text');
            oldValue = target.html();
            target.html(newValue);
        }
    });

}


function removeDestination(obj){

    var dId = obj.parent('div').attr('data-id');
    var parentLi = obj.parent('div').parent('li');

    if (dId < 0){
        parentLi.remove();
        if(destinationsList.children().length==0)
            $('.view-combinations-container').addClass('hidden');
        destinationsList.nestedSortable('refresh');
        $('.sw-container').height($('#destinations').outerHeight(true));
        return;
    }else {
        $.ajax({
            url:ajaxUrl+"/deleteDestination",
            dataType:'JSON',
            type: 'POST',
            data: {did: dId},
            success: function(data){
                if (data.success){
                    parentLi.remove();
                    if(destinationsList.children().length==0)
                        $('.view-combinations-container').addClass('hidden');
                    destinationsList.nestedSortable('refresh');
                    $('.sw-container').height($('#destinations').outerHeight(true));
                }
            },
            error: function(){
                alert('There was something wrong with the request. Please try again.');
            }
        });
    }
}

function removeItem(obj){


    var parentLi = obj.parent('div').parent('li');


        parentLi.remove();
   /*     if(itemsList.children().length==0)
            $('.view-combinations-container').addClass('hidden');*/
        itemsList.nestedSortable('refresh');
     //   $('.sw-container').height($('#destinations').outerHeight(true));
        return;
}

function saveDestination(obj){

    var sortable =obj.parent('div').siblings('div').find($('ol'));

    var list = $('ol.sortable-items');
    var ton = new Array();
    i=0;
    list.children('li').each(function(){
        ton[i++] = {'type':$(this).find($('.item-type')).val(),'noItems':$(this).find($('.noItems')).val()};
    });
  //  console.log(JSON.stringify(ton));

    var dList = $('ol.sortable-destinations');
    dList.children('li').each(function(){
        if($(this).find($('.destination-container')).attr('data-id') === $('#destination_id').val()) {
            $(this).find($('.encodedanswers')).val(JSON.stringify(ton));
        }
    });
 /*   $('.ad-container').removeClass('hidden');
    $('.ed-container').addClass('hidden');
*/
}

/*function getTrucks(){
  //  uid = 1;
    $.ajax({
        url: ajaxUrl + "/getTrucks",
        type: "POST",
        dataType: "JSON",
    //    data: {user_id: uid},
        success: function (data) {
            if (data.success == true) {
                truckList = data.trucks;
            }
        },
        error: function () {

        }
    })


}*/

function viewCombinations(obj) {

    var list = $('ol.sortable-destinations');
    if (list.children('li').length == 0) {
        return;
    }

    var t = new Array();
    list.children('li').each(function (e) {
        var m = {};
        m.id = $(this).find('.destination-container').attr('data-id');
        m.name = $(this).find('.destination-text').html();
        m.road = $(this).find('.road-input').val();
        m.city = $(this).find('.city-input').val();
        m.postal_code = $(this).find('.postalcode-input').val();
        m.items = $(this).find($('.encodedanswers')).val();
        m.lat = $(this).find($('.latitude')).val();
        m.lng = $(this).find($('.longitude')).val();
        m.distance = $(this).find($('.distance')).val();
        m.duration = $(this).find($('.duration')).val();
        t.push(m);
    });


    if ($('#delivery_id').val() > 0) {
        delivery.action = 'edit';
        delivery.id = $('#delivery_id').val();
        delivery.description = $('#description').val();
        delivery.deliveryDate = $('#deliveryDate').val();
    }
    else {
        delivery.action = 'create';
        delivery.id = $('#delivery_id').val();
        delivery.description = $('#description').val();
        delivery.deliveryDate = $('#deliveryDate').val();
    }

    delivery.destinations = t;
    combinedDestinations = [];
    distances = [];
    durations = [];
    for (i = 0; i < delivery.destinations.length; i++) {
        dest = delivery.destinations[i]['name'];
        parts = dest.split(", ");
        name = parts[0];
        combinedDestinations.push(name);
        distances.push(delivery.destinations[i]['distance']);
        dur = parseInt(delivery.destinations[i]['duration']) + parseInt(installationTime);
        durations.push(dur);
        comb = delivery.destinations[i]['name'];
        for (j = i + 1; j < delivery.destinations.length; j++) {
            var dist = computeCombinationDistance(delivery.destinations[i]['name'], delivery.destinations[j]['name'], delivery.destinations[i]['distance'], dur);
            comb += ", " + delivery.destinations[j]['name'];
            if (j != i + 1) {
                var dist = computeCombinedCombinations(comb);

            }
        }
        if (jQuery.inArray(comb, combinedDestinations) == -1) {
            //     var dist = computeCombinationDistance (delivery.destinations[i]['name'],delivery.destinations[j]['name'], delivery.destinations[i]['distance']);
            //   distances.push(0);
        }
    }
    //  console.log(combinedDestinations);
    //   console.log(distances);

    $.ajax({
        url: ajaxUrl + "/checkTrucks",
        dataType: "JSON",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({data: delivery}),
        success: function (data) {
            if (data.success) {
                var listDom = $('ol.sortable.sortable-combinations');
                listDom.html("");
                var html = '';
                console.log(durations);
                console.log(combinedDestinations);
                var iComb = 0;
                var arrays = new Array();
                for (var i = 0; i < data.data.length; i++) {
                    var array = {};

             //       html += '<input type="radio" name="choiceCombination" class="col-sm-2 col-md-2 col-lg-2" value="'+iComb+'"> <b>Συνδυασμός '+ (iComb+1) +' </b></input>';
                    var iLegs=0;
                    combination_cost = 0;
                    var legs = new Array();
                    for (var index = 0; index < truckList.length; index++) {
                        if (data.data[i][truckList[index]['name']] != 0) {
                            var leg = {};
                            iDistance = jQuery.inArray(data.data[i][truckList[index]['name']], combinedDestinations);

                            if (iDistance == -1) {
                                alert("Δημιουργήθηκε κάποιο λάθος. Προσπασθήστε ξανά.");
                                continue;
                            } else {
                                deliveryDistance = distances[iDistance];
                                deliveryDistanceText = editDistanceText(deliveryDistance);
                                deliveryDuration = durations[iDistance];
                                if(deliveryDuration>maxTime && truckList[index]['stay_cost']==0){
                                    continue;
                                }
                                deliveryDurationText = editDurationText(deliveryDuration);
                                if(iLegs==0){
                                    html += '<li>';
                                    var rand = Math.floor(Math.random() * 200) - 200;
                                    while ($('.combination-container[data-id="' + rand + '"]').length > 0) {
                                        rand = Math.floor(Math.random() * 200) - 200;
                                    }
                                    html += '<div class="truck-container" data-id="' + rand + '">';
                                    html += '<fieldset>' +
                                        '<legend><input type="radio" name="choiceCombination"  value="'+iComb+'">' +
                                        ' <small> Συνδυασμός '+ (iComb+1) +' </small></input></legend>'+
                                        '</fieldset>';
                                    array.combination = 'Συνδυασμός '+ (iComb+1);
                                    array.choice = iComb;
                                }

                            }

                            cost = (truckList[index]['klm_cost'] * deliveryDistance) / 1000;
                            cost = (Math.round(cost * 100) / 100).toFixed(2);
                            staff_cost = truckList[index]['staff_cost'];
                            stay_cost = 0;
                            html += '<div class="leg">';
                            if(iLegs!=0){
                                html += '<fieldset><legend/></fieldset>';
                            }
                            html += '<div class="truck-input-container ">';
                            html += '<label for="destin" class="destin control-label"> Προορισμός: ' + data.data[i][truckList[index]['name']] + '</label>' +
                                '               </div>';
                            html += '<div class="truck-input-container">';
                            html += '<label for="truck" class="truck control-label">Φορτηγό: ' + truckList[index]['name'] + '</label>' +
                                '               </div>';
                            html += '<div class="truck-input-container">';
                            html += '<label for="duration" class="duration control-label">Χρόνος: ' + deliveryDurationText + '</label>' +
                                '</div>';
                            html += '<div class="truck-input-container">';
                            html += '<label for="distance" class="distance control-label">Απόσταση: ' + deliveryDistanceText + '</label>' +
                                '</div>';
                            html += '<div class="truck-input-container">';
                            html += '<label for="cost" class="cost control-label">Κόστος: ' + cost + ' ευρώ</label>' +
                                '</div>';
                            html += '<div class="truck-input-container">';
                            html += '<label for="staff" class="staff control-label">Κόστος προσωπικού: ' + staff_cost + ' ευρώ</label>' +
                                '</div>';
                            if (deliveryDistance > 250000) {
                                if (deliveryDuration > maxTime) {
                                    stays = parseInt(deliveryDuration) / 28800
                                    stay_cost = parseInt(stays) * parseFloat(truckList[index]['stay_cost']);
                                    html += '<div class="truck-input-container">';
                                    html += '<label for="stay" class="stay control-label">Κόστος διανυκτέρευσης: ' + stay_cost + ' ευρώ</label>' +
                                        '</div>';

                                }
                            }
                            total_cost = parseFloat(cost) + parseFloat(staff_cost) + parseFloat(stay_cost);
                            total_cost = (Math.round(total_cost * 100) / 100).toFixed(2);
                            html += '<div class="truck-input-container">';
                            html += '<label for="total_cost" class="total_cost control-label">Συνολικό κόστος: ' + total_cost + ' ευρώ</label>' +
                                '</div>';
                            html += '</div>';
                            leg.destinations = data.data[i][truckList[index]['name']];
                            leg.truck = truckList[index]['name'];
                            leg.duration = 'Χρόνος: ' + deliveryDurationText;
                            leg.distance = 'Απόσταση: ' + deliveryDistanceText;
                            leg.cost = 'Κόστος: ' + cost + ' ευρώ';
                            leg.staff_cost = 'Κόστος προσωπικού: ' + staff_cost + ' ευρώ';
                            if(stay_cost>0){
                                leg.stay_cost = 'Κόστος διανυκτέρευσης: ' + stay_cost + ' ευρώ';
                            }
                            leg.total_cost = 'Συνολικό κόστος: ' + total_cost + ' ευρώ';
                            legs.push(leg);
                            iLegs++;
                            combination_cost += parseFloat(total_cost);
                        }
                    }

                    combination_cost = (Math.round(combination_cost * 100) / 100).toFixed(2);
                    if(combination_cost!=0) {
                        html += '<fieldset><legend></legend></fieldset>';
                        html += '<fieldset>' +
                            '<legend>Συνολικό κόστος: ' + combination_cost + '</legend>' +
                            '</fieldset>';
                        html += '</div>';
                        html += '</li>';
                        array.legs = legs;
                        array.total_cost = combination_cost;
                        arrays.push(array);
                        iComb++;
                    }

                }

                listDom.prepend(html);
                combinationsList.nestedSortable('refresh');
                viewArray(arrays);
     //           alert('bin packaging - OK');
            }
        },
        error: function () {
      //      console.log(distances);
      //     console.log(combinedDestinations);
            alert('Error in bin packing');
        }
    });



}

function viewArray(array) {

    var list = $('ul.nav-tabs');
    list.children('li').each(function() {
        if($(this).attr('data-id') == 4) {
            $(this).removeClass('hidden');
            $(this).find('a').trigger('click');
        }
    });


    $.ajax({
        locale: 'en-US',
        sidePagination: 'server',
        pagination: true,
        pageSize: 50,
        pageList: [50, 100],
        url: ajaxUrl+"/getCombinations",
        dataType: "JSON",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({data:array}),
   /*     data: {group_id: $('#group-value').val(), fromDate:$('#datefrom').val(),toDate:$('#dateto').val()},
        dataType: "JSON",
        type: "POST",*/
        success: function(data){
            $('#tbl-combinations').bootstrapTable('load',data);

        }

    });

 /*   array = {total:1,rows:[{"combination":"Συνδυασμός 1", "destinations":"lalal", "truck":"Neo","duration":"1 hour","distance":"1klm","stay":"2","total_cost":"10 euros"}]};
array = JSON.stringify(array);
    alert(array);
    $('#tbl-combinations').bootstrapTable({
        locale: engineLanguage,
        sidePagination: 'server',
        pagination: true,
        pageSize: 50,
        pageList: [50, 100],
 /*       url: ajaxUrl +"/getCombinations",
        dataType: "JSON",
        type: "POST",
        contentType: "application/json; charset=utf-8",  */
    //    data: JSON.stringify(array),
 /*       data: array,
        search: true,
        showFooter: false,
        uniqueId: 'id'
    });
    $('#tbl-combinations').on('post-body.bs.table', function (e) {
        $('[data-toggle="tooltip"]').tooltip();
        /*    $('.delete-delivery').on('click', function(e){
         e.preventDefault();
         vDeleteDelivery($(this));
         });*/
 //   });


}

function saveCampaign(obj){

    var tv = 0;
    var seo = 0;
    var adwords = 0;
    var facebook = 0;

    if($('#TV').length > 0){
        if($('#TV').val()>0){
            tv=$('#TV').val();
        }else{
            tv=0;
        }
    }
    if($('#SEO').length > 0){
        if($('#SEO').val()>0){
            seo=$('#SEO').val();
        }else{
            seo=0;
        }
    }
    if($('#Adwords').length > 0){
        if($('#Adwords').val()>0){
            adwords=$('#Adwords').val();
        }else{
            adwords=0;
        }
    }
    if($('#Facebook').length > 0){
        if($('#Facebook').val()>0){
            facebook=$('#Facebook').val();
        }else{
            facebook=0;
        }
    }
    $.ajax({
        url: ajaxUrl+"/saveCampaign",
        type: "POST",
        dataType: "JSON",
        data: {name: $('#name').val(),description: $('#description').val(), budget: $('#budget').val(), market_base: $('#market_base').val(),tv:tv, seo:seo,adwords:adwords,facebook:facebook},
        success:function(data){
            if (data.success) {
                alert('Η αποστολή αποθηκεύτηκε επιτυχώς!');
           //    location.href = "#settings";
           //     window.location.reload();
            }else{
                if(data.message) {
                    alert(data.message);
             //       window.location.reload();
                } else
             ; //      window.location.reload();
            }
        },
        error: function(){
            alert('SaveDelivery:There was an error with your request.');
        }
    });

}

function editDistanceText(distance) {

    klm = distance / 1000.00;
    meters = distance % 1000;
    text="";
    if(klm>0){
        text =  (Math.round(klm * 100) / 100).toFixed(2) + " χλμ";
    }else {
        text = meters + " μέτρα";
    }
    return text;
}

function editDurationText(duration) {

    hours = duration / 3600;
    mins = duration % 3600;

    mins = mins / 60;
    text="";
    if(hours>1){
        if(mins>59){
            hours =  parseInt(hours) + 1;
            mins = 0;
        }
        text =  parseInt(hours) + " ώρες";
        if(mins>1)
            text +=  " και " + (Math.round(mins * 100) / 100).toFixed(0) + " λεπτά";
    }else if(hours == 1) {
        if(mins>59){
            hours =  parseInt(hours) +1;
            mins = 0;
            text = parseInt(hours) + " ώρες";
        }else {
            text = parseInt(hours) + " ώρα";
            if (mins > 1)
                text += " και " + (Math.round(mins * 100) / 100).toFixed(0) + " λεπτά";
        }
    }else{
        text = (Math.round(mins * 100) / 100).toFixed(0) + " λεπτά";
    }
    return text;
}

function computeCombinationDistance(origin, destination, distance, duration){
    var service = new google.maps.DistanceMatrixService();
    var dist;
    var dur;

        service.getDistanceMatrix(
            {
                origins: [origin],
                destinations: [destination],
                travelMode: 'DRIVING',
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, function (results, status) {
                if (status == google.maps.DistanceMatrixStatus.OK) {
                    dist = results.rows[0].elements[0].distance.value + parseInt(distance);
                    dur = results.rows[0].elements[0].duration.value + parseInt(duration) + parseInt(installationTime);
                    o = origin;
                    d = destination;
                    parts = o.split(",");
                    oName = parts[0];
                    parts = d.split(",");
                    dName = parts[0];
                    dest = oName + ", " + dName;
                    if (jQuery.inArray(dest, combinedDestinations) == -1) {
                        distances.push(dist);
                        durations.push(dur);
                        combinedDestinations.push(dest);
                    }
                    return dist;
                } else
                    return 0;
            });

}

function computeCombinedCombinations(comb){
    var service = new google.maps.DistanceMatrixService();
    var dist = 0;
    var dur = 0;
    var dest = comb;
    var parts = dest.split(' Ελλάδα,');
    var name = "";

    var index = distances.length;
    destination = parts[0] + ", Ελλάδα";
    service.getDistanceMatrix(
        {
            origins: [origin],
            destinations: [destination],
            travelMode: 'DRIVING',
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
        }, function (results, status) {
            if (status == google.maps.DistanceMatrixStatus.OK) {
                dist = results.rows[0].elements[0].distance.value;
                dur = results.rows[0].elements[0].duration.value + parseInt(installationTime);
                if(distances.length>index){
                    distances[index] += dist;
                    durations[index] += dur ;
                }

            }
        });

    for(var i=0; i<parts.length-1; i++){
        origin = parts[i] + ", Ελλάδα";
        destination = parts[i+1] + ", Ελλάδα";
        service.getDistanceMatrix(
            {
                origins: [origin],
                destinations: [destination],
                travelMode: 'DRIVING',
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, function (results, status) {
                if (status == google.maps.DistanceMatrixStatus.OK) {
                    dist = results.rows[0].elements[0].distance.value;
                    dur = results.rows[0].elements[0].duration.value + parseInt(installationTime);
                    if(distances.length>index){
                        distances[index] += dist;
                        durations[index] += dur;
                    }

                }
            });

        var o = origin.split(', ');
        name += o[0].trim() + ", ";
    }
    var d = destination.split(', ');
    name += d[0].trim();


    if (jQuery.inArray(name, combinedDestinations) == -1) {
        distances.push(0);
        durations.push(0);
        combinedDestinations.push(name);
    }

}

function getItems(){

    $.ajax({
        url: ajaxUrl+"/getItems",
        type: "POST",
        dataType: "JSON",
     //   data: {user_id: uid},
        success:function(data){
            if (data.success == true){
                availableItems = data.items;
            }
        },
        error: function(){

        }
    })

}

function editDestinations(obj){

    var hasChanged = true;
 /*   $('.ad-container').addClass('hidden');
    $('.ed-container').removeClass('hidden');*/
    var id = obj.parent('div').parent('div').attr('data-id');
    if(id === $('#destination_id').val()){
        hasChanged = false;
       return;
    }
    $('#destination_id').val(id);

    var dList = $('ol.sortable-destinations');
    dList.children('li').each(function(){
        if($(this).find($('.destination-container')).attr('data-id') === $('#destination_id').val()) {
            if($(this).find($('.encodedanswers')).val()===""){
                var list = $('ol.sortable-items');
                list.children('li').each(function(){
                   $(this).remove();
                });
                obj.parent('div').parent('div').parent('div').find($('.save-item-container').addClass('hidden'));
            }
            else{
                if(hasChanged){
                    var list = $('ol.sortable-items');
                    list.children('li').each(function(){
                        $(this).remove();
                    });

                    var listDom = $('ol.sortable.sortable-items');
                    var value = $(this).find($('.encodedanswers')).val();
                    value = JSON.parse(value);
                    var html ="";
                    for(k=0;k<value.length ;k++) {
                        html += '<li>';
                        html += '<div class="item-container">';
                        html += '<div class="item-input-container">';
                        html += '<label for="item-type" class="control-label">Είδος: </label>' +
                            '<select id="item-type" name="item-type[]" class="form-control1 item-type">' +
                            '<option value="-1">Επιλέξτε είδος</option>';
                        for (i = 0; i < availableItems.length; i++) {
                            if(availableItems[i]['id'] == value[k]['type']) {
                                html += '<option value="' + availableItems[i]['id'] + '" selected>' + availableItems[i]['name'] + '</option>';
                            }
                            else
                                html += '<option value="' + availableItems[i]['id'] + '">' + availableItems[i]['name'] + '</option>';
                        }
                        html += '</select>' +
                            '<label for="noItems" class="control-label">Ποσότητα: </label>' +
                            '<input class="noItems form-control1" type="number" name="noItems" value="'+value[k]['noItems']+'"></div>';
                        html += '</div>';
                        html += '</li>';
                    }

                    $('.save-item-container').removeClass('hidden');

                    listDom.prepend(html);

                    itemsList.nestedSortable('refresh');

                }

            }
        }
    });
    getItems();
}

function deliveredFormatter( row){

    if (row.choice == 1){
        return ['Έχει παραδοθεί'];
    }else {
        return ['Προς παράδοση'];
    }


}

function chooseFormatter(value, row, index){

    return '<input type="radio" name="tmpCombination"  value="'+row.choice+'"/>';

}

function viewDetails(obj){

    var choice = obj.attr('data-id');
    var list = $('ol.sortable-combinations');
    var array = new Array();
    list.children('li').each(function (e) {
        if($(this).find($('input[name=choiceCombination]')).val() == choice ) {

            var ilist = $('ol.sortable-combinations2');
            ilist.html("");
            html = $(this);
            ilist.prepend(html);
            combinationsList2.nestedSortable('refresh');
            $('#showTable').addClass('hidden');
            $('#showItem').removeClass('hidden');

        }
    });
}


