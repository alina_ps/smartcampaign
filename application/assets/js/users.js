/**
 * Created by user on 21/7/2016.
 */
$(document).ready(function (){
    $('[data-toggle="tooltip"]').tooltip();

    if ($('select').length > 0)
        $('select').selectpicker({
            noneSelectedText: noneSelected
        });

    if ($('#tbl-users').length > 0){
        $('#tbl-users').bootstrapTable({
            locale: engineLanguage,
            sidePagination: 'server',
            pagination: true,
            pageSize: 50,
            pageList: [50, 100],
            url: ajaxUrl +"/getUsers",
            search: false,
            showFooter: false,
            uniqueId: 'id'
        });

        $('#tbl-users').on('post-body.bs.table', function (e) {
            $('[data-toggle="tooltip"]').tooltip();
            $('.delete-user').on('click', function(e){
                e.preventDefault();
                vDeleteUser($(this));
            });
        });


    }

    $('#create-user-form').on('submit',function(e){

        if (validateUserForm(false)){
            return true;
        }else {
            return false;
        }
    });
    $('#edit-user-form').on('submit',function(e){
        if (validateUserForm(true)){
            return true;
        }else {
            return false;
        }
    });

});

function userActionFormatter(value, row, index){

    return row.formatter;

}

function vDeleteUser(object){
    var uid = object.attr('data-id');
    var uname = object.attr('data-uname');
    var res = confirm("Are you sure you want to delete user: "+uname+" ?");
    if (res){
        $.ajax({
            url: ajaxUrl+"/delete",
            type: "POST",
            dataType: "JSON",
            data: {user_id: uid},
            success:function(data){
                if (data.success == true){
                    $('#tbl-users').bootstrapTable('removeByUniqueId', uid);
                }
            },
            error: function(){

            }
        })
    }
}

function validateUserForm(edit){
    var username = $('#username').val();
    var password = $('#password').val();
    var role = $('#role').val();
    var flag = true;


    if ($('#username').length > 0){
        if ($.trim(username).length == 0){
            $('#username').parent('div').parent('div').addClass('has-error');
            $('#username').siblings('.help-block').html(emptyFieldText);
            flag = false;
        }
        if ($.trim(username).length > 30){
            $('#username').parent('div').parent('div').addClass('has-error');
            $('#username').siblings('.help-block').html("Field can not exceed 30 characters.");
            flag = false;
        }
        if (flag == true){
            $('#username').parent('div').parent('div').removeClass('has-error');
            $('#username').siblings('.help-block').html("");
        }
    }
    if (edit == false) {
        if ($.trim(password).length == 0) {
            $('#password').parent('div').parent('div').addClass('has-error');
            $('#password').siblings('.help-block').html(emptyFieldText);
            flag = false;
        } else {
            $('#password').parent('div').parent('div').removeClass('has-error');
            $('#password').siblings('.help-block').html("");
        }

    }

    if ($('#role').length > 0){
        if (role == -1){
            $('#role').parent('div').parent('div').parent('div').addClass('has-error');
            $('#role').siblings('.help-block').html(emptyFieldText);
            flag = false;
        }else {
            $('#role').parent('div').parent('div').parent('div').removeClass('has-error');
            $('#role').siblings('.help-block').html("");
        }
    }

    return flag;
}