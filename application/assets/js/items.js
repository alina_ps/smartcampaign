/**
 * Created by user on 22/7/2016.
 */
var selectedValue;
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    if ($('#tbl-items').length > 0){
        $('#tbl-items').bootstrapTable({
            locale: engineLanguage,
            sidePagination: 'server',
            pagination: true,
            pageSize: 50,
            pageList: [50, 100],
            url: ajaxUrl +"/getItems",
            search: true,
            showFooter: false,
            uniqueId: 'id'
        });
        $('#tbl-items').on('post-body.bs.table', function (e) {
            $('[data-toggle="tooltip"]').tooltip();
            $('.delete-item').on('click', function(e){
                e.preventDefault();
                vDeleteItem($(this));
            });
        });
    }

  /*  if ($('select').length > 0)
        $('select').selectpicker({
            noneSelectedText: noneSelected
        });


*/

    if ($('.nav-tabs').length > 0){
        $('.nav-tabs a').click(function(e){
            e.preventDefault();
            $(this).tab('show');
        });
        $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
            var id = $(e.target).attr("href").substr(1);
            window.location.hash = id;
        });
        var hash = window.location.hash;
        $('.nav-tabs a[href="' + hash + '"]').tab('show');
    }


});

function itemActionFormatter(value, row, index){
    return row.formatter;

}

function vDeleteItem(object){

    var uid = object.attr('data-id');
    var uname = object.attr('data-uname');
    var res = confirm("Είστε σίγουρος ότι θέλετε να διαγράψετε το είδος: "+uname+" ;");
    if (res){
        $.ajax({
            url: ajaxUrl+"/delete",
            type: "POST",
            dataType: "JSON",
            data: {item_id: uid},
            success:function(data){
                if (data.success == true){
                    $('#tbl-items').bootstrapTable('removeByUniqueId', uid);
                }
            },
            error: function(){

            }
        })
    }
}