<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14/3/2017
 * Time: 11:20 πμ
 */

use DVDoug\BoxPacker\Box;
use DVDoug\BoxPacker\Item;

class SimpleBox implements Box
{

    public function __construct($reference, $width, $length, $depth)
    {
        $this->reference = $reference;

        $this->outerWidth = $width;
        $this->outerLength = $length;
        $this->outerDepth = $depth;

        $this->innerWidth = $width;
        $this->innerLength = $length;
        $this->innerDepth = $depth;

        $this->emptyWeight = 0;
        $this->maxWeight = 1;

        $this->innerVolume = $this->innerWidth * $this->innerLength * $this->innerDepth;
    }

    public function getReference()
    {
        return $this->reference;
    }

    public function getOuterWidth()
    {
        return $this->outerWidth;
    }

    public function getOuterLength()
    {
        return $this->outerLength;
    }

    public function getOuterDepth()
    {
        return $this->outerDepth;
    }

    public function getEmptyWeight()
    {
        return $this->emptyWeight;
    }

    public function getInnerWidth()
    {
        return $this->innerWidth;
    }

    public function getInnerLength()
    {
        return $this->innerLength;
    }

    public function getInnerDepth()
    {
        return $this->innerDepth;
    }

    public function getInnerVolume()
    {
        return $this->innerVolume;
    }

    public function getMaxWeight()
    {
        return $this->maxWeight;
    }
}

class SimpleItem implements Item
{

    public function __construct($description, $width, $length, $depth)
    {
        $this->description = $description;
        $this->width = $width;
        $this->length = $length;
        $this->depth = $depth;
        $this->weight = 0;
        $this->keepFlat = true;

        $this->volume = $this->width * $this->length * $this->depth;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getLength()
    {
        return $this->length;
    }

    public function getDepth()
    {
        return $this->depth;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getVolume()
    {
        return $this->volume;
    }

    public function getKeepFlat()
    {
        return $this->keepFlat;
    }
}
