<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14/3/2017
 * Time: 11:19 πμ
 */

require_once 'vendor\autoload.php';
require_once 'SimpleBox.php';

use DVDoug\BoxPacker\Packer;
use DVDoug\BoxPacker\ItemList;
use DVDoug\BoxPacker\VolumePacker;

function check_volumes($groups, $trucks) {
    $found = array();
 /*   $perm = compute_trucks($groups, $trucks);
    print_r($perm);*/
    $perm = get_combinations($trucks,$groups);

//    print_r($perm);

    foreach($perm as $selection) {
        if(!empty($selection)) {
            $verdict = true;

            //Create the trucks
            for ($i = 0; $i < count($trucks); $i++) {
                $box = new SimpleBox($trucks[$i][0],
                    $trucks[$i][1][0], $trucks[$i][1][1], $trucks[$i][1][2]);

                //Build the item list
                $items = new ItemList();
                //    $cgroup = $selection[$i];
                $cgroup = $selection[$trucks[$i][0]];
                //    $cgroup = $i;
                $total_items = 0;

                if (!empty($cgroup)) {
                    $parts = explode(",", $cgroup);
                    $id = 0;
                    foreach ($parts as $part) {
                        //The group ID
                        //   $id = intval(substr($part, 5)) - 1;

                        $sgroup = $groups[$id];
                        //   $gname = $sgroup[0];
                        $gname = $part;
                        /*       $id = $i;
                                  $i++;
                                   $sgroup = $groups[$id];
                                  $gname = $sgroup[0];*/

                        for ($j = 1; $j < count($sgroup); $j++) {
                            $items->insert(new SimpleItem($gname,
                                $sgroup[$j][0], $sgroup[$j][1], $sgroup[$j][2]));
                            $total_items++;
                        }
                        $id++;
                    }

                    $volumePacker = new VolumePacker($box, $items);
                    $packedBox = $volumePacker->pack();

                    if ($total_items != count($packedBox->getItems())) {
                        $verdict = false;
                    }
                }

                //     echo $trucks[$i][0] ." [". $cgroup ."]: ". $total_items ."\n";
            }

            if ($verdict == true) {
                $found[] = $selection;
            }
        }

    }

    return $found;
}

function combineCombinations($array, $groups, $trucks){

    $combinations = array();

//    print_r($array);
 //   print_r($groups);
 //   print_r($trucks);

    foreach($array as $comb){
        $destinations = $groups;
        $tmpTrucks = $trucks;

        foreach($comb as $k=>$truck) {
            foreach ($groups as $key=>$group) {
                if (strpos($truck,$group[0])!==false) {
                   // print_r($destinations[$key]);
                //    echo 'truck ' . $k ;
                    unset($destinations[$key]);
                    foreach($trucks as $trKey=>$tr){
                        if ($tr['name'] === $k) {
                            echo $trucks[$trKey];
                            unset($trucks[$trKey]);

                        }
                    }
                }
            }
        }
   //     print_r($comb);
   //     print_r($destinations);
        if(empty($destinations)){
            array_push($combinations,$comb);
        }else{
            print_r($destinations);
            print_r($tmpTrucks);

        /*    $combs = check_volumes($destinations, $tmpTrucks);
            foreach ($combs as $c){
                $c = array_merge($c, $comb);
                array_push($combinations,$c);
            }*/
        }
    }
//    print_r($combinations);
    return $combinations;
}

function filterCombinations($combinations,$smallTrucks,$farDestinations){

    foreach ($combinations as $kComb=>$comb){ //exclude small trucks from destinations > 250km
        foreach($comb as $key=>$truck){
            foreach ($smallTrucks as $tr){
                if(strval($key) === strval($tr[0])){
                    foreach($farDestinations as $dest){
                        if(strpos($truck,$dest[0])!==false){
                        //    print_r($combinations[$kComb]);
                            unset($combinations[$kComb]);
                        }
                    }
                }
            }
        }
    }
   // print_r($combinations);
    return $combinations;
}

function compute_trucks($groups, $trucks) {
    //Lets examine the possible combinations in the trucks
    $firstpass = array();
    $realgroups = array();
    $dummygroups = array();
    foreach($groups as $group) {
        $realgroups[] = $group[0];
        $dummygroups[] = "group0";
    };

    $firstpass = $realgroups;
    $skipfirst = true;
    foreach($trucks as $truck) {
        if ( $skipfirst ) {
            $skipfirst = false;
            continue;
        }

        $firstpass = array_merge($firstpass, $dummygroups);
    }

    $perms = pc_permute($firstpass);

    //Now combine them all
    $newperms = array();

    $repeats = count($trucks);
    $elements = count($groups);

    foreach($perms as $selection) {
        $loadpart = array();
        for($i=0; $i<$repeats; $i++) {
            $start_index = $i*$elements;

            $load = array();
            for($j=0; $j<$elements; $j++,$start_index++) {
                if ( $selection[$start_index]=="group0" )
                    continue;
                $load[] = $selection[$start_index];
            }
            sort($load);
            $newitem = implode(",", $load);

            $loadpart[] = $newitem;
        }

        /*
        print_r($selection);
        echo "-----------\n";
        print_r($loadpart);
        sleep(1);
        */

        if ( in_arr($loadpart, $newperms)===FALSE )
            $newperms[] = $loadpart;
    }

    return $newperms;
}

function pc_permute($items, $perms = array( )) {
    if (empty($items)) {
        $return = array($perms);
    }  else {
        $return = array();
        for ($i = count($items) - 1; $i >= 0; --$i) {
            $newitems = $items;
            $newperms = $perms;
            list($foo) = array_splice($newitems, $i, 1);
            array_unshift($newperms, $foo);
            $return = array_merge($return, pc_permute($newitems, $newperms));
        }
    }
    return $return;
}
/*
function powerSet($in,$trucks,$minLength = 1) {
    $count = count($in);
    $members = pow(2,$count);
    $return = array();
    foreach ($trucks as $truck) {
        for ($i = 0; $i < $members; $i++) {
            $b = sprintf("%0" . $count . "b", $i);
            $out = array();
            $out[] = $truck;
            for ($j = 0; $j < $count; $j++) {
                if ($b{$j} == '1') {
          //          $out[$truck] = $in[$j];
                    $out[] = $in[$j];
                }
            }
            if (count($out) > $minLength) {
                $return[] = $out;
            }
        }
    }
    return $return;
}
*/
function combinations($arrays, $i = 0) {
    if (!isset($arrays[$i])) {
        return array();
    }
    if ($i == count($arrays) - 1) {
        return $arrays[$i];
    }

    // get combinations from subsequent arrays
    $tmp = combinations($arrays, $i + 1);

    $result = array();

    // concat each array from tmp with each element from $arrays[$i]
    foreach ($arrays[$i] as $v) {
        foreach ($tmp as $t) {
            $result[] = is_array($t) ?
                array_merge(array($v), $t) :
                array($v, $t);
        }
    }

    return $result;
}

function get_combinations($trucks, $destinations){

    $i=0;

    $tDestinations = powerSet($destinations);
  //  print_r($tDestinations);
    $finalCombs = array();
    foreach ($trucks as $truck){
        $j=0;
        foreach ($tDestinations as $destination){
            $arrayItems = array();
            array_push($arrayItems, $truck);
            array_push($arrayItems, $destination);
         //   print_r(combinations($arrayItems));
            /*We get the combinations of destinations for the current truck*/
            $comb1 = combinations($arrayItems);
            foreach($comb1 as $key => $comb){
                if($key!=0) {
                    $comb1[0] = array_merge($comb1[0], $comb);
                    unset($comb1[$key]);
                }
            }
         //   print_r($comb1);
            $tmp = $trucks;
            unset($tmp[$i]);
            $tmpDestinations = $destinations; // list with all the remaining destinations
            $k=0;
            foreach($destinations as $dest) {
                if(in_array($dest, $destination)) {
                    unset($tmpDestinations[$k]);
                }
                $k++;
            }
            $tmpItems = array();
            array_push($tmpItems,$tmp);
            array_push($tmpItems,$tmpDestinations);
            $comb2 = combinations($tmpItems);
            foreach($comb2 as $comb){
                foreach($comb1 as $key => $c) {
                    if(is_array($c)) {
                        if (!in_array($comb[1], $c) && !in_array($comb[0][0], $c)) { //check if it is the current truck or the destination is served by the current truck
                            array_push($c, $comb[0][0]);
                            array_push($c, $comb[1]);
                            array_push($comb1, $c);
                        }
                    }
                }
            }
            foreach($comb1 as $key => $comb){
                $allDestinations = true;
                foreach($destinations as $dest){
                    if(!in_array($dest[0],$comb) ){
                        $allDestinations = false;
                    }
                }
                if(!$allDestinations) //delete all the combinations that do not include all destinations
                    unset($comb1[$key]);
                else{
                    $combination = array();
                    foreach($trucks as $trk){
                     //   array_push($combination, $trk[0]);
                        if(in_array($trk[0], $comb)){
                            $tkey = array_keys($comb, $trk[0]);
                            foreach($tkey as $index) {
                                if(isset($combination[$trk[0]])) {
                                    $dest = $combination[$trk[0]] . ", " . $comb[$index + 1];
                                    $combination[$trk[0]] = $dest;
                                }
                                else
                                    $combination[$trk[0]] = $comb[$index + 1];
                            }
                        }else
                            $combination[$trk[0]] = 0;
                           // array_push ($combination, 0);
                    }
              //      print_r($combination);
                    $comb1[$key] = $combination;
                }
            }
            array_push($finalCombs,$comb1);
       //     print_r($comb1);
         //   echo("----------------------------------");
            $j++;

        }
        $i++;
    }
    foreach($finalCombs as $k =>$comb){
        foreach($comb as $key => $value) {
            unset($finalCombs[$k]);
            array_push($finalCombs, $value);
        }

    }

    $tmp = $finalCombs;
    foreach($finalCombs as $key =>$comb){
      //  unset($finalCombs[$k]);
        foreach($tmp as $k=>$c){
            if($c === $comb && $key != $k) {
                unset($finalCombs[$key]);
                unset($tmp[$key]);

            }
        }
    }
 /*   foreach($finalCombs as $key => $comb){
        if($comb == 0)
            unset($finalCombs[$key]);
    }*/
/*
    foreach($finalCombs as $key => $comb){
        foreach($comb as $k=>$c) {
            $i=0;

           foreach($trucks as $tkey=>$truck) {
               if ($c === $truck[0]) {
                   $finalCombs[$key][$c] = $finalCombs[$key][$k + 1];
                   unset($finalCombs[$key][$k]);
                   unset($finalCombs[$key][$k + 1]);
                   break;
               }
           }

        }
   }*/
 //   print_r($finalCombs);
// echo count($finalCombs);
    return $finalCombs;
}

function powerSet($in,$minLength = 1) {
    $count = count($in);
    $members = pow(2,$count);
    $return = array();
    for ($i = 0; $i < $members; $i++) {
        $b = sprintf("%0".$count."b",$i);
        $out = array();
        for ($j = 0; $j < $count; $j++) {
            if ($b{$j} == '1') $out[] = $in[$j];
        }
        if (count($out) >= $minLength) {
            $return[] = $out;
        }
    }
    return $return;
}


function in_arr($needle, $haystack) {
    $outcome = FALSE;

    foreach($haystack as $elem) {
        $c1 = count($elem);
        $c2 = count($needle);

        if ( $c1==$c2 ) {
            $outcome = TRUE;
            for($i=0; $i<$c1; $i++) {
                if ( $elem[$i]!=$needle[$i] ) {
                    $outcome = FALSE;
                    break;
                }
            }
        }

        if ( $outcome==TRUE )
            break;
    }

    return $outcome;
}
