<?php
/**
 * User: Frantzolakis Manolis
 * Date: 18/7/2016
 * Time: 5:19 μμ
 */

namespace application\controllers;

use Engine;

class User extends \BaseController{


    public function __construct(){
        parent::__construct();
        $this->pushStyle(APPLICATION_STYLES_URL."main.css");
        $this->view->setFooter("footer.php");
    }

    public function login($args){
        if ($this->session->isUserLoggedIn()){
            Engine::redirect(Engine::url(array('controller'=>'index', 'action'=>'index')));
        }

        $logUrl = Engine::url(array('controller'=>'user','action'=>'login'));

        $this->view->assign('loginUrl', $logUrl);

        $this->model = new \application\models\User();

        if (isset($_POST['submit'])){
            $validate = $this->model->validate($_POST, 0);
            if (isset($validate['success']) && $validate['success'] === false){
                $this->view->assign("errormsg", $validate['error']['description']);
                $this->view->assign("username", $_POST['username']);
            }else {
                $this->model->setId($validate['user']->getId());
                $this->session->initialize(
                    array(
                        'user'=>$validate['user'],
                        'last_activity'=>time()
                    )
                );
                if (isset($args['_get_params']) && isset($args['_get_params']['device_id'])){
                    Engine::redirect(Engine::url(array('controller'=>'','action'=>'','device_id'=>$args['_get_params']['device_id'])));
                    exit;
                }
                Engine::redirect(Engine::url(array('controller'=>'index','action'=>'index')));
            }
        }

        $this->view->setHeader("header_loggedout.php");
        $this->viewName = "User/login.php";
    }

    public function index($args){
        if (!$this->session->isUserLoggedIn()){
            Engine::redirect(Engine::url(array('controller'=>'user', 'action'=>'login')));
            die();
        }

        if ($this->session->getUser()->getRole() !== "admin"){
            Engine::redirect(Engine::url(array('controller'=>'index', 'action'=>'index')));
            die();
        }
        $this->removeStyle(APPLICATION_STYLES_URL."main.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."bootstrap_table/bootstrap-table-new.min.css");
        $this->pushStyle(APPLICATION_STYLES_URL."main.css");

        $scripts = array();
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-locale-all.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-key-events.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-mobile.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-toolbar.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-filter.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-filter-control.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-natural-sorting.min.js";
        $this->view->assign("successmsg", $this->session->flushSuccessMessage());
        $this->view->assign("errormsg", $this->session->flushErrorMessage());
        $scripts[] = APPLICATION_SCRIPTS_URL."users.js";

        $this->view->assign("scripts",$scripts);
        $this->view->assign("sessionUser", $this->session->getUser());

        $this->view->setHeader("logged/{$this->session->getUser()->getRole()}/header.php");
        $this->view->setFooter("footer.php");
        $this->viewName = ("User/index.php");

    }

    public function getUsers($args){
        if (!$this->session->isUserLoggedIn() || $this->session->getUser()->getRole() !== "admin"){
            echo json_encode(array('error'=>array('msg'=>\Translator::getTranslation('You are not authorized for this resource.'))));
            die();
        }

        $this->model = new \application\models\User();
        $users = $this->model->get();
        if ($users){
            $count = count($users);

            $offset = 0;
            $limit = 50;
            if (!isset($args['_request_params']['sort'])){
                $sort = null;
            }else {
                $sort = $args['_request_params']['sort'] .' '.$args['_request_params']['order'];
            }

            if (isset($args['_request_params']['offset']) && is_numeric($args['_request_params']['offset'])){
                $offset = intval($args['_request_params']['offset']);
            }

            $users = $this->model->get(array('order'=>$sort, 'limit'=>$limit, 'offset'=>$offset));

            $tooltipEdit = \Translator::getTranslation('editTooltip');
            $tooltipDelete = \Translator::getTranslation('deleteTooltip');

            $finalEdit = sprintf($tooltipEdit, \Translator::getTranslation('user_gen'));
            $finalDelete = sprintf($tooltipDelete, \Translator::getTranslation('user_gen'));

            foreach($users as &$user){

                unset($user['password']);
                $user['role'] = \Translator::getTranslation($user['role']);
                $user['formatter'] = '<a data-toggle="tooltip" data-placement="bottom" title="'.$finalEdit.'" class="icon-link" href="'.Engine::url(array('controller'=>'user','action'=>'edit','id'=>$user['id'])).'">'.
                    '<span class="glyphicon glyphicon-edit"></span>'.
                    '</a>'.
                    '<a data-toggle="tooltip" data-placement="bottom" title="'.$finalDelete.'" class="delete-user" href="" data-id="'.$user['id'].'" data-uname="'.$user['username'].'">'.
                    '<span class="glyphicon glyphicon-trash"></span>'.
                    '</a>';
            }

            unset($user);

            echo json_encode(array('total'=>$count, 'rows'=>$users));
            die();
        }else {
            echo json_encode(array('total'=>0,'rows'=>array()));
            die();
        }

    }

    public function edit($args){
        if (!$this->session->isUserLoggedIn() || $this->session->getUser()->getRole() !== "admin"){
            Engine::redirect(Engine::url(array('controller'=>'index', 'action'=>'index')));
            die();
        }


        if (!isset($args["_get_params"]["id"])){
            $error = \Translator::getTranslation("user_not_found");
            $this->session->setErrorMessage($error);
            Engine::redirect(Engine::url(array("controller"=>"user","action"=>"index")));
            die();
        }

        $userId = intval($args["_get_params"]["id"]);
        $this->model = new \application\models\User();
        $user = $this->model->get(array('where'=>array('id'=>$userId)));

        if ($user){

            if (isset($_POST['submit'])){
                $res = $this->processEdit($_POST, $this->session->getUser(), $args);
                Engine::redirect(Engine::url(array('controller'=>'user', 'action'=>'edit', 'id'=>$userId)));
                die();
                //if ($res['success']){
                    //$this->view->assign("successmsg", $res['successmsg']);
                //}else {
                    //$this->view->assign("errormsg", $res['errormsg']);
                //}
                //$user = $this->model->get(array('where'=>array('id'=>$userId)));
            }

            $this->view->assign('user', $user[0]);

        }else {
            $error = \Translator::getTranslation("user_not_found");
            $this->session->setErrorMessage($error);
            Engine::redirect(Engine::url(array("controller"=>"user","action"=>"index")));
            die();
        }

        $this->view->assign("successmsg", $this->session->flushSuccessMessage());
        $this->view->assign("errormsg", $this->session->flushErrorMessage());
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."select/bootstrap-select.min.js";
        $scripts[] = APPLICATION_SCRIPTS_URL."users.js";
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."select/bootstrap-select.min.css");

        $this->view->assign("scripts",$scripts);
        $emptyFieldText = \Translator::getTranslation('empty_field');
        $cannotExceedText = sprintf(\Translator::getTranslation('can_not_exceed'), "30");
        $rawScripts[] = '<script> var noneSelected="'.NONE_SELECTED_TEXT.'"; var emptyFieldText="'.$emptyFieldText.'"; var cannotExceedText = "'.$cannotExceedText.'"</script>';
        $this->view->assign("rawScripts",$rawScripts);
        $this->viewName = ("User/edit.php");
        $this->view->setHeader("logged/{$this->session->getUser()->getRole()}/header.php");
        $this->view->setFooter("footer.php");

    }

    public function create($args){


        if (!$this->session->isUserLoggedIn() || $this->session->getUser()->getRole() !== "admin"){
            Engine::redirect(Engine::url(array('controller'=>'index', 'action'=>'index')));
            die();
        }

        if (isset($_POST['submit'])){

            $res = $this->process($_POST, $this->session->getUser());
            Engine::redirect(Engine::url(array('controller'=>'user', 'action'=>'create')));
            die();
            /*  if ($res['success'] == true){
                $this->view->assign("successmsg", $res['successmsg']);
            }else {
                $this->view->assign("errormsg", $res['errormsg']);
            }*/
        }

        $this->view->assign("successmsg", $this->session->flushSuccessMessage());
        $this->view->assign("errormsg", $this->session->flushErrorMessage());
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."select/bootstrap-select.min.js";
        $scripts[] = APPLICATION_SCRIPTS_URL."users.js";
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."select/bootstrap-select.min.css");

        $this->view->assign("scripts",$scripts);

        $emptyFieldText = \Translator::getTranslation('empty_field');
        $cannotExceedText = sprintf(\Translator::getTranslation('can_not_exceed'), "30");
        $rawScripts[] = '<script> var noneSelected="'.NONE_SELECTED_TEXT.'"; var emptyFieldText="'.$emptyFieldText.'"; var cannotExceedText = "'.$cannotExceedText.'"</script>';
        $this->view->assign("rawScripts",$rawScripts);
        $this->viewName = ("User/create.php");
        $this->view->setHeader("logged/{$this->session->getUser()->getRole()}/header.php");
        $this->view->setFooter("footer.php");

    }

    private function process($postdata, $sessionUser){
        $shouldSave = true;
        $finalError = "";
        $result = array();

        if (!isset($_POST['username']) || strlen(trim($_POST['username'])) == 0 || strlen(trim($_POST['username'])) > 30){
            $shouldSave = false;
            //$error = EngineError::getErrorMessage("202");
            $error = \Translator::getTranslation('error_202');
            $errormsg = sprintf($error, \Translator::getTranslation('Username'));
            $finalError = '<div>'.$errormsg.'</div>';
        }
        if (!isset($_POST['password']) || strlen(trim($_POST['password'])) == 0){
            $shouldSave = false;
            $error = \Translator::getTranslation('error_202');
            $errormsg = sprintf($error, \Translator::getTranslation('Password'));
            $finalError .= '<div>'.$errormsg.'</div>';
        }

        if (!isset($_POST['role']) || strlen(trim($_POST['role'])) == 0 || ($_POST['role'] !== 'admin' && $_POST['role'] !== 'user')){
            $shouldSave = false;
            $error = \Translator::getTranslation('error_202');
            $errormsg = sprintf($error, \Translator::getTranslation('Role'));
            $finalError .= '<div>'.$errormsg.'</div>';
        }

        $this->model = new \application\models\User();
        $user = $this->model->get(array('where'=>array('username'=>$_POST['username'])));
        if ($user){
            $error = \Translator::getTranslation('error_201');
            $errormsg = sprintf($error, \Translator::getTranslation('User'), \Translator::getTranslation('Username'), $_POST['username']);
            $finalError = '<div>'.$errormsg.'</div>';
            $shouldSave = false;
        }

        if ($shouldSave){
            $createArray = $_POST;
            unset($createArray['submit']);
            $this->model->create($createArray);
            $res = $this->model->save();
            if ($res){
                $url = Engine::url(array("controller"=>"user","action"=>"index"));
                $result['success'] = true;
                $smsg = \Translator::getTranslation('success_msg_user_create');
                $result['successmsg'] = sprintf($smsg, $_POST['username'], $url);
                $this->session->setSuccessMessage($result['successmsg']);
                //$result['successmsg'] = "User with username: ".$_POST['username']." created successfully.<div><a href=\"".$url."\" >Return to Users view</a></div>";
            }else {
                $result['success'] = false;
                $smsg = \Translator::getTranslation('error_msg_user_create');
                $result['errormsg'] = sprintf($smsg, $_POST['username']);
                $this->session->setErrorMessage($result['errormsg']);
            }
        }else{
            $result['success'] = false;
            $result['errormsg'] = $finalError;
            $this->session->setErrorMessage($result['errormsg']);
        }

        return $result;

    }

    private function processEdit($postdata, $sessionUser, $args){
        $result = array();
        $shouldSave = true;
        $finalError = "";

        $fields = array('username'=>'username','role'=>'role',"password"=>"password");

        if (!isset($postdata['username']) || strlen(trim($postdata['username'])) == 0 || strlen(trim($postdata['username'])) > 30){
            $shouldSave = false;
            $error = \Translator::getTranslation('error_202');
            $errormsg = sprintf($error, \Translator::getTranslation('Username'));
            $finalError = '<div>'.$errormsg.'</div>';
            $result['success'] = false;
        }
        $this->model = new \application\models\User();
        $testUsername = $this->model->checkUsernameValidity($postdata['username'], $postdata['user_id']);
        if (!$testUsername){
            $shouldSave = false;
            $error = \Translator::getTranslation('error_202');
            $errormsg = sprintf($error, \Translator::getTranslation('Username'));
            $finalError = '<div>'.$errormsg.'</div>';
            $result['success'] = false;
        }



        if (!isset($postdata['password']) || strlen(trim($postdata['password'])) == 0){
            unset($fields['password']);
        }

        if (!isset($postdata['role']) || strlen(trim($postdata['role'])) == 0 || ($postdata['role'] !== 'admin' && $postdata['role'] !== 'user')){
            $shouldSave = false;
            $result['success'] = false;
            $error = \Translator::getTranslation('error_202');
            $errormsg = sprintf($error, \Translator::getTranslation('Role'));
            $finalError .= '<div>'.$errormsg.'</div>';
        }

        if ($shouldSave){
            $this->model = new \application\models\User();
            $createArray = $postdata;
            $this->model->setId($postdata['user_id']);
            $this->model->create($createArray);
            $res = $this->model->update($fields);
            if ($res){
                $url = Engine::url(array("controller"=>"user","action"=>"index"));
                $result['success'] = true;
                $smsg = \Translator::getTranslation('success_msg_user_edit');
                $result['successmsg'] = sprintf($smsg, $_POST['username'], $url);
                $this->session->setSuccessMessage($result['successmsg']);
                //$result['successmsg'] = "User with username: ".$postdata['username']." updated successfully.<div><a href=\"".$url."\" >Return to Users view</a></div>";
            }else {
                $result['success'] = false;
                $smsg = \Translator::getTranslation('error_msg_user_edit');
                $result['errormsg'] = sprintf($smsg, $_POST['username']);
                $this->session->setErrorMessage($result['errormsg']);
                //$result['errormsg']  = "User with username: ".$postdata['username']." could not be updated. Please try again later.";

            }

        }else{
            $result['success'] = false;
            $result['errormsg'] = $finalError;
            $this->session->setErrorMessage($result['errormsg']);
        }
        return $result;
    }

    public function delete(){
        $result = array();
        Engine::setResponseContent();
        if (!$this->session->isUserLoggedIn() || $this->session->getUser()->getRole() !== "admin"){
            $result['success'] = false;
            $result['error'] = array('code'=>'001', 'msg'=>\Translator::getTranslation('You are not authorized for this resource.'));
            echo json_encode($result);
            die();
        }
        if (!isset($_POST['user_id']) || strlen(trim($_POST['user_id'])) == 0){
            $result['success'] = false;
            $result['error'] = array('code'=>'002', 'msg'=>\Translator::getTranslation('failed_request'));
            echo json_encode($result);
            die();
        }
        $this->model = new \application\models\User();
        $users = $this->model->get(array('where'=>array('id'=>intval($_POST['user_id']))));
        if (!$users){
            $result['success'] = false;
            $result['error'] = array('code'=>'002', 'msg'=>\Translator::getTranslation('failed_request'));
            echo json_encode($result);
            die();
        }
        $user = $users[0];

        $this->model->create($user);
        $this->model->delete();
        $result['success'] = true;
        echo json_encode($result);
        die();
    }


}