<?php
/**
 * User: Frantzolakis Manolis
 * Date: 18/7/2016
 * Time: 5:16 μμ
 */

namespace application\controllers;

use Engine;

class Index extends \BaseController {

    public function __construct()
    {
        parent::__construct();
        $this->pushStyle(APPLICATION_STYLES_URL."main.css");
        $this->view->setFooter("footer.php");
    }

    public function index(){
        if (!$this->session->isUserLoggedIn()){
            Engine::redirect(Engine::url(array('controller'=>'user', 'action'=>'login')));
        }

        $sessionUser = $this->session->getUser();

        $this->view->setHeader("logged/{$sessionUser->getRole()}/header.php");
        $this->view->setFooter("footer.php");
        $this->viewName = ("Index/index.php");

    }

    public function logout(){
        parent::logout();
    }

}