<?php
/**
 * User: Frantzolakis Manolis
 * Date: 22/7/2016
 * Time: 10:46 πμ
 */

namespace application\controllers;

use Engine;


class Item extends \BaseController {

    public function __construct(){
        parent::__construct();
        $this->pushStyle(APPLICATION_STYLES_URL."main.css");
        $this->view->setFooter("footer.php");
    }

    public function index($args){

        if (!$this->session->isUserLoggedIn()){
            Engine::redirect(Engine::url(array('controller'=>'user', 'action'=>'login')));
        }

        $this->removeStyle(APPLICATION_STYLES_URL."main.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."bootstrap_table/bootstrap-table-new.min.css");
        $this->pushStyle(APPLICATION_STYLES_URL."main.css");

        $scripts = array();
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-locale-all.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-key-events.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-mobile.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-toolbar.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-filter.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-filter-control.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-natural-sorting.min.js";
        $this->view->assign("successmsg", $this->session->flushSuccessMessage());
        $this->view->assign("errormsg", $this->session->flushErrorMessage());
        $scripts[] = APPLICATION_SCRIPTS_URL."items.js";

        $this->view->assign("scripts",$scripts);
        $this->view->assign("sessionUser", $this->session->getUser());

        $this->view->setHeader("logged/{$this->session->getUser()->getRole()}/header.php");
        $this->view->setFooter("footer.php");
        $this->view->assign('fluid',true);
        $this->viewName = ("Item/index.php");

    }

    private function process($postdata, $id){

        $shouldSave = true;
        $finalError = "";
        $result = array();

        if (!isset($postdata['type']) || strlen(trim($postdata['type'])) == 0 ){
            $shouldSave = false;
            $finalError = '<div>Field \'Type\' is not valid.</div>';
        }else{
            $createArray['type'] = $postdata['type'];
        }
        if (!isset($postdata['description']) || strlen(trim($postdata['description'])) == 0 ){
            $createArray['description'] = null;
        }else{
            $createArray['description'] = $postdata['description'];
        }
        if (!isset($postdata['ROI']) || !is_numeric($postdata['ROI'])){
            $shouldSave = false;
            $finalError = '<div>Field \'ROI\' is not valid.</div>';
        }else{
            $createArray['ROI'] = $postdata['ROI'];
        }
    /*    if (!isset($postdata['min_spend']) || !is_numeric($postdata['min_spend'])){
            $createArray['min_spend'] = null;
        }else{
            $createArray['min_spend'] = $postdata['min_spend'];
        }
        if (!isset($postdata['max_spend']) || !is_numeric($postdata['max_spend'])){
            $createArray['max_spend'] = null;
        }else{
            $createArray['max_spend'] = $postdata['max_spend'];
        }
        if (!isset($postdata['min_cost']) || !is_numeric($postdata['min_cost'])){
            $createArray['min_cost'] = null;
        }else{
            $createArray['min_cost'] = $postdata['min_cost'];
        }*/
        if (!isset($postdata['cpereuro']) || !is_numeric($postdata['cpereuro'])){
            $createArray['cpereuro'] = null;
        }else{
            $createArray['cpereuro'] = $postdata['cpereuro'];
        }

        $this->session->getUserId();

        $this->model = new \application\models\Item();

        if ($shouldSave){
            if($id) {
                $createArray = $_POST;
                $createArray['id'] = $id;
                $createArray['user_id'] = $this->session->getUserId();;

                unset($createArray['submit']);
                $this->model->create($createArray);

                $res = $this->model->update();
            }else{
                $createArray = $_POST;
                $createArray['user_id'] = $this->session->getUserId();;

                unset($createArray['submit']);
                $this->model->create($createArray);

                $res = $this->model->save();
            }


            if ($res){
                $result['valid'] = true;
                $result['success'] = true;
                if(!$id){
                    $result['successmsg'] = 'Item' . $_POST['type'] . ' created successfully.';//<div><a href="'. $url .">Επιστροφή στη Προβολή</a></div>";
                }else
                    $result['successmsg'] = 'Item ' . $_POST['type'] . ' updated successfully.';//<div><a href="'. $url .">Επιστροφή στη Προβολή</a></div>";
                $this->session->setSuccessMessage($result['successmsg']);

            }else {
                $result['valid'] = true;
                $result['success'] = false;
                if (!$id) {
                    $result['errormsg'] = 'Item ' . $_POST['type'] . ' could not be created. Please try again later.';
                }else{
                    $result['errormsg'] = 'Item ' . $_POST['type'] .  ' could not be updated. Please try again later.';
                }
                $this->session->setErrorMessage($result['errormsg']);
            }
        }else{
            $result['valid'] = false;
            $result['success'] = false;
            $result['errormsg'] = $finalError;
            $this->session->setErrorMessage($result['errormsg']);
        }

        return $result;
    }
    public function create($args){
        if (!$this->session->isUserLoggedIn()){
            Engine::redirect(Engine::url(array('controller'=>'user', 'action'=>'login')));
        }

        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."timepicker/moment.min.js";
        if (isset($_POST['submit'])){

            $res = $this->process($_POST, null);
            Engine::redirect(Engine::url(array('controller'=>'item', 'action'=>'index')));
            die();

            $res = $this->process($_POST,null , $args);
            if(isset($res['valid']) && $res['valid']) {
                Engine::redirect(Engine::url(array('controller' => 'item', 'action' => 'index')));
                die();
            }else{
                Engine::redirect(Engine::url(array('controller' => 'item', 'action' => 'create')));
                die();
            }


        /*  if ($res['success'] == true){
            $this->view->assign("successmsg", $res['successmsg']);
        }else {
            $this->view->assign("errormsg", $res['errormsg']);
        }*/
        }
        $datePickerLocale = "en";
        if (Engine::$LANGUAGE === "el"){
            $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."timepicker/locales/el.js";
            $datePickerLocale = "el";
        }

        $this->model = new \application\models\Item();
        $items = $this->model->get();
        $this->view->assign('items', $items);

        $this->view->assign("successmsg", $this->session->flushSuccessMessage());
        $this->view->assign("errormsg", $this->session->flushErrorMessage());
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."timepicker/bootstrap-datetimepicker.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."select/bootstrap-select.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."typeahead/twitter-typeahead.bundle.min.js";
        $scripts[] = APPLICATION_SCRIPTS_URL."items.js";
        $this->view->setHeader("logged/{$this->session->getUser()->getRole()}/header.php");
        $emptyFieldText = \Translator::getTranslation('empty_field');
        $cannotExceedText = sprintf(\Translator::getTranslation('can_not_exceed'), "30");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."select/bootstrap-select.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."timepicker/bootstrap-datetimepicker.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."typeahead/bootstrap-tokenfield.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."typeahead/tokenfield-typeahead.min.css");
        $rawScripts[] = '<script> var datepickerlocale ="'.$datePickerLocale.'"; var noneSelected="'.NONE_SELECTED_TEXT.'"; var emptyFieldText="'.$emptyFieldText.'"; var cannotExceedText = "'.$cannotExceedText.'"</script>';
        $this->view->assign("scripts",$scripts);
        $this->view->assign("rawScripts",$rawScripts);

        $this->view->setFooter("footer.php");
        $this->viewName = ("Item/create.php");
    }

    public function getItems($args){
        if (!$this->session->isUserLoggedIn()){
            echo json_encode(array('total' => 0, 'rows'=>array()));
            die();
        }
        $this->model = new \application\models\Item();
        $items = $this->model->get();
        if ($items){
            $count = count($items);
            $offset = 0;
            $limit = 50;
          /*  if (!isset($args['_request_params']['sort'])){
                $sort = null;
            }else {
                if ($args['_request_params']['sort'] === "fullname"){
                    $sort =  'surname '.$args['_request_params']['order'].', name '.$args['_request_params']['order'];
                }else {
                    $sort = $args['_request_params']['sort'] .' '.$args['_request_params']['order'];
                }
            }*/

            if (isset($args['_request_params']['offset']) && is_numeric($args['_request_params']['offset'])){
                $offset = intval($args['_request_params']['offset']);
            }

            if (isset($args['_request_params']['search']) && strlen(trim($args['_request_params']['search'])) > 0){
                $searchArg = str_replace(";", "", $args['_request_params']['search']);
                $items = $this->model->get(array('where'=>array('name LIKE'=>"%".$searchArg."%")));
                $count = count($items);
         //       $patients = $this->model->get(array('order'=>$sort, 'limit'=>$limit, 'offset'=>$offset,'where'=>array('unique_id LIKE'=>"%".$searchArg."%",'insurance_number LIKE'=>"%".$searchArg."%",'surname LIKE'=>"%".$searchArg."%"),'relation'=>'OR'));
            }else {
                $items = $this->model->get(array('limit'=>$limit, 'offset'=>$offset));
            }

            foreach($items as &$item){

                $item['formatter'] = '<a data-toggle="tooltip" data-placement="bottom" title="Επεξεργασία" class="icon-link" href="'.Engine::url(array('controller'=>'item','action'=>'edit','id'=>$item['id'])).'">'.
                    '<span class="glyphicon glyphicon-edit"></span>'.
                    '</a>'.
                    '<a data-toggle="tooltip" data-placement="bottom" title="Delete" class="delete-item" href="" data-id="'.$item['id'].'" data-uname="'.$item['type'].'">'.
                    '<span class="glyphicon glyphicon-trash"></span>'.
                    '</a>';
            }
            unset($item);
            echo json_encode(array('total'=>$count, 'rows'=>$items));
        }else {
            echo json_encode(array('total'=>0, 'rows'=>array()));

        }
    }

    public function edit($args){
        if (!$this->session->isUserLoggedIn()){
            Engine::redirect(Engine::url(array('controller'=>'user', 'action'=>'login')));
            die();
        }

        if (!isset($args['_get_params']['id']) || !is_numeric($args['_get_params']['id'])){
            Engine::redirect(Engine::url(array('controller'=>'item', 'action'=>'index')));
            die();
        }

   /*     if (isset($_POST['submit'])){
            if (isset($_POST['form']) && $_POST['form'] === "demographics") {
                $res = $this->process($_POST, $this->session->getUser(), true);
                Engine::redirect(Engine::url(array('controller' => 'patient', 'action' => 'edit', 'id' => $args['_get_params']['id'])));
                die();
            }else if (isset($_POST['form']) && $_POST['form'] === "spouse"){
                $res = $this->processSpouse($_POST, $this->session->getUser());
                Engine::redirect(Engine::url(array('controller' => 'patient', 'action' => 'edit', 'id' => $args['_get_params']['id'])));
                die();
            }
            /*  if ($res['success'] == true){
                $this->view->assign("successmsg", $res['successmsg']);
            }else {
                $this->view->assign("errormsg", $res['errormsg']);
            }*/
    /*    }

        $this->model = new \application\models\Patient();
        $patients = $this->model->get(array('where'=>array('id'=>intval($args['_get_params']['id']))));
        if (!$patients){
            Engine::redirect(Engine::url(array('controller'=>'patient', 'action'=>'index')));
            die();
        }

        $t = $patients[0]['birthdate'];
        $t1 = explode('-', $t);
        $t2 = $t1[2].'/'.$t1[1].'/'.$t1[0];
        $patients[0]['birthdate'] = $t2;
        $this->view->assign('patient', $patients[0]);
        $datePickerLocale = "en";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."timepicker/moment.min.js";
        if (Engine::$LANGUAGE === "el"){
            $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."timepicker/locales/el.js";
            $datePickerLocale = "el";
        }*/
        if (isset($_POST['submit'])){
            $res = $this->process($_POST,$args['_get_params']['id'] , $args);
            if(isset($res['valid']) && $res['valid']) {
                Engine::redirect(Engine::url(array('controller' => 'item', 'action' => 'index')));
                die();
            }else{
                Engine::redirect(Engine::url(array('controller' => 'item', 'action' => 'edit', 'id' => $args['_get_params']['id'])));
                die();
            }

        /*    $shouldSave = true;
            $finalError = "";
            $result = array();
            if (!isset($_POST['name']) || strlen(trim($_POST['name'])) == 0 || strlen(trim($_POST['name'])) > 100){
                $shouldSave = false;
                $finalError = '<div>Το πεδίο \'Onoma\' δεν είναι έγκυρο.</div>';
            }
            if (!isset($_POST['length'])){
                $shouldSave = false;
                $finalError .= '<div>Το πεδίο \'Μήκος\' δεν είναι έγκυρο.</div>';
            }

            if (!isset($_POST['width'])){
                $shouldSave = false;
                $finalError .= '<div>Το πεδίο \'Πλάτος\' δεν είναι έγκυρο.</div>';
            }
            if (!isset($_POST['height'])){
                $shouldSave = false;
                $finalError .= '<div>Το πεδίο \'Ύψος\' δεν είναι έγκυρο.</div>';
            }

            $this->model = new \application\models\Item();

            if ($shouldSave){

                $createArray = $_POST;
                $createArray['name'] = $_POST['name'];
                $createArray['length'] = $_POST['length'];
                $createArray['width'] = $_POST['width'];
                $createArray['height'] = $_POST['height'];
                $this->view->assign('item',$createArray);
                unset($createArray['submit']);
                echo $createArray['name'];
             //   $this->model->create($createArray);
                $res = $this->model->update($createArray);
                if ($res){
                    $url =  Engine::url(array("controller"=>"item","action"=>"index"));
                    $result['success'] = true;
                    $result['successmsg'] = 'Το είδος ' . $_POST['name'] . ' ενημερώθηκε επιτυχώς.<div><a href="'. $url .">Επιστροφή στη Προβολή</a></div>";
                    $this->view->assign("successmsg", $res['successmsg']);
                }else {
                    $result['success'] = false;
                    $result['errormsg'] = 'Το είδος ' . $_POST['name'] .  ' δεν μπόρεσε να ενημερωθεί. Παρακαλώ δοκιμάστε αργότερα.';
                    $this->view->assign("errormsg", $res['successmsg']);
                }
            }else{
                $result['success'] = false;
                $result['errormsg'] = $finalError;
                $this->view->assign("errormsg", $result['errormsg']);
            }
            return;*/
        }
      //  $datePickerLocale = "el";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."timepicker/moment.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."timepicker/bootstrap-datetimepicker.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."select/bootstrap-select.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-locale-all.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-key-events.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-mobile.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-toolbar.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-filter.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-filter-control.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-natural-sorting.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."typeahead/twitter-typeahead.bundle.min.js";
        $scripts[] = APPLICATION_SCRIPTS_URL."items.js";
        $this->view->assign("successmsg", $this->session->flushSuccessMessage());
        $this->view->assign("errormsg", $this->session->flushErrorMessage());
        $emptyFieldText = \Translator::getTranslation('empty_field');
        $cannotExceedText = sprintf(\Translator::getTranslation('can_not_exceed'), "30");
        $this->removeStyle(APPLICATION_STYLES_URL."main.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."bootstrap_table/bootstrap-table-new.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."select/bootstrap-select.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."timepicker/bootstrap-datetimepicker.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."typeahead/bootstrap-tokenfield.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."typeahead/tokenfield-typeahead.min.css");
        $this->pushStyle(APPLICATION_STYLES_URL."main.css");

     //   $rawScripts[] = '<script> var datepickerlocale ="'.$datePickerLocale.'"; var noneSelected="'.NONE_SELECTED_TEXT.'"; var emptyFieldText="'.$emptyFieldText.'"; var cannotExceedText = "'.$cannotExceedText.'"</script>';
     //   $rawScripts[] = '<script> var noneSelected="'.NONE_SELECTED_TEXT.'"; var emptyFieldText="'.$emptyFieldText.'"; var cannotExceedText = "'.$cannotExceedText.'"</script>';
        $this->view->assign("scripts",$scripts);
        $this->model = new \application\models\Item();
        $items = $this->model->get(array('where'=>array('id'=>intval($args['_get_params']['id']))));
        if (!$items){
            Engine::redirect(Engine::url(array('controller'=>'item', 'action'=>'index')));
            die();
        }
        $this->view->assign('item', $items[0]);

        $this->view->assign("scripts",$scripts);
    //    $this->view->assign("rawScripts",$rawScripts);

        $this->view->setHeader("logged/{$this->session->getUser()->getRole()}/header.php");
        $this->view->setFooter("footer.php");
        $this->view->assign('fluid',true);
        $this->viewName = ("Item/edit.php");
    }



    public function getUrl($args){
        if (!isset($args['_request_params']['template']) || !is_numeric($args['_request_params']['template'])){
            echo json_encode(array('success'=>false, 'error'=>array('msg'=>'Please define a template')));
        }

        switch($args['_request_params']['template']){
            case 1:
                $url = Engine::url(array('controller'=>'examinations','action'=>'create','patId'=>$args['_request_params']['patId'],'form'=>'sperm_refrigeration'));
                echo json_encode(array('success'=>true, 'href'=>$url));
                break;
            case 2:
                $url = Engine::url(array('controller'=>'examinations','action'=>'create','patId'=>$args['_request_params']['patId'],'form'=>'vitrification'));
                echo json_encode(array('success'=>true, 'href'=>$url));
                break;
            case 3:
                $url = Engine::url(array('controller'=>'examinations','action'=>'create','patId'=>$args['_request_params']['patId'],'form'=>'embryos_vitrification'));
                echo json_encode(array('success'=>true, 'href'=>$url));

                break;
            case 4:
                $url = Engine::url(array('controller'=>'examinations','action'=>'create','patId'=>$args['_request_params']['patId'],'form'=>'blood_examination'));
                echo json_encode(array('success'=>true, 'href'=>$url));

                break;
            case 5:
                $url = Engine::url(array('controller'=>'examinations','action'=>'create','patId'=>$args['_request_params']['patId'],'form'=>'med_history'));
                echo json_encode(array('success'=>true, 'href'=>$url));

                break;
        }
        die();
    }

    public function delete($args){

        if (!$this->session->isUserLoggedIn()){
            echo json_encode(array('success'=>false));
            die();
        }
        if (!isset($_POST['item_id']) || !is_numeric($_POST['item_id'])){
            echo json_encode(array('success'=>false));
            die();
        }

        $item = new \application\models\Item();
        $items = $item->get(array('where'=>array('id'=>intval($_POST['item_id']))));

        if ($items){
            $item->delete($_POST['item_id']);
            echo json_encode(array('success'=>true));
            die();

        }else {
            echo json_encode(array('success'=>false));
            die();
        }

    }
}