<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/3/2017
 * Time: 5:03 μμ
 */


namespace application\controllers;

use Engine;

use application\models\Item;

//include "setting.php";
//include "data.php";
//include "TSimplex.class.php";
use application\models\TSimplex;



//require_once 'application/util/truck.php';
//require_once 'application/util/SimpleBox.php';

class Campaign extends \BaseController {

    public function __construct(){
        parent::__construct();
        $this->pushStyle(APPLICATION_STYLES_URL."main.css");
        $this->view->setFooter("footer.php");
    }

    public function index(){

        if (!$this->session->isUserLoggedIn()){
            Engine::redirect(Engine::url(array('controller'=>'user', 'action'=>'login')));
        }

        $this->removeStyle(APPLICATION_STYLES_URL."main.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."bootstrap_table/bootstrap-table-new.min.css");
        $this->pushStyle(APPLICATION_STYLES_URL."main.css");

        $scripts = array();
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-locale-all.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-key-events.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-mobile.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-toolbar.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-filter.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-filter-control.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-natural-sorting.min.js";
        $this->view->assign("successmsg", $this->session->flushSuccessMessage());
        $this->view->assign("errormsg", $this->session->flushErrorMessage());
        $scripts[] = APPLICATION_SCRIPTS_URL."campaign.js";

        $this->view->assign("scripts",$scripts);
        $this->view->assign("sessionUser", $this->session->getUser());

        $this->view->setHeader("logged/{$this->session->getUser()->getRole()}/header.php");
        $this->view->setFooter("footer.php");
        $this->view->assign('fluid',true);
        $this->viewName = ("Campaign/index.php");

    }

    private function process($postdata, $id){

        $shouldSave = true;
        $finalError = "";
        $result = array();

        if (!isset($postdata['name']) || strlen(trim($postdata['name'])) == 0 ){
            $shouldSave = false;
            $finalError = '<div>Field \'Name\' is not valid.</div>';
        }else{
            $createArray['name'] = $postdata['name'];
        }
        if (!isset($postdata['description']) || strlen(trim($postdata['description'])) == 0 ){
            $createArray['description'] = null;
        }else{
            $createArray['description'] = $postdata['description'];
        }
        if (!isset($postdata['budget']) || !is_numeric($postdata['budget'])){
            $shouldSave = false;
            $finalError = '<div>Field \'Budget\' is not valid.</div>';
        }else{
            $createArray['budget'] = $postdata['budget'];
        }
        if (!isset($postdata['market_base']) || !is_numeric($postdata['market_base'])){
            $shouldSave = false;
            $finalError = '<div>Field \'Market base\' is not valid.</div>';
        }else{
            $createArray['market_base'] = $postdata['market_base'];
        }

        $this->model = new \application\models\Campaign();
        $code = null;
        if ($shouldSave){
            if($id) {
                $createArray = $_POST;
                $createArray['id'] = $id;
                unset($createArray['submit']);
                $this->model->create($createArray);
                $res = $this->model->update();
            }else{
                $createArray = $_POST;
                unset($createArray['submit']);
                $this->model->create($createArray);
                $res = $this->model->save();
            }

            if ($res){
                $result['valid'] = true;
                $result['success'] = true;
                if(!$id){
                    $result['successmsg'] = 'Η αποστολή με κωδικό ' . $code . ' δημιουργήθηκε επιτυχώς.';//<div><a href="'. $url .">Επιστροφή στη Προβολή</a></div>";
                }else
                    $result['successmsg'] = 'Η αποστολή με κωδικό ' . $code . ' ενημερώθηκε επιτυχώς.';//<div><a href="'. $url .">Επιστροφή στη Προβολή</a></div>";
                $this->session->setSuccessMessage($result['successmsg']);

            }else {
                $result['valid'] = true;
                $result['success'] = false;
                if (!$id) {
                    $result['errormsg'] = 'Η αποστολή με κωδικό ' . $res['code'] . ' δεν μπόρεσε να δημιουργηθεί. Παρακαλώ δοκιμάστε αργότερα.';
                }else{
                    $result['errormsg'] = 'Η αποστολή με κωδικό ' . $res['code'] .  ' δεν μπόρεσε να ενημερωθεί. Παρακαλώ δοκιμάστε αργότερα.';
                }
                $this->session->setErrorMessage($result['errormsg']);
            }
        }else{
            $result['valid'] = false;
            $result['success'] = false;
            $result['errormsg'] = $finalError;
            $this->session->setErrorMessage($result['errormsg']);
        }

        return $result;
    }
    public function create($args){
        if (!$this->session->isUserLoggedIn()){
            Engine::redirect(Engine::url(array('controller'=>'user', 'action'=>'login')));
        }

        if (isset($_POST['submit'])){
            $res = $this->process($_POST,null , $args);
            if(isset($res['valid']) && $res['valid']) {
                Engine::redirect(Engine::url(array('controller' => 'campaign', 'action' => 'index')));
                die();
            }else{
                Engine::redirect(Engine::url(array('controller' => 'campaign', 'action' => 'create')));
                die();
            }
        }

        $this->view->assign("successmsg", $this->session->flushSuccessMessage());
        $this->view->assign("errormsg", $this->session->flushErrorMessage());
        $scripts[] = APPLICATION_SCRIPTS_URL."Sortable.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."timepicker/moment.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."timepicker/bootstrap-datetimepicker.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."select/bootstrap-select.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."sortable/jquery-ui.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."sortable/jquery.mjs.nestedSortable.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."typeahead/twitter-typeahead.bundle.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-filter.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-filter-control.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-natural-sorting.min.js";

        $scripts[] = APPLICATION_SCRIPTS_URL."campaign.js";
        $this->view->setHeader("logged/{$this->session->getUser()->getRole()}/header.php");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."select/bootstrap-select.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."timepicker/bootstrap-datetimepicker.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."typeahead/bootstrap-tokenfield.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."typeahead/tokenfield-typeahead.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."bootstrap_table/bootstrap-table-new.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."sortable/jquery-ui.min.css");
        $this->pushStyle(ENGINE_STYLES_LIB_URLS."font-awesome.min.css");
        $rawScripts[] = '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPvIPGNWydSk2d5k5iNtqgqEJ11u-tahg" async defer></script>';

    /*

        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."bootstrap/css/bootstrap-tagsinput.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."select2/css/select2.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."summernote/summernote.css");*/
        $this->view->assign("scripts",$scripts);
        $this->view->assign('rawScripts', $rawScripts);
        $userId = $this->session->getUserId();

        $item = new Item();
        $items = $item->get(array('where' => array ('user_id' => $userId)));
        if($items) {
            $this->view->assign("items", $items);
        }
        $this->view->setFooter("footer.php");
        $this->viewName = ("Campaign/create.php");
    }

    public function getCampaigns($args){
        if (!$this->session->isUserLoggedIn()){
            echo json_encode(array('total' => 0, 'rows'=>array()));
            die();
        }
        $this->model = new \application\models\Campaign();
        $campaigns = $this->model->get();
        if ($campaigns){
            $count = count($campaigns);
            $offset = 0;
            $limit = 50;
            /*  if (!isset($args['_request_params']['sort'])){
                  $sort = null;
              }else {
                  if ($args['_request_params']['sort'] === "fullname"){
                      $sort =  'surname '.$args['_request_params']['order'].', name '.$args['_request_params']['order'];
                  }else {
                      $sort = $args['_request_params']['sort'] .' '.$args['_request_params']['order'];
                  }
              }*/

            if (isset($args['_request_params']['offset']) && is_numeric($args['_request_params']['offset'])){
                $offset = intval($args['_request_params']['offset']);
            }

            if (isset($args['_request_params']['search']) && strlen(trim($args['_request_params']['search'])) > 0){
                $searchArg = str_replace(";", "", $args['_request_params']['search']);
                $campaigns = $this->model->get(array('where'=>array('name LIKE'=>"%".$searchArg."%")));
                $count = count($campaigns);
                //       $patients = $this->model->get(array('order'=>$sort, 'limit'=>$limit, 'offset'=>$offset,'where'=>array('unique_id LIKE'=>"%".$searchArg."%",'insurance_number LIKE'=>"%".$searchArg."%",'surname LIKE'=>"%".$searchArg."%"),'relation'=>'OR'));
            }else {
                $campaigns = $this->model->get(array('limit'=>$limit, 'offset'=>$offset));
            }

            foreach($campaigns as &$campaign){

                $campaign['formatter'] = '<a data-toggle="tooltip" data-placement="bottom" title="Edit" class="icon-link" href="'.Engine::url(array('controller'=>'campaign','action'=>'edit','id'=>$campaign['id'])).'">'.
                    '<span class="glyphicon glyphicon-eye-open"></span>'.
                    '</a>'.
                    '<a data-toggle="tooltip" data-placement="bottom" title="Delete" class="delete-campaign" href="" data-id="'.$campaign['id'].'" data-name="'.$campaign['name'].'">'.
                    '<span class="glyphicon glyphicon-trash"></span>'.
                    '</a>';
            }
            unset($campaign);
            echo json_encode(array('total'=>$count, 'rows'=>$campaigns));
        }else {
            echo json_encode(array('total'=>0, 'rows'=>array()));

        }
    }

    public function getItems(){
        if (!$this->session->isUserLoggedIn()){
            echo json_encode(array('success' => false));
            die();
        }
        $userId = $this->session->getUserId();

        $item = new Item();
        $items = $item->get(array('where' => array ('user_id' => $userId)));

        if($items){
            echo json_encode(array('success'=>true,'items'=>$items));
        }else {
            echo json_encode(array('success' => false));
        }
    }


    public function saveCampaign(){
        if (!$this->session->isUserLoggedIn()){
            $result['success'] = false;
            $result['errormsg'] = "Ο χρήστης δεν είναι πλέον συνδεδεμενος";
            $result['url'] = Engine::url(array('controller'=>'user', 'action'=>'login'));
            echo json_encode($result);
            die();
        }

        //$createArray = new array();

        if (!isset($_POST['name']) ){
            echo json_encode(array('success'=>false));
            die();
        }

        if (!isset($_POST['description']) ){
            $createArray['description'] = null;
        }else{
            $createArray['description'] = $_POST['description'];
        }

        if (!isset($_POST['budget']) || !is_numeric($_POST['budget'])){
            echo json_encode(array('success'=>false));
            die();
        }

        if (!isset($_POST['market_base']) || !is_numeric($_POST['market_base'])){
            echo json_encode(array('success'=>false));
            die();
        }

        if (!isset($_POST['tv']) || !is_numeric($_POST['tv'])){
            echo json_encode(array('success'=>false));
            die();
        }

        if (!isset($_POST['seo']) || !is_numeric($_POST['seo'])){
            echo json_encode(array('success'=>false));
            die();
        }

        if (!isset($_POST['adwords']) || !is_numeric($_POST['adwords'])){
            echo json_encode(array('success'=>false));
            die();
        }

        if (!isset($_POST['facebook']) || !is_numeric($_POST['facebook'])){
            echo json_encode(array('success'=>false));
            die();
        }

        $items = '{';
        $hasPrevious = false;
        if($_POST['tv']>0){
            if($hasPrevious) {
                $items .= '",TV :' . $_POST['tv'] . '"';
                $hasPrevious = true;
            }else{
                $items .= '"TV :' . $_POST['tv'] . '"';
                $hasPrevious = true;
            }
        }
        if($_POST['seo']>0){
            if($hasPrevious) {
                $items .= '",SEO :' . $_POST['seo'] . '"';
                $hasPrevious = true;
            }else{
                $items .= '"SEO :' . $_POST['seo'] . '"';
                $hasPrevious = true;
            }
        }
        if($_POST['adwords']>0){
            if($hasPrevious) {
                $items .= '",Adwords :' . $_POST['adwords'] . '"';
                $hasPrevious = true;
            }else{
                $items .= '"Adwords :' . $_POST['adwords'] . '"';
                $hasPrevious = true;
            }
        }
        if($_POST['facebook']>0){
            if($hasPrevious) {
                $items .= '",Facebook :' . $_POST['facebook'] . '"';
                $hasPrevious = true;
            }else{
                $items .= '"Facebook :' . $_POST['facebook'] . '"';
                $hasPrevious = true;
            }
        }
        $items .= '}';

        $createArray['name'] = $_POST['name'];
        $createArray['budget'] = $_POST['budget'];
        $createArray['market_base'] = $_POST['market_base'];
        $createArray['items'] = $items;
        $createArray['user_id'] = $this->session->getUserId();;

     //   unset($createArray['submit']);
        $this->model = new \application\models\Campaign();
        $this->model->create($createArray);

        $res = $this->model->save();

        $url = Engine::url(array("controller" => "campaign", "action" => "index"));
        $result['success'] = true;
        $result['id'] = $res;
        $result['successmsg'] = "Η αποστολή ---- με κωδικό ---- ενημερώθηκε επιτυχώς <div><a href=\"".$url."\" >Επιστροφή στην προβολή των αποστολών</a></div>";
        echo json_encode($result);
        die();


    }


    public function edit($args){
        if (!$this->session->isUserLoggedIn()){
            Engine::redirect(Engine::url(array('controller'=>'user', 'action'=>'login')));
            die();
        }

        if (!isset($args['_get_params']['id']) || !is_numeric($args['_get_params']['id'])){
            Engine::redirect(Engine::url(array('controller'=>'campaign', 'action'=>'index')));
            die();
        }

        if (isset($_POST['submit'])){
            $res = $this->process($_POST,$args['_get_params']['id'] , $args);
            if(isset($res['valid']) && $res['valid']) {
                Engine::redirect(Engine::url(array('controller' => 'campaign', 'action' => 'index')));
                die();
            }else{
        //        Engine::redirect(Engine::url(array('controller' => 'campaign', 'action' => 'edit', 'id' => $args['_get_params']['id'])));
        //        die();
            }


        }
        //  $datePickerLocale = "el";
        $scripts[] = APPLICATION_SCRIPTS_URL."Sortable.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."timepicker/moment.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."timepicker/bootstrap-datetimepicker.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."select/bootstrap-select.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-locale-all.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-key-events.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-mobile.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-toolbar.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-filter.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-filter-control.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."bootstrap_table/bootstrap-table-natural-sorting.min.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."sortable/jquery-ui.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."sortable/jquery.mjs.nestedSortable.js";
        $scripts[] = ENGINE_SCRIPTS_PLUGIN_URLS."typeahead/twitter-typeahead.bundle.min.js";

        $scripts[] = APPLICATION_SCRIPTS_URL."campaign.js";

        $this->view->assign("successmsg", $this->session->flushSuccessMessage());
        $this->view->assign("errormsg", $this->session->flushErrorMessage());
    //    $emptyFieldText = \Translator::getTranslation('empty_field');
    //    $cannotExceedText = sprintf(\Translator::getTranslation('can_not_exceed'), "30");
        $this->removeStyle(APPLICATION_STYLES_URL."main.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."bootstrap_table/bootstrap-table-new.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."select/bootstrap-select.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."timepicker/bootstrap-datetimepicker.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."typeahead/bootstrap-tokenfield.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."typeahead/tokenfield-typeahead.min.css");
        $this->pushStyle(ENGINE_STYLES_PLUGIN_URLS."sortable/jquery-ui.min.css");
        $this->pushStyle(APPLICATION_STYLES_URL."main.css");
        $rawScripts[] = '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPvIPGNWydSk2d5k5iNtqgqEJ11u-tahg" async defer></script>';


        //   $rawScripts[] = '<script> var datepickerlocale ="'.$datePickerLocale.'"; var noneSelected="'.NONE_SELECTED_TEXT.'"; var emptyFieldText="'.$emptyFieldText.'"; var cannotExceedText = "'.$cannotExceedText.'"</script>';
        //   $rawScripts[] = '<script> var noneSelected="'.NONE_SELECTED_TEXT.'"; var emptyFieldText="'.$emptyFieldText.'"; var cannotExceedText = "'.$cannotExceedText.'"</script>';
        $this->view->assign("scripts",$scripts);
        $this->model = new \application\models\Campaign();
        $campaigns = $this->model->get(array('where'=>array('id'=>intval($args['_get_params']['id']))));
        if (!$campaigns){
            Engine::redirect(Engine::url(array('controller'=>'campaign', 'action'=>'index')));
            die();
        }
        $this->view->assign('campaign', $campaigns[0]);

        $destination = new Destination();
        $destinations = $destination->get(array('where'=>array('delivery_id'=>$campaigns[0]['id'])));
        if(!$destinations){
            Engine::redirect(Engine::url(array('controller'=>'campaign', 'action'=>'index')));
            die();
        }
        $this->view->assign("destinations", $destinations);
        $this->view->assign("scripts",$scripts);
        $this->view->assign("rawScripts",$rawScripts);

        $this->view->setHeader("logged/{$this->session->getUser()->getRole()}/header.php");
        $this->view->setFooter("footer.php");
        $this->view->assign('fluid',true);
        $this->viewName = ("Campaign/edit.php");
    }


    public function delete(){

        if (!$this->session->isUserLoggedIn()){
            echo json_encode(array('success'=>false));
            die();
        }
        if (!isset($_POST['delivery_id']) || !is_numeric($_POST['delivery_id'])){
            echo json_encode(array('success'=>false));
            die();
        }

        $delivery = new \application\models\Delivery();
        $deliveries = $delivery->get(array('where'=>array('id'=>intval($_POST['delivery_id']))));

        if ($deliveries){
            $destination = new Destination();
            $destinations = $destination->get(array('where'=>array('delivery_id'=>intval($_POST['delivery_id']))));
            if(isset($destinations)) {
                foreach ($destinations as $dest) {
                    $destination->delete($dest['id']);
                }
            }
            $delivery->delete($_POST['delivery_id']);
            echo json_encode(array('success'=>true));
            die();

        }else {
            echo json_encode(array('success'=>false));
            die();
        }

    }

    public function calculateCampaign(){

        if (!isset($_POST['name']) ){
            echo json_encode(array('success'=>false));
            die();
        }

        if (!isset($_POST['budget']) || !is_numeric($_POST['budget'])){
            echo json_encode(array('success'=>false));
            die();
        }

        if (!isset($_POST['market_base']) || !is_numeric($_POST['market_base'])){
            echo json_encode(array('success'=>false));
            die();
        }

        if (!isset($_POST['tv']) || !is_numeric($_POST['tv'])){
            echo json_encode(array('success'=>false));
            die();
        }

        if (!isset($_POST['seo']) || !is_numeric($_POST['seo'])){
            echo json_encode(array('success'=>false));
            die();
        }

        if (!isset($_POST['adwords']) || !is_numeric($_POST['adwords'])){
            echo json_encode(array('success'=>false));
            die();
        }

        if (!isset($_POST['facebook']) || !is_numeric($_POST['facebook'])){
            echo json_encode(array('success'=>false));
            die();
        }

        $ROItv = 0;
        $ROIseo = 0;
        $ROIadwords = 0;
        $ROIfacebook = 0;
        $cptv = 0;
        $cpseo = 0;
        $cpadwords = 0;
        $cpfacebook = 0;
        $btv = 0;
        $bseo = 0;
        $badwords = 0;
        $bfacebook = 0;
        if($_POST['tv']>0){
            $item = new Item();
            $items = $item->get(array('where'=>array('id'=>intval($_POST['tv']))));
            if(isset($items)) {
                $ROItv = $items[0]['ROI']/100.0;
                $cptv = $items[0]['cpereuro'];
                $btv = 1;
            }
        }
        if($_POST['seo']>0){
            $item = new Item();
            $items = $item->get(array('where'=>array('id'=>intval($_POST['seo']))));
            if(isset($items)) {
                $ROIseo = $items[0]['ROI']/100.0;
                $cpseo = $items[0]['cpereuro'];
                $bseo = 1;
            }
        }
        if($_POST['adwords']>0){
            $item = new Item();
            $items = $item->get(array('where'=>array('id'=>intval($_POST['adwords']))));
            if(isset($items)) {
                $ROIadwords = $items[0]['ROI']/100.0;
                $cpadwords = $items[0]['cpereuro'];
                $badwords = 1;
            }
        }
        if($_POST['facebook']>0){
            $item = new Item();
            $items = $item->get(array('where'=>array('id'=>intval($_POST['facebook']))));
            if(isset($items)) {
                $ROIfacebook = $items[0]['ROI']/100.0;
                $cpfacebook = $items[0]['cpereuro'];
                $bfacebook = 1;
            }
        }

        $leftdata=array
        (
            array($btv,$bseo,$badwords,$bfacebook),
            array($cptv,$cpseo,$cpadwords,$cpfacebook)
        );  // Persamaan disebelah kiri // The equatition on the left

        $rightdata=array($_POST['budget'],$_POST['market_base']); //nilai seblah keanan  <- Subject to constraint // right values

        $zdata=array($ROItv,$ROIseo,$ROIadwords,$ROIfacebook); // Persamaan Maximum atau minimum // Maximum or minimum equations
        $maxiterasi = 10;
        $iscetak = false;


     /*   print_r($leftdata);
        print_r($rightdata);
        print_r($zdata);
 /*       echo $ROIfacebook .'-'. $ROIadwords .'-'. $ROIseo .'-'. $ROItv .'<br>';
        echo $cptv .'-'. $cpseo .'-'. $cpadwords .'-'. $cpfacebook;
*/
        $r = new \application\models\TSimplex($leftdata,$rightdata,$zdata,$maxiterasi,$iscetak);
        echo json_encode(array('success'=>true, 'objective'=>$r->result, 'variables'=>$r->varvalue()));
        die();
    }
}